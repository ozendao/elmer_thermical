!
!
!  Function a(D) and b(D) from Gagliardini and Meyssonier, 1997.
!  modified to fulfill the condition 3x a(D) >= 2x b(D) for D > 0.1 
!  for n=3 only 

FUNCTION ParameterA ( D ) RESULT(a)
   USE types
   USE CoordinateSystems
   USE SolverUtils
   USE ElementDescription
   USE DefUtils
   IMPLICIT NONE
   REAL(KIND=dp) :: a, D, DD     

    IF (D >= 1.0_dp) THEN
      a = 1.0_dp           
   ELSE IF (D <= 0.5) THEN
      a = EXP(5.3292_dp -15.78652_dp * (D-0.5) )
    ELSE IF (D <= 0.81) THEN
      DD = D
     ! IF (D < 0.4 ) DD = 0.4_dp

!a = EXP( 16.02784497_dp - 19.25_dp * DD )

!============paramétrisation Zwinger et al. 2006:========================
 !     a = EXP( 13.22240_dp - 15.78652_dp * DD ) 

!==============paramétrisation Gilbert gouter 2012 :=================
       !a = EXP(14.592_dp -17.477_dp * DD )
       a = EXP(0.4353_dp -15.78652_dp * (DD-0.81) )

!Vallot calibré avec B=35 et a/b=a0/b0 :

!a = EXP(18.5901_dp*DD**2-38.0464_dp*DD+19.0560)   
! a = EXP(-41.2058_dp*DD**3+113.5893_dp*DD**2-109.5524_dp*DD+36.5453_dp)


    ELSE
      a =  1.0_dp  + 2.0/3.0 * (1.0_dp - D) 
      a = a / ( D**1.5 )

    END IF
END FUNCTION ParameterA 

FUNCTION ParameterB ( D ) RESULT(b)
   USE types
   USE CoordinateSystems
   USE SolverUtils
   USE ElementDescription
   USE DefUtils
   IMPLICIT NONE
   REAL(KIND=dp) :: b, D, DD 

    IF (D >= 1.0_dp) THEN
      b = 0.0_dp           
    ELSE IF (D <= 0.5) THEN
      b = EXP(  4.8612_dp -20.4648_dp * (D-0.5) ) 
    ELSE IF (D <= 0.81) THEN
      DD = D
   !   IF (D < 0.4 ) DD = 0.4_dp
      !b = EXP( 16.75024378_dp - 22.51_dp * DD )

!============paramétrisation Zwinger et al. 2006 :=================
 !     b = EXP( 15.09371_dp - 20.46489_dp * DD )

 !============paramétrisation Gilbert gouter 2012 :=================


!b = EXP(  14.653_dp -19.921_dp * DD ) 
b = EXP(  -1.4829_dp -20.4648_dp * (DD-0.81) ) 
!b = EXP(166.2381_dp*DD**4-467.2375_dp*DD**3+508.8621_dp*DD**2-267.2714_dp*DD+57.8917)

!Vallot calibré avec B=35 et a/b=a0/b0

!b = EXP(17.9475_dp*DD**2-39.0749_dp*DD+18.3936)
!b = EXP(-109.9530_dp*DD**3+255.6465_dp*DD**2-207.995_dp*DD+57.6972_dp)

    ELSE
      b = (1.0_dp - D)**(1.0/3.0) 
      b = 3.0/4.0 * ( b / (3.0 * (1 - b)) )**1.5

    END IF
END FUNCTION ParameterB 
