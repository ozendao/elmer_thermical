 
!-------------------------------------------------------------------------------
! File: fonctions.f90
! Written by: AG, november 2011
! Modified by: -
!-------------------------------------------------------------------------------
FUNCTION freesurfmax(Model, n, f) RESULT(g)
USE DefUtils
IMPLICIT NONE
TYPE(Model_t) :: Model
TYPE(Variable_t), POINTER :: dens
INTEGER :: n,i,Nb
REAL(KIND=dp) :: f, g, x, y
logical :: first_time=.true.
real,dimension(:),allocatable :: Ztop_ini


save Ztop_ini,first_time

if (first_time) then
	first_time=.false.
	Nb = model % NumberOfNodes
	allocate(Ztop_ini(Nb))
	do i=1,Nb
		Ztop_ini(i)= model % nodes % z(i)
	enddo
endif

g=Ztop_ini(n)+100.0

END FUNCTION freesurfmax

FUNCTION freesurfmin(Model, n, f) RESULT(g)
USE DefUtils
IMPLICIT NONE
TYPE(Model_t) :: Model
TYPE(Variable_t), POINTER :: dens
INTEGER :: n,i,Nb
REAL(KIND=dp) :: f, g
logical :: first_time=.true.
real,dimension(:),allocatable :: Ztop_ini


save Ztop_ini,first_time

if (first_time) then
	first_time=.false.
	Nb = model % NumberOfNodes
	allocate(Ztop_ini(Nb))
	do i=1,Nb
		Ztop_ini(i)= model % nodes % z(i)
	enddo
endif

g=Ztop_ini(n)-40.0


END FUNCTION freesurfmin

FUNCTION surf_temp_steady(Model, n, f) RESULT(g)
USE DefUtils
TYPE(Model_t) :: Model

INTEGER :: n,i,j,cont,k
REAL(KIND=dp) :: f,g,z,x,y,t_surf,T0
real,dimension(49051,3) :: temp_l
real,dimension(271,181,3) :: temp

logical :: first_time=.true.

SAVE first_time,temp,T0

z = model % nodes % z (n)
x = model % nodes % x (n)
y = model % nodes % y (n)


!=============================================================================================
!Récupération des données=====================================================================

if (first_time) then
  first_time=.false.
 T0=GetConstReal(Model % Constants, "T_ref_enthalpy")
  open(1,file='Data/temp_surf_demlidar')
  do i=1,49051
    read(1,*) temp_l(i,1),temp_l(i,2),temp_l(i,3)
  enddo
  close(1)

  cont=0
  do i=1,271
    do j=1,181
      cont=cont+1

      temp(i,j,1)=temp_l(cont,1)
      temp(i,j,2)=temp_l(cont,2)
      temp(i,j,3)=temp_l(cont,3)
    enddo
  enddo

endif
!==============================================================================================

k=floor((x-temp(1,1,1))/10)+1
j=floor((y-temp(1,1,2))/10)+1

if ((x-temp(1,1,1))/10-floor((x-temp(1,1,1))/10)>0.5) then
k=k+1
endif

if ((y-temp(1,1,2))/10-floor((y-temp(1,1,2))/10)>0.5) then
j=j+1
endif

t_surf=temp(j,k,3)*(temp(j,k+1,1)-x)*(temp(j+1,k,2)-y)+temp(j,k+1,3)*&
&(x-temp(j,k,1))*(temp(j+1,k,2)-y)+temp(j+1,k,3)*(temp(j,k+1,1)-x)*(y-temp(j,k,2)&
&)+temp(j+1,k+1,3)*(x-temp(j,k,1))*(y-temp(j,k,2))
t_surf=t_surf/100

  g=273.15+t_surf

if (g==273.15) then
g=g+2.0
endif

g=3.626*g**2+146.3*g-T0*(146.3+3.626*T0)

END FUNCTION surf_temp_steady

FUNCTION surf_temp(Model, n, f) RESULT(g)
USE DefUtils
TYPE(Model_t) :: Model

INTEGER :: n,i,j,cont,k
REAL(KIND=dp) :: f,g,z,x,y,delta_T,T0
real,dimension(52596) :: Tmax,Tmin
real,dimension(6561,3) :: grad_l
real,dimension(81,81,3) :: grad

logical :: first_time=.true.

SAVE Tmax,Tmin,first_time,grad,T0,delta_T

z = model % nodes % z (n)
x = model % nodes % x (n)
y = model % nodes % y (n)


!=============================================================================================
!Récupération des données=====================================================================

if (first_time) then
  first_time=.false.

 T0=GetConstReal(Model % Constants, "T_ref_enthalpy")
 delta_T=GetConstReal(Model % Constants, "grad")

  open(1,file='Data/temp_fut_CNRM_max')
  open(2,file='Data/temp_fut_CNRM_min')
  do i=1,52596
    read(1,*) Tmax(i)
    read(2,*) Tmin(i)
  enddo
  close(1)
  close(2)

endif
!==============================================================================================



i=floor(f/(24*3600))


if ((Tmax(i)+Tmin(i))/2-delta_T*(z-200)<0.0) then
  g=273.15+(Tmax(i)+Tmin(i))/2-delta_T*(z-200)
else
  g=273.15
endif

g=3.626*g**2+146.3*g-T0*(146.3+3.626*T0)

END FUNCTION surf_temp

FUNCTION surf_melting(Model, n, f) RESULT(g)
USE DefUtils
TYPE(Model_t) :: Model

INTEGER :: n,i,j,cont,k
REAL(KIND=dp) :: f,g,z,x,y,delta_T,deg_jour_min,deg_jour_max
real,dimension(52596) :: Tmax

logical :: first_time=.true.

SAVE Tmax,first_time,delta_T,deg_jour_min,deg_jour_max

!=============================================================================================
!Récupération des données=====================================================================

if (first_time) then
  first_time=.false.

delta_T=GetConstReal(Model % Constants, "grad")
deg_jour_min=GetConstReal(Model % Constants, "deg_jour_min")
deg_jour_max=GetConstReal(Model % Constants, "deg_jour_max")

  open(1,file='Data/temp_fut_CNRM_max')
  do i=1,52596
    read(1,*) Tmax(i)
  enddo
  close(1)

endif
!==============================================================================================

z = model % nodes % z (n)
x = model % nodes % x (n)
y = model % nodes % y (n)

i=floor(f/(24*3600))

deg_jour=deg_jour_min+(4300.0-z)*(deg_jour_max-deg_jour_min)/(4300.0-3400.0)


  IF (Tmax(i)-delta_T*(z-200)>0) THEN
    g=(Tmax(i)-delta_T*(z-200))*deg_jour/24.0/3600.0
  ELSE
    g=0.0
  ENDIF

END FUNCTION surf_melting

FUNCTION diffusivity_calc(Model, n, f) RESULT(g)
USE DefUtils
IMPLICIT NONE
TYPE(Model_t) :: Model
TYPE(Variable_t), POINTER :: Dens
INTEGER :: n,i,j,nb,k,cont
REAL(KIND=dp) :: f, g, d, rho_ice, theta, kfirn, ksnow


rho_ice=GetConstReal(Model % Constants, "rho_ice")
Dens => VariableGet( Model % Variables, 'DensRel')
d=Dens % values (Dens % perm (n))*rho_ice


!Calonne 2019

theta=1.0/(1+exp(-2.0*0.02*(d-450.0)))

kfirn=2.107+0.003618*(d-rho_ice)
ksnow=0.024-1.23e-4*d+2.5e-6*d**2

g=(1.0-theta)*ksnow+theta*kfirn


  g=g*(9.828*exp(-5.7e-3*(f+273.15)))/2.1*3600.0*24*365.25
  g=g/(146.3+7.253*(f+273.15))

END FUNCTION diffusivity_calc

FUNCTION rock_diffusivity(Model, n, f) RESULT(g)
USE DefUtils
TYPE(Model_t) :: Model
TYPE(variable_t), POINTER :: Hs
INTEGER :: n
REAL(KIND=dp) :: f, g, cp, rock_k

rock_k=GetConstReal(Model % Constants, 'rock_k')
 cp=146.3+7.253*(f+273.15)
g=rock_k/cp*3600.0*24*365.25

END FUNCTION rock_diffusivity

FUNCTION rho_apparent(Model, n, f) RESULT(g)
USE DefUtils
TYPE(Model_t) :: Model
INTEGER :: n
REAL(KIND=dp) :: f, g, cp, rho_rock, rock_cp

rho_rock=GetConstReal(Model % Constants, 'rho_rock')
rock_cp=GetConstReal(Model % Constants, 'rock_cp')

 cp=146.3+7.253*(f+273.15)

g=rho_rock*rock_cp/cp

END FUNCTION rho_apparent

FUNCTION calc_heat_source(Model, n, f) RESULT(g)
USE DefUtils
TYPE(Model_t) :: Model
TYPE(Variable_t), POINTER :: dens,Qlat
INTEGER :: n
REAL(KIND=dp) :: f, g

dens => VariableGet( Model % Variables, 'DensRel')
Qlat => VariableGet( Model % Variables, 'Qlat' )

g=f*1.0e6/917.0/dens % values (dens % perm (n)) + Qlat % values (Qlat % perm (n))

END FUNCTION calc_heat_source

FUNCTION calc_dens_source(Model, n, f) RESULT(g)
USE DefUtils
TYPE(Model_t) :: Model
TYPE(Variable_t), POINTER :: D
INTEGER :: n
REAL(KIND=dp) :: f, g

D => VariableGet( Model % Variables, 'DensRel')

if (f>0.0) then
f=0.0
endif

g=-f*D % values (D % perm (n))

END FUNCTION calc_dens_source

FUNCTION fluidity_calc(Model, n, f) RESULT(g)
USE DefUtils
TYPE(Model_t) :: Model
TYPE(ValueList_t), POINTER ::Constants
TYPE(Variable_t), POINTER :: Damage,T10m,Depth
INTEGER :: n
REAL(KIND=dp) :: f, g, a,A0,A1,A2,Q1,Q2,temp
logical :: first_time=.true.

save a, first_time

if (first_time) then
  a=GetConstReal(Model % Constants, "Fluidity_correc")
  first_time=.false.
endif

Damage => VariableGet( Model % Variables, 'Damage')
Depth => VariableGet( Model % Variables, 'Depth')
!T10m => VariableGet( Model % Variables, 'TempBellowActiveExp')

!Patterson 2010 :

A0=3.5E-25
Q1=6.0e4
Q2=11.5e4
A1=A0*EXP(Q1/(8.314*263.0))*1e18*3600.0*24*365.25
A2=A0*EXP(Q2/(8.314*263.0))*1e18*3600.0*24*365.25

!if (depth % values (depth % perm(n)) < 17.0) then
!temp=T10m % values (T10m % perm(n))+273.15
!else
temp=f+273.15
!endif


IF (f<263.15) THEN
  g=2.0*A1*exp(-Q1/(8.314*temp))*a
ELSE
  g=2.0*A2*exp(-Q2/(8.314*temp))*a
ENDIF


!Patterson 1994 :

! A0=4.90451E-25
! Q1=6.0e4
! Q2=13.9e4
! A1=A0*EXP(Q1/(8.314*263.0))*1e18*3600.0*24*365.25
! A2=A0*EXP(Q2/(8.314*263.0))*1e18*3600.0*24*365.25

! temp=f+273.15

! IF (f<263.15) THEN
  ! g=2.0*A1*exp(-Q1/(8.314*temp))*a
! ELSE
  ! g=2.0*A2*exp(-Q2/(8.314*temp))*a
! ENDIF



 g=g*(1.0 - Damage % values (Damage % perm (n)))**(-3.0)


END FUNCTION fluidity_calc




FUNCTION accu_calc(Model, n, f) RESULT(g)
USE DefUtils
IMPLICIT NONE
TYPE(Model_t) :: Model
INTEGER :: n,i,j,nb,k,cont
REAL(KIND=dp) :: f, g, D, Depth , x, y, z, mean_accu,a
logical :: first_time=.true.
real,dimension(49051,3) :: accu_l
real,dimension(271,181,3) :: accu

save first_time,accu,a

if (first_time) then
  first_time=.false.
  a=GetConstReal(Model % Constants, "Accu_fact")
  open(1,file='Data/Taco_surf_accu-1.0.dat')
  do i=1,49051
    read(1,*) accu_l(i,1),accu_l(i,2),accu_l(i,3)
  enddo
  close(1)

  cont=0
  do i=1,271
    do j=1,181
      cont=cont+1

      accu(i,j,1)=accu_l(cont,1)
      accu(i,j,2)=accu_l(cont,2)
      accu(i,j,3)=accu_l(cont,3)
    enddo
  enddo

endif

     x = model % nodes % x (n)
     y = model % nodes % y (n)


k=floor((x-accu(1,1,1))/10)+1
j=floor((y-accu(1,1,2))/10)+1

if ((x-accu(1,1,1))/10-floor((x-accu(1,1,1))/10)>0.5) then
k=k+1
endif

if ((y-accu(1,1,2))/10-floor((y-accu(1,1,2))/10)>0.5) then
j=j+1
endif

mean_accu=accu(j,k,3)*(accu(j,k+1,1)-x)*(accu(j+1,k,2)-y)+accu(j,k+1,3)*(x-accu(j,k,1))&
&*(accu(j+1,k,2)-y)+accu(j+1,k,3)*(accu(j,k+1,1)-x)*(y-accu(j,k,2))+accu(j+1,k+1,3)*&
&(x-accu(j,k,1))*(y-accu(j,k,2))
mean_accu=mean_accu/100

g=mean_accu*a

END FUNCTION accu_calc


FUNCTION HydroP (Model, Node, D ) RESULT(Snn)
  USE types
  USE CoordinateSystems
  USE SolverUtils
  USE ElementDescription
  USE DefUtils

  IMPLICIT NONE
  TYPE(Model_t) :: Model
  INTEGER :: Node, bf_id
  TYPE(ValueList_t), POINTER :: bdf_id 
  REAL(KIND=dp) :: Depth, D, Snn, LocalRhog , a
  TYPE(Variable_t), POINTER :: DepthVariable
  REAL(KIND=dp), POINTER :: DepthValues(:)
  INTEGER, POINTER :: DepthPerm(:)
  Logical :: GotIt


  DepthVariable => &
       VariableGet(Model % Variables,'Depth')
  IF ( ASSOCIATED( DepthVariable ) ) THEN
     DepthPerm    => DepthVariable % Perm
     DepthValues => DepthVariable % Values
  ELSE
     WRITE(Message,'(A)') 'Variable Depth not associated'
     CALL FATAL('HydroP', Message)
  END IF

  a = GetConstReal(Model % Constants, "Snn_fact")

  bf_id = ListGetInteger( Model % Bodies(1) % Values,&
    'Body Force',  minv=1, maxv=Model % NumberOFMaterials)
  LocalRhog = GetConstReal( Model % BodyForces(bf_id) % Values, 'Porous Force 3',Gotit)

! bdf_id = GetBodyForce()
! LocalRhog = GetConstReal( bdf_id , 'Porous Force 3',Gotit)

            
  Depth = DepthValues( DepthPerm( Node ) ) 
  Snn = LocalRhog*D*Depth

  Snn = a * Snn
 
END FUNCTION HydroP   

RECURSIVE SUBROUTINE getInitialVolumeFraction(Model,Solver,Timestep,TransientSimulation)
  USE DefUtils
  USE Materialmodels
!-----------------------------------------------------------
  IMPLICIT NONE
!------------ external variables ---------------------------
  TYPE(Model_t)  :: Model
  TYPE(Solver_t), TARGET :: Solver
  LOGICAL :: TransientSimulation
  REAL(KIND=dp) :: Timestep
!------------ internal variables ---------------------------
  INTEGER :: i,cont,j,k
  LOGICAL :: GotIt,first_time=.true.
  CHARACTER(LEN=MAX_NAME_LEN) :: TempName, DensityName, DepthName
  TYPE(ValueList_t), POINTER ::Constants
  TYPE(Variable_t), POINTER :: DensityVariable, DepthVariable, VolumefractionVariable
  REAL(KIND=dp), POINTER :: DensityValues(:), DepthValues(:), VolumefractionValues(:)
  INTEGER, POINTER :: DensityPerm(:), DepthPerm(:), VolumefractionPerm(:)
  REAL(KIND=dp)  :: D, Depth , x, y, z, accu_factor, ini_density, mean_temp, ice_dens, k0 ,k1, z0, h_055,mean_surf
real,dimension(49051,3) :: temp_l,surf_l
real,dimension(271,181,3) :: temp,surf

  !=============================================================================================
!Récupération des données=====================================================================
save temp,first_time,surf

if (first_time) then
  first_time=.false.
 
  open(1,file='Data/temp_surf_demlidar')
  do i=1,49051
    read(1,*) temp_l(i,1),temp_l(i,2),temp_l(i,3)
  enddo
  close(1)

  cont=0
  do i=1,271
    do j=1,181
      cont=cont+1

      temp(i,j,1)=temp_l(cont,1)
      temp(i,j,2)=temp_l(cont,2)
      temp(i,j,3)=temp_l(cont,3)
    enddo
  enddo

  open(1,file='Data/Taco_surf.dat')
  do i=1,49051
    read(1,*) surf_l(i,1),surf_l(i,2),surf_l(i,3)
  enddo
  close(1)

  cont=0
  do i=1,271
    do j=1,181
      cont=cont+1

      surf(i,j,1)=surf_l(cont,1)
      surf(i,j,2)=surf_l(cont,2)
      surf(i,j,3)=surf_l(cont,3)
    enddo
  enddo


endif


  VolumefractionVariable => Solver % Variable
  VolumefractionPerm => VolumefractionVariable % Perm
  VolumefractionValues => VolumefractionVariable % Values

  Constants => GetConstants()
  DensityName = GetString(Constants,'Density Name', GotIt)
  IF (.NOT.GotIt) THEN
     CALL WARN('getInitialRelativeDensity', 'No Keyword >Density Name< defined. Using >Density< as default.')
     WRITE(DensityName,'(A)') 'Density'
  ELSE
     WRITE(Message,'(a,a)') 'Variable Name for density: ', DensityName
     CALL INFO('getInitialRelativeDensity',Message,Level=12)
  END IF
  DensityVariable => &
       VariableGet(Solver % Mesh % Variables,DensityName)
  IF ( ASSOCIATED( DensityVariable ) ) THEN
     DensityPerm    => DensityVariable % Perm
     DensityValues => DensityVariable % Values
  ELSE
    WRITE(Message,'(A,A,A)') 'Variable ',  DensityName, ' not associated'
    CALL FATAL('getInitialRelativeDensity', Message)
  END IF

  DepthVariable => &
       VariableGet(Solver % Mesh % Variables,'Depth')
  IF ( ASSOCIATED( DepthVariable ) ) THEN
     DepthPerm    => DepthVariable % Perm
     DepthValues => DepthVariable % Values
  ELSE
     WRITE(Message,'(A)') 'Variable Depth not associated'
     CALL FATAL('getInitialRelativeDensity', Message)
  END IF



  DO i=1,Model % Mesh % NumberOfNodes
     Depth = DepthValues( DepthPerm( i ) ) 

     x = model % nodes % x (i)
     y = model % nodes % y (i)
     z = model % nodes % z (i)

k=floor((x-temp(1,1,1))/10)+1
j=floor((y-temp(1,1,2))/10)+1

if ((x-temp(1,1,1))/10-floor((x-temp(1,1,1))/10)>0.5) then
k=k+1
endif

if ((y-temp(1,1,2))/10-floor((y-temp(1,1,2))/10)>0.5) then
j=j+1
endif

mean_temp=temp(j,k,3)*(temp(j,k+1,1)-x)*(temp(j+1,k,2)-y)+temp(j,k+1,3)*(x-temp(j,k,1))&
&*(temp(j+1,k,2)-y)+temp(j+1,k,3)*(temp(j,k+1,1)-x)*(y-temp(j,k,2))+temp(j+1,k+1,3)*&
&(x-temp(j,k,1))*(y-temp(j,k,2))
mean_temp=mean_temp/100

mean_surf=surf(j,k,3)*(surf(j,k+1,1)-x)*(surf(j+1,k,2)-y)+surf(j,k+1,3)*(x-surf(j,k,1))&
&*(surf(j+1,k,2)-y)+surf(j+1,k,3)*(surf(j,k+1,1)-x)*(y-surf(j,k,2))+surf(j+1,k+1,3)*&
&(x-surf(j,k,1))*(y-surf(j,k,2))
mean_surf=mean_surf/100


ice_dens=1.09+(4300-mean_surf)*0.2/1000.0

if (mean_surf<3750.0) then
accu_factor=8.7578e-6*mean_surf**2-0.058907*mean_surf+100.11
else
accu_factor=6.8456e-6*mean_surf**2-0.058957*mean_surf+127.19
endif

ini_density=0.35

k0=11.0*EXP(-10160.0/((273.15+mean_temp)*8.314))
k1=575.0*EXP(-21400.0/((273.15+mean_temp)*8.314))
h_055=(1/(ice_dens*k0)*(log(0.55/(ice_dens-0.55))-log(ini_density/(ice_dens-ini_density))))

if (Depth<=h_055) then
z0=EXP((ice_dens*k0*Depth)+log(ini_density/(ice_dens-ini_density)))
D=(ice_dens*z0)/(1+z0)
else
z0=EXP((ice_dens*k1*(Depth-h_055)/((accu_factor)**0.5))+log(0.55/(ice_dens-0.55)))
D=(ice_dens*z0)/(1+z0)
endif

if (D>0.917) then
D=0.917
endif


     DensityValues(DensityPerm(i)) = D/0.917 


  END DO

END SUBROUTINE getInitialVolumeFraction

SUBROUTINE w_calc(Model, Solver, dt, Transient)
Use DefUtils
IMPLICIT NONE
TYPE(Solver_t) :: Solver
TYPE(Model_t) :: Model
TYPE(Element_t), POINTER :: Element 
TYPE(ValueList_t), POINTER :: Material
INTEGER, POINTER :: NodeIndexes(:)
REAL(KIND=dp), ALLOCATABLE :: Density(:)
TYPE(variable_t), POINTER :: Se,S_cap
REAL(KIND=dp) :: dt, rho_w, rho_ice, max_w
LOGICAL :: Transient
INTEGER :: N,i,nb,j
real,dimension(:),allocatable :: dens

logical :: first_time=.true.

SAVE dens,first_time,rho_w,rho_ice
SAVE Density

if (first_time) then
  first_time=.false.

allocate(dens(Model % Mesh % NumberOfNodes)) 
nb = Model % MaxElementNodes
ALLOCATE(Density(nb))

j=Solver % NumberOfActiveElements

do i=1,j
   Element => GetActiveElement(i)
   nb = GetElementNOFNodes(Element)
   NodeIndexes => Element % NodeIndexes
   Material => GetMaterial(Element)
   Density(1:nb) = ListGetReal( Material,'Enthalpy Density', nb, NodeIndexes)
   dens(NodeIndexes)=Density(1:nb)
enddo

rho_w=GetConstReal(Model % Constants, "rho_w")
rho_ice=GetConstReal(Model % Constants, "rho_ice")

endif

S_cap   => VariableGet( Model % Variables, 'S_cap' )
Se   => VariableGet( Model % Variables, 'Se' )
N = Model % NumberOfNodes

print*,'Calculate total water content ...'

do i=1,N
    max_w=rho_w*(1.0-dens(i)/rho_ice)
    Solver % Variable % Values(Solver % Variable % Perm (i)) = (Se % values &
&(Se % perm (i))+S_cap % values (S_cap % perm (i))) * max_w
enddo

END SUBROUTINE w_calc

FUNCTION calc_temp(Model, n, f) RESULT(g)
USE DefUtils
TYPE(Model_t) :: Model
TYPE(variable_t), POINTER :: Hs
INTEGER :: n
REAL(KIND=dp) :: f, g, hm,T0

T0=GetConstReal(Model % Constants, 'T_ref_enthalpy')
Hs   => VariableGet( Model % Variables, 'Phase Change Enthalpy' )
hm=Hs % values (Hs % perm (n))

if (f<hm) then
g=(-146.3+(146.3**2+14.506*(3.626*T0**2+146.3*T0+f))**0.5 ) / 7.253
else
g=(-146.3+(146.3**2+14.506*(3.626*T0**2+146.3*T0+hm))**0.5 ) / 7.253
endif

END FUNCTION calc_temp

FUNCTION calc_enth(Model, n, f) RESULT(g)
USE DefUtils
TYPE(Model_t) :: Model
INTEGER :: n
REAL(KIND=dp) :: f, g, A_cap,B_cap,Tref
logical :: GotIt

A_cap = GetConstReal(Model % Constants, "Enthalpy Heat Capacity A",GotIt)
IF (.NOT.GotIt) THEN
    CALL WARN('EnthalpySolver', 'No Keyword >Enthalpy Heat Capacity A< &
     & defined in model constants. Using >7.253< as default (Paterson, 1994).')
     A_cap = 7.253
END IF
	  
B_cap = GetConstReal(Model % Constants, "Enthalpy Heat Capacity B",GotIt)
IF (.NOT.GotIt) THEN
    CALL WARN('EnthalpySolver', 'No Keyword >Enthalpy Heat Capacity B< & 
    & defined in model constants. Using >146.3< as default (Paterson, 1994).')
     B_cap = 146.3
END IF

Tref = GetConstReal(Model % Constants, "T_ref_enthalpy",GotIt)
IF (.NOT.GotIt) THEN
   CALL WARN('EnthalpySolver', 'No Keyword >Reference Enthalpy< defined. Using >200K< as default.')
   Tref = 200.0
END IF

g=B_cap*(f-Tref)+A_cap/2.0*(f**2-Tref**2)


END FUNCTION calc_enth




