SUBROUTINE percol_1D_solver(Model, Solver, dt, Transient)
Use DefUtils
IMPLICIT NONE
TYPE(Solver_t) :: Solver
TYPE(Model_t) :: Model
TYPE(ValueList_t), POINTER :: BC,Material,SolverParams
TYPE(Element_t), POINTER :: Element 
INTEGER, POINTER :: NodeIndexes(:)
TYPE(variable_t), POINTER :: Enth,Hs,Depth,SE,Damage,Qlat,ThickTemp,SurfEnth
REAL(KIND=dp) :: dt,dz,Sr,rho_w,rho_ice,L_heat,wres,H_lim,cont,NewEnth,PercolCrevDamage,SurfEnthBoundaryDepth
LOGICAL :: Transient,GotIt,Found,PercolBottom
INTEGER :: N,i,j,nb,k,Nsurf
real,dimension(:),allocatable :: dens,fonte
REAL(KIND=dp), ALLOCATABLE :: Density(:),fonte_loc(:)

logical :: first_time=.true.,PercolCrev

SAVE first_time,Sr,rho_w,rho_ice,L_heat,N,Nsurf,PercolCrev,PercolCrevDamage,SurfEnthBoundaryDepth

if (first_time) then
  first_time=.false.

N = Model % NumberOfNodes

!========Get number of surface node===================================================

Depth => VariableGet( Model % Variables, 'Depth')
Nsurf=0
DO i=1,N
IF (Depth % values (Depth % perm (i))==0.0) THEN
Nsurf=Nsurf+1
ENDIF
ENDDO


rho_w=GetConstReal(Model % Constants, "rho_w")
rho_ice=GetConstReal(Model % Constants, "rho_ice")
Sr=GetConstReal(Model % Constants, "Residual Saturation")
L_heat=GetConstReal(Model % Constants, "L_heat")
PercolCrev=GetLogical(Model % Constants, "PercolCrev")
PercolCrevDamage=GetConstReal(Model % Constants, "PercolCrevDamage")
SurfEnthBoundaryDepth=GetConstReal(Model % Constants, "SurfEnthBoundaryDepth")

endif

!==========Density and capacity from materials=======================================


ALLOCATE(dens(N)) 
ALLOCATE(Density(Model % MaxElementNodes))

do i=1,Solver % NumberOfActiveElements

   Element => GetActiveElement(i)
   nb = GetElementNOFNodes(Element)
   NodeIndexes => Element % NodeIndexes
   Material => GetMaterial(Element)
   Density(1:nb) = ListGetReal( Material,'Enthalpy Density', nb, NodeIndexes)
   dens(NodeIndexes)=Density(1:nb)

enddo


!======Get enthalpy and depth================================================

Hs => VariableGet( Model % Variables, 'Phase Change Enthalpy' )
Enth => VariableGet( Model % Variables, 'enthalpy_h')
Qlat => VariableGet( Model % Variables, 'Qlat')
Depth => VariableGet( Model % Variables, 'Depth')
Damage => VariableGet( Model % Variables, 'Damage')
!ThickTemp => VariableGet( Model % Variables, 'Temperate Ice Thick')
SurfEnth => VariableGet( Model % Variables, 'Surf Enth')

!===================================================================================
!========Get surface melting=========================================================
ALLOCATE(fonte_loc(Model % MaxElementNodes))
ALLOCATE(fonte(N)) 

fonte=0.0
fonte_loc=0.0

DO i=1,Model % NumberOfBoundaryElements

   Element => GetBoundaryElement(i)
   nb = GetElementNOFNodes(Element)
   NodeIndexes => Element % NodeIndexes

   BC => GetBC(Element)

   fonte_loc(1:nb) = ListGetReal( BC,'Surf_melt', nb, NodeIndexes,GotIt)
   IF (.NOT.GotIt) THEN
	!fonte(NodeIndexes)=0.0
   ELSE
        fonte(NodeIndexes)=fonte_loc(1:nb)
   END IF

ENDDO

!Initialize Qlat===========================================================================

do i=1,N
Qlat % values (Qlat % perm (i))=0.0
enddo

!===========================================================================================
!=========Loop over surface nodes=========================================================
DO i=1,Nsurf
!Surface melting en kg m-2:
cont=fonte(N+1-i)*rho_w
j=0
PercolBottom=.false.

!=========Loop over verticaly aligned nodes ===============================================
DO WHILE (cont>0.0)


!Curent node number:
	k = N+1-i-j*Nsurf

	IF (SurfEnthBoundaryDepth>Depth % values (Depth % perm (k)))  THEN
		j=j+1
		CYCLE
	ENDIF
	
	IF (Damage % values (Damage % perm (k))>PercolCrevDamage) THEN
		PercolBottom=.true.
	ENDIF

!layer thickness:
	dz = Depth % values (Depth % perm (k-Nsurf))-Depth % values (Depth % perm (k))

	IF (((PercolBottom).and.(PercolCrev)).or.(dens(k)<800.0)) THEN

!===========Residual water content========================================================

		wres=Sr*rho_w*(1.0-dens(k)/rho_ice)
		H_lim = Hs % values (Hs % perm (k)) + wres/dens(k)*L_heat

!================percolation ============================================================

		NewEnth = Enth % values (Enth % perm (k)) + L_heat*cont/dz/dens(k)

		if (NewEnth > H_lim) then
			cont=cont-(H_lim-Enth % values (Enth % perm (k)))*dz*dens(k)/L_heat
			Qlat % values (Qlat % perm (k))= (H_lim-Enth % values (Enth % perm (k)))/dt
		else
			cont=cont-cont
			Qlat % values (Qlat % perm (k))=  L_heat*cont/dz/dens(k)/dt
		endif

	ELSE

	cont=0.0

	ENDIF

	j=j+1

	if (j==Model % NumberOfNodes/Nsurf-1) then
		cont=0.0
	endif

	! if ((Damage % values (Damage % perm (N+1-i))<=PercolCrevDamage).and.(dens(k)>800.0).and.(PercolCrev)) then
		! cont=0.0
	! elseif ((dens(k)>800.0).and.(.not. PercolCrev)) then
		! cont=0.0
	! endif

	!Just in case ...
	if (Qlat % values (Qlat % perm (k))<0.0) then
		Qlat % values (Qlat % perm (k))=0.0
	endif

ENDDO !while

!Compute thickness of temperate ice:

! cont=0.0
! DO j=0,floor(1.0*N/Nsurf)-2
	! k = N+1-i-j*Nsurf
	
    ! H_lim = Hs % values (Hs % perm (k))
	
	! IF (Enth % values (Enth % perm (k))>H_lim) THEN
		! dz = Depth % values (Depth % perm (k-Nsurf))-Depth % values (Depth % perm (k))
		! cont=cont+dz
	! ENDIF

! ENDDO

! ThickTemp % values (ThickTemp % perm (N+1-i)) = cont

ENDDO !surface node


END SUBROUTINE percol_1D_solver
