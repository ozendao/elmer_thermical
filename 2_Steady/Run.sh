#!/bin/bash
#################################
#OAR --name SteadyTaco
#OAR --project elmerice

#OAR -O simu.%jobid%.out
#OAR -E simu.%jobid%.err

#OAR -l nodes=1/core=4,walltime=30:00:00

#################################
# Exit on error
set -e

#################################
# Calcul du nombre de noeuds, et nombre de cores
nbcores=`cat $OAR_NODE_FILE|wc -l`
# Number of nodes
nbnodes=`cat $OAR_NODE_FILE|sort|uniq|wc -l`

echo "############"
echo "Nbre de noeuds: " $nbnodes
echo "Nbre de coeurs: " $nbcores
echo "############"
echo 
#################################
### Parameters ###

### let's go
ulimit -s unlimited
export OMP_NUM_THREADS=1


### Run code ###
mpirun -np $nbcores ElmerSolver_mpi Taco_steady_Damage.sif

