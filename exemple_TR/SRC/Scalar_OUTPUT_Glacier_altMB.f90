!/*****************************************************************************/
! *
! *  Elmer/Ice, a glaciological add-on to Elmer
! *  http://elmerice.elmerfem.org
! *
! *
! *  This program is free software; you can redistribute it and/or
! *  modify it under the terms of the GNU General Public License
! *  as published by the Free Software Foundation; either version 2
! *  of the License, or (at your option) any later version.
! *
! *  This program is distributed in the hope that it will be useful,
! *  but WITHOUT ANY WARRANTY; without even the implied warranty of
! *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! *  GNU General Public License for more details.
! *
! *  You should have received a copy of the GNU General Public License
! *  along with this program (in file fem/GPL-2); if not, write to the
! *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
! *  Boston, MA 02110-1301, USA.
! *
! *****************************************************************************/
! ******************************************************************************
! *
! *  Author: F. Gillet-Chaulet (IGE)
! *  Email:  fabien.gillet-chaulet@univ-grenoble-alpes.fr
! *  Web:    http://elmerice.elmerfem.org
! *
! *  Original Date: 03-2018, 
! *  11-2020
! *  O. Gagliardini (IGE) modified the initial version from Fabien to adpat it for Glacier
! *****************************************************************************
!!! Compute standard 1D variables:
!   1: Time
!   2: Volume
!   3: Volume change
!   4: Area    
!   5: Ablation Area    
!   6: Accumulation Area
!   7: Firn Area
!   8: Bottom Temperate Area    
!   9: SMB Total
!  10: SMB Ablation 
!  11: SMB Accumulation 
!  12: Front elevation
!  13: X Front
!  14: Y Front
!  15: Front distance
!  16: H firn
!  17: X firn
!  18: Y firn
! *****************************************************************************     
      SUBROUTINE Scalar_OUTPUT( Model,Solver,dt,TransientSimulation )
      USE DefUtils
      IMPLICIT NONE
      TYPE(Model_t) :: Model
      TYPE(Solver_t):: Solver
      REAL(KIND=dp) :: dt
      LOGICAL :: TransientSimulation,FoundXRef,FoundYRef,GotIt
      TYPE(Variable_t),POINTER :: SMBVar,HVar,ZsVar,ZsLoads,DzDtVar,EVar,IceMask,MassBalance,Firn,Depth
      TYPE(Variable_t),POINTER :: BottomPresVar,BottomTempVar,BottomVelVar1,BottomVelVar2,BottomVelVar3
      TYPE(Variable_t),POINTER :: TempVar,VelVar1,VelVar2,VelVar3,AccuVar
      INTEGER,POINTER :: Permutation(:)
      TYPE(ValueList_t), POINTER :: SolverParams
      REAL (KIND=DP), POINTER :: AltRange(:,:),SpaceRange(:,:)
      REAL(KIND=dp) :: Volume,Vchange
      REAL (KIND=dp),SAVE :: melt_tol,BINRESZ,BINRESX
      REAL(KIND=dp) :: TotalArea,AblaArea,AccuArea,FirnArea,BottomTempArea
      REAL(KIND=dp) :: TotalSMB,AblaSMB,AccuSMB
      REAL(KIND=dp) :: Hfront,XFront,YFront,XRefFront,YRefFront
      REAL(KIND=dp) :: HFirn,XFirn,YFirn
      REAL(KIND=dp) :: ZsLoadsFlux
      REAL (KIND=dp), ALLOCATABLE, DIMENSION(:),SAVE :: NodeArea
      REAL (KIND=dp), ALLOCATABLE, DIMENSION(:),SAVE :: LocalArea,SpaceBin
      REAL (KIND=dp), ALLOCATABLE, DIMENSION(:),SAVE :: NodalH,MinH,TotalSMBAlt,AreaAlt,BTempAlt,AltitudeBin,&
                                                         TotalSMBSpace,AreaSpace,BTempSpace,avg_Zs,space_index
      REAL (KIND=dp), ALLOCATABLE, DIMENSION(:),SAVE :: NodalFirn,NodalSMB,NodalZs,NodalDzDt
      REAL (KIND=dp), ALLOCATABLE, DIMENSION(:),SAVE :: NodalBottomPresVar,NodalBottomTempVar
      REAL (KIND=dp), ALLOCATABLE, DIMENSION(:),SAVE :: Val_total,Val_binx,Val_binz,Val_nodes,&
                                                        ParVal_total,ParVal_binx,ParVal_binz
      REAL (KIND=dp),ALLOCATABLE,SAVE :: Basis(:), dBasisdx(:,:)
      INTEGER :: i,k
      INTEGER :: ierr
      INTEGER, SAVE :: Nbinz,Nbinx,N_nodes,N_nodes_surf,before_surf
      INTEGER,PARAMETER :: NVal_t=18,NVAL_x=6,NVAL_z=4,NVAL_n=14
      INTEGER, PARAMETER :: io=12
      INTEGER,PARAMETER :: DIM=3 !dimension of the pb restricted to 3 currently
      INTEGER :: FlowDofs
      LOGICAL,SAVE :: Firsttime=.TRUE.,Save_2D
      REAL,SAVE :: beta,Psurf,Ptriple,Ttriple,ZeroCel
      CHARACTER(LEN=MAX_NAME_LEN) :: SolverName='Scalar_OUTPUT_Glacier'
      CHARACTER(LEN=MAX_NAME_LEN),SAVE :: OUTPUT_FName_total,OUTPUT_FName_binx,OUTPUT_FName_binz,OUTPUT_FName_nodes
      character(LEN=25) :: outputstring
      CHARACTER(LEN=MAX_NAME_LEN),ALLOCATABLE,SAVE :: ValueNames_total(:),ValueNames_binx(:),&
                                                      ValueNames_binz(:),ValueNames_nodes(:)

      CALL GET_VARIABLES()
      SolverParams=>GetSolverParams(Solver) 
      IF (Firsttime.OR.Solver%Mesh%Changed) THEN        
        IF (.NOT.ASSOCIATED(Solver%Variable)) & 
         CALL FATAL(SolverName,'Solver%Variable Not associated')
        IF (.NOT.ASSOCIATED(Solver%Matrix)) &
         CALL FATAL(SolverName,'Solver%Matrix Not associated')
        IF ( CurrentCoordinateSystem() /= Cartesian )  &
          CALL FATAL(SolverName,'Only For cartesian system')
        IF ( Model % Mesh % MeshDim /= DIM ) &
          CALL FATAL(SolverName,'Only For 2D plan view')
       !## DO SOME ALLOCATION
        IF (Firsttime) THEN   
          BINRESZ=GetConstReal(SolverParams,'BinResz',Gotit)
          if (.not.GotIt)then
            CALL WARN(SolverName,'Missing keyword BinResz. Using 50 as default')
            BINRESZ=50
          end if
          Save_2D=GetLogical(SolverParams,'Save2D',Gotit)
          if (.not.GotIt)then
            Save_2D=.False.
          endif
          if(Save_2D)then
            BINRESX=GetConstReal(SolverParams,'BinResx',Gotit)  
            if (.not.GotIt)then
              CALL WARN(SolverName,'Missing keyword BinResx. Using 50 as default')
              BINRESX=50
            end if
            N_nodes = Model % NumberOfNodes
            Depth => VariableGet( Model % Variables, 'Depth',UnfoundFatal=.TRUE.)
            N_nodes_surf=0
            DO i=1,N_nodes
              IF (Depth % values (Depth % perm (i))==0.0) THEN
                N_nodes_surf=N_nodes_surf+1
              ENDIF
            ENDDO
            before_surf=N_nodes-N_nodes_surf
          endif
        ENDIF 
        CALL DO_ALLOCATION(Firsttime,Nbinz,Nbinx)
       !## Name of Saved variables
        ValueNames_total(1)='Volume'
        ValueNames_total(2)='Volume Change'
        ValueNames_total(3)='Area'
        ValueNames_total(4)='Ablation Area'
        ValueNames_total(5)='Accumulation Area'
        ValueNames_total(6)='Firn Area'
        ValueNames_total(7)='Bottom Temperate Area'
        ValueNames_total(8)='SMB Total'
        ValueNames_total(9)='SMB Ablation'
        ValueNames_total(10)='SMB Accumulation'
        ValueNames_total(11)='Residual Flux'
        ValueNames_total(12)='Front Elevation'
        ValueNames_total(13)='X Front'
        ValueNames_total(14)='Y Front'
        ValueNames_total(15)='Front Distance'
        ValueNames_total(16)='H Firn'
        ValueNames_total(17)='X Firn'
        ValueNames_total(18)='Y Firn'
        if(save_2D)then
          DO i=1,size(SpaceBin,1)
            ValueNames_binx(1+(i-1)*NVAL_x)='Space bin'
            ValueNames_binx(2+(i-1)*NVAL_x)='Somme Altitudes'
            ValueNames_binx(3+(i-1)*NVAL_x)='Nb_points'
            ValueNames_binx(4+(i-1)*NVAL_x)='SMBAlt Tot'
            ValueNames_binx(5+(i-1)*NVAL_x)='AreaAlt'
            ValueNames_binx(6+(i-1)*NVAL_x)='BTempAlt'
          ENDDO
          ValueNames_nodes(1)='x'
          ValueNames_nodes(2)='Zs'
          ValueNames_nodes(3)='Height'
          ValueNames_nodes(4)='Zs Change'
          ValueNames_nodes(5)='Mass Balance'
          ValueNames_nodes(6)='Accu'
          ValueNames_nodes(7)='Firn'
          ValueNames_nodes(8)='Velocity x surf'
          ValueNames_nodes(9)='Velocity z surf'
          ValueNames_nodes(10)='Temp surf'
          ValueNames_nodes(11)='Temp bottom'
          ValueNames_nodes(12)='Pres bottom'
          ValueNames_nodes(13)='Velocity x bottom'
          ValueNames_nodes(14)='Velocity z bottom'
        endif
        DO i=1,size(AltitudeBin,1)
          ValueNames_binz(1+(i-1)*NVAL_z)='Altitude bin'
          ValueNames_binz(2+(i-1)*NVAL_z)='SMBAlt Tot'
          ValueNames_binz(3+(i-1)*NVAL_z)='AreaAlt'
          ValueNames_binz(4+(i-1)*NVAL_z)='BTempAlt'
        ENDDO
        IF (Firsttime) then
          beta = GetConstReal(Model % Constants, "beta_clapeyron",GotIt)
          IF (.NOT.GotIt) THEN
             CALL WARN(SolverName,'No Keyword >beta_clapeyron< defined. Using >9.74 10-2 K Mpa-1< as default.')
             beta = 0.0974
          END IF
          Psurf = GetConstReal(Model % Constants, "P_surf",GotIt)
          IF (.NOT.GotIt) THEN
             CALL WARN(SolverName,'No Keyword >Psurf< defined. Using >1.013 10-1 MPa < as default.')
             Psurf = 0.1013
          END IF  
          Ptriple = GetConstReal(Model % Constants,"P_triple",GotIt)
          IF (.NOT.GotIt) THEN
             CALL WARN(SolverName,'No Keyword >P_triple< defined. Using >0.00061173 MPa < as default.')
             Ptriple = 0.00061173
          END IF
          Ttriple = GetConstReal(Model % Constants,"T_triple",GotIt)
          IF (.NOT.GotIt) THEN
             CALL WARN(SolverName,'No Keyword >T_triple< defined. Using > 273.16 K < as default.')
             Ttriple=273.16
          END IF
          ZeroCel=273.15
          melt_tol=GetConstReal(SolverParams,'MeltTol',Gotit)
          if (.not.GotIt)then
            CALL WARN(SolverName,'Missing keyword MeltTol. Using 1e-5 as default')
            melt_tol=1e-5
          end if
          CALL INIT_OUTPUT_FILE(OUTPUT_FName_total,OUTPUT_FName_binx,OUTPUT_FName_binz,OUTPUT_FName_nodes)
          CALL COMPUTE_NodeArea(NodeArea)
          Firsttime=.FALSE.
        END IF             
      END IF
  
      XRefFront = GetConstReal(SolverParams,'XRefFrontPosition',FoundXRef)
      YRefFront = GetConstReal(SolverParams,'YRefFrontPosition',FoundYRef)
      AltRange => ListGetConstRealArray(SolverParams,'SMB altitude Range')
      AltitudeBin= (/(i, i=floor(AltRange(1,1)),floor(AltRange(2,1)), INT(BINRESZ))/)
      if(save_2D)then
        SpaceRange => ListGetConstRealArray(SolverParams,'SMB space Range')
        SpaceBin= (/(i, i=floor(SpaceRange(1,1)),floor(SpaceRange(2,1)), INT(BINRESX))/)
      endif

      CALL BODY_INTEGRATION(Volume,TotalArea,AblaARea,AccuArea,FirnArea,BottomTempArea,TotalSMB, &
                            AblaSMB,AccuSMB,Hfront,XFront,YFront,ZsLoadsFlux,Vchange, &
                            TotalSMBAlt,AreaAlt,BTempAlt,AltitudeBin,SpaceBin,HFirn,XFirn,YFirn,&
                            TotalSMBSpace,AreaSpace,BTempSpace,avg_Zs,space_index) 

      Val_total(1)=Volume
      Val_total(2)=Vchange
      Val_total(3)=TotalArea
      Val_total(4)=AblaArea
      Val_total(5)=AccuArea
      Val_total(6)=FirnArea
      Val_total(7)=BottomTempArea
      Val_total(8)=TotalSMB
      Val_total(9)=AblaSMB
      Val_total(10)=AccuSMB
      Val_total(11)= ZsLoadsFlux
      Val_total(12)= Hfront
      Val_total(13)= Xfront
      Val_total(14)= Yfront
      if (FoundYRef.and.FoundXRef) then
        Val_total(15)=sqrt((Val_total(13)-XRefFront)**2+(Val_total(14)-YRefFront)**2)
      else
        Val_total(15)=0.0
      endif
       
      Val_total(16)=HFirn
      Val_total(17)=XFirn
      Val_total(18)=YFirn  
      DO i=1,Nbinz
        Val_binz(1+(i-1)*NVAL_z)=AltitudeBin(i)
        Val_binz(2+(i-1)*NVAL_z)=TotalSMBAlt(i)
        Val_binz(3+(i-1)*NVAL_z)=AreaAlt(i)
        Val_binz(4+(i-1)*NVAL_z)=BTempAlt(i)
      ENDDO
      if(save_2D)then
        DO i=1,Nbinx
          Val_binx(1+(i-1)*NVAL_x)=SpaceBin(i) 
          Val_binx(2+(i-1)*NVAL_x)=avg_Zs(i)
          Val_binx(3+(i-1)*NVAL_x)=space_index(i)
          Val_binx(4+(i-1)*NVAL_x)=TotalSMBSpace(i)
          Val_binx(5+(i-1)*NVAL_x)=AreaSpace(i)
          Val_binx(6+(i-1)*NVAL_x)=BTempSpace(i)
        ENDDO
      endif
      IF (ParEnv % PEs > 1 ) THEN
        DO i=1,11
          CALL MPI_ALLREDUCE(Val_total(i),ParVal_total(i),1,&
                       MPI_DOUBLE_PRECISION,MPI_SUM,ELMER_COMM_WORLD,ierr) 
        END DO
        CALL MPI_ALLREDUCE(Val_total(12),ParVal_total(12),1,&
                       MPI_DOUBLE_PRECISION,MPI_MIN,ELMER_COMM_WORLD,ierr)
        if (Val_total(15).eq.huge(1.0)) then
           Val_total(15)=0._dp
        end if
        CALL MPI_ALLREDUCE(Val_total(15),ParVal_total(15),1,&
                       MPI_DOUBLE_PRECISION,MPI_MIN,ELMER_COMM_WORLD,ierr)
        CALL MPI_ALLREDUCE(Val_total(16),ParVal_total(16),1,&
                       MPI_DOUBLE_PRECISION,MPI_MIN,ELMER_COMM_WORLD,ierr)
        IF (Val_total(12).NE.ParVal_total(12)) THEN
          Val_total(13) = Huge(1.0)
          Val_total(14) = Huge(1.0)
        END IF
        IF (Val_total(16).NE.ParVal_total(16)) THEN
          Val_total(17) = Huge(1.0)
          Val_total(18) = Huge(1.0)
        END IF
        DO i=13,14
          CALL MPI_ALLREDUCE(Val_total(i),ParVal_total(i),1,&
                      MPI_DOUBLE_PRECISION,MPI_MIN,ELMER_COMM_WORLD,ierr)
        END DO
        DO i=17,18
          CALL MPI_ALLREDUCE(Val_total(i),ParVal_total(i),1,&
                      MPI_DOUBLE_PRECISION,MPI_MIN,ELMER_COMM_WORLD,ierr)
        END DO  
        DO i=1,NVAL_z*Nbinz
          IF (mod(i,NVAL_z)==1) THEN
            ParVal_binz(i)=Val_binz(i)
          ELSE
            CALL MPI_ALLREDUCE(Val_binz(i),ParVal_binz(i),1,&
                      MPI_DOUBLE_PRECISION,MPI_SUM,ELMER_COMM_WORLD,ierr)
          ENDIF
        END DO 
        if(save_2D)then
          DO i=1,NVAL_x*Nbinx
            IF (mod(i,NVAL_x)==1) THEN
              ParVal_binx(i)=Val_binx(i)
            ELSE
              CALL MPI_ALLREDUCE(Val_binx(i),ParVal_binx(i),1,&
                         MPI_DOUBLE_PRECISION,MPI_SUM,ELMER_COMM_WORLD,ierr)
            ENDIF
          END DO
          Val_binx(1:NVal_x*Nbinx)=ParVal_binx(1:NVal_x*Nbinx)
        endif
        Val_total(1:NVal_t)=ParVal_total(1:NVal_t)
        Val_binz(1:NVal_z*Nbinz)=ParVal_binz(1:NVal_z*Nbinz)
      END IF
      IF ((ParEnv % PEs > 1 ).AND.(ParEnv % MyPe.NE.0)) RETURN

      IF( Solver % TimesVisited > 0 ) THEN
        OPEN(io,file=TRIM(OUTPUT_FName_total),position='append')
      ELSE
        OPEN(io,file=TRIM(OUTPUT_FName_total))
      END IF
      WRITE(outputstring,'(SP,E25.16E3)') GetTime()
      outputstring=adjustl(outputstring)
      write(io,'(A24)',advance='no') outputstring
      Do i=1,NVal_t-1
        WRITE(io,'(SP,E25.16E3)',advance='no') Val_total(i)
      End do
        WRITE(io,'(SP,E25.16E3)') Val_total(NVal_t)
      CLOSE(io)

      if(save_2D)then
        IF( Solver % TimesVisited > 0 ) THEN
          OPEN(io,file=TRIM(OUTPUT_FName_binx),position='append')
        ELSE
          OPEN(io,file=TRIM(OUTPUT_FName_binx))
        END IF
        WRITE(outputstring,'(SP,E25.16E3)') GetTime()
        outputstring=adjustl(outputstring)
        write(io,'(A24)',advance='no') outputstring
        Do i=1,(NVal_x*Nbinx)-1
          WRITE(io,'(SP,E25.16E3)',advance='no') Val_binx(i)
        End do
        WRITE(io,'(SP,E25.16E3)') Val_binx(NVAL_x*Nbinx)
        CLOSE(io)       
      endif
      
      IF( Solver % TimesVisited > 0 ) THEN
        OPEN(io,file=TRIM(OUTPUT_FName_binz),position='append')
      ELSE
        OPEN(io,file=TRIM(OUTPUT_FName_binz))
      END IF
      WRITE(outputstring,'(SP,E25.16E3)') GetTime()
      outputstring=adjustl(outputstring)
      write(io,'(A24)',advance='no') outputstring
      Do i=1,(NVal_z*Nbinz)-1
        WRITE(io,'(SP,E25.16E3)',advance='no') Val_binz(i)
      End do
        WRITE(io,'(SP,E25.16E3)') Val_binz(NVal_z*Nbinz)
      CLOSE(io)
      if(save_2D) then  
        IF( Solver % TimesVisited > 0 ) THEN
          OPEN(io,file=TRIM(OUTPUT_FName_nodes),position='append')
        ELSE
          OPEN(io,file=TRIM(OUTPUT_FName_nodes))
        END IF
        DO i=1,INT(N_nodes_surf/2.0)
          Val_nodes(1)=(model % nodes % x(before_surf+i)+&
          model % nodes % x(before_surf+i+INT(N_nodes_surf/2.0)))/2
          Val_nodes(2)=(ZsVar% values (ZsVar % perm (before_surf+i))+&
          ZsVar% values (ZsVar % perm (before_surf+i+INT(N_nodes_surf/2.0))))/2.0
          Val_nodes(3)=(HVar% values (HVar % perm (before_surf+i))+&
          HVar% values (HVar % perm (before_surf+i+INT(N_nodes_surf/2.0))))/2.0
          Val_nodes(4)=(DzDtVar% values (DzDtVar % perm (before_surf+i))+&
          DzDtVar% values (DzDtVar % perm (before_surf+i+INT(N_nodes_surf/2.0))))/2.0
          Val_nodes(5)=(SMBVar% values (SMBVar % perm (before_surf+i))+&
          SMBVar% values (SMBVar % perm (before_surf+i+INT(N_nodes_surf/2.0))))/2.0
          Val_nodes(6)=(AccuVar% values (AccuVar % perm (before_surf+i))+&
          AccuVar% values (AccuVar % perm (before_surf+i+INT(N_nodes_surf/2.0))))/2.0
          Val_nodes(7)=(Firn% values (Firn % perm (before_surf+i))+&
          Firn% values (Firn % perm (before_surf+i+INT(N_nodes_surf/2.0))))/2.0
          Val_nodes(8)=(VelVar1% values (VelVar1 % perm (before_surf+i))+&
          VelVar1% values (VelVar1 % perm (before_surf+i+INT(N_nodes_surf/2.0))))/2.0
          Val_nodes(9)=(VelVar3% values (VelVar3 % perm (before_surf+i))+&
          VelVar3% values (VelVar3 % perm (before_surf+i+INT(N_nodes_surf/2.0))))/2.0
          Val_nodes(10)=(TempVar% values (TempVar % perm (before_surf+i))+&
          TempVar% values (TempVar % perm (before_surf+i+INT(N_nodes_surf/2.0))))/2.0
          Val_nodes(11)=(BottomTempVar% values (BottomTempVar % perm (before_surf+i))+&
          BottomTempVar% values (BottomTempVar % perm (before_surf+i+INT(N_nodes_surf/2.0))))/2.0
          Val_nodes(12)=(BottomPresVar% values (BottomPresVar % perm (before_surf+i))+&
          BottomPresVar% values (BottomPresVar % perm (before_surf+i+INT(N_nodes_surf/2.0))))/2.0
          Val_nodes(13)=(BottomVelVar1% values (BottomVelVar1 % perm (before_surf+i))+&
          BottomVelVar1% values (BottomVelVar1 % perm (before_surf+i+INT(N_nodes_surf/2.0))))/2.0
          Val_nodes(14)=(BottomVelVar3% values (BottomVelVar3% perm (before_surf+i))+&
          BottomVelVar2% values (BottomVelVar3% perm (before_surf+i+INT(N_nodes_surf/2.0))))/2.0          
          WRITE(outputstring,'(SP,E25.16E3)') GetTime()
          outputstring=adjustl(outputstring)
          write(io,'(A24)',advance='no') outputstring
          Do k=1,NVal_n-1
            WRITE(io,'(SP,E25.16E3)',advance='no') Val_nodes(k)
          End do
            WRITE(io,'(SP,E25.16E3)') Val_nodes(NVal_n)
        ENDDO
        CLOSE(io) 
      endif
      CONTAINS

      SUBROUTINE DO_ALLOCATION(Firsttime,Nbinz,Nbinx)
      LOGICAL,INTENT(IN) :: Firsttime
      LOGICAL :: Found
      INTEGER :: M
      INTEGER :: N
      INTEGER,intent(out) :: Nbinz,Nbinx
      REAL (KIND=DP), POINTER :: AltRange(:,:),SpaceRange(:,:)
      TYPE(ValueList_t), POINTER :: SolverParams
 
      IF (.NOT.Firsttime) &
        DEALLOCATE(NodalH,NodalZs,MinH,NodalSMB,NodalDzDt,&
                      NodalBottomPresVar,NodalBottomTempVar,LocalArea, &
                      Basis,dBasisdx,Val_nodes,&
                      Val_total,Val_binx,Val_binz,ParVal_total,ParVal_binx,ParVal_binz,&
                      ValueNames_total,ValueNames_binx,ValueNames_binz,ValueNames_nodes,&
                      NodeArea,TotalSMBAlt,AreaAlt,BTempAlt,AltitudeBin,SpaceBin,&
                      TotalSMBSpace,AreaSpace,BTempSpace,avg_Zs,space_index)
      N=Model % Mesh % NumberOfNodes
      M=Model % MaxElementNodes  
      SolverParams=>GetSolverParams(Solver) 
      AltRange => ListGetConstRealArray(SolverParams,'SMB altitude Range',Found)
      Nbinz = floor((AltRange(2,1)-AltRange(1,1))/BINRESZ)+1  
      IF (.NOT.Found) &
        CALL FATAL(SolverName,'SMB altitude Range should be provided in scalar output solver')
      if(save_2D)then
        SpaceRange => ListGetConstRealArray(SolverParams,'SMB space Range',Found)
        IF (.NOT.Found) &
           CALL FATAL(SolverName,'SMB space Range should be provided in scalar output solver')
        Nbinx = floor((SpaceRange(2,1)-SpaceRange(1,1))/BINRESX)+1
      else
        Nbinx=2
      endif
      ALLOCATE(Basis(M),&
                MinH(M),&
                dBasisdx(M,3),&
                NodalH(M),NodalZs(M),NodalBottomPresVar(M),NodalBottomTempVar(M),&
                NodalSMB(M),NodalDzDt(M),&
                LocalArea(M),NodalFirn(M),Val_nodes(NVal_n),&
                Val_total(NVal_t),Val_binx(NVal_x*Nbinx),Val_binz(NVal_z*Nbinz),ValueNames_nodes(NVal_n),&
                ParVal_total(NVAL_t),ParVal_binx(NVAL_x*Nbinx),ParVal_binz(NVAL_z*Nbinz),&
                ValueNames_total(NVal_t),ValueNames_binx(NVAL_x*Nbinx),ValueNames_binz(NVAL_z*Nbinz),&
                NodeArea(N),TotalSMBAlt(Nbinz),AreaAlt(Nbinz),BTempAlt(NBinz),AltitudeBin(Nbinz),SpaceBin(Nbinx),&
                TotalSMBSpace(Nbinx),AreaSpace(Nbinx),BTempSpace(Nbinx),avg_Zs(Nbinx),space_index(Nbinx))
      END SUBROUTINE DO_ALLOCATION

      SUBROUTINE INIT_OUTPUT_FILE(OUTPUT_FName_total,OUTPUT_FName_binx,OUTPUT_FName_binz,OUTPUT_FName_nodes)
      USE GeneralUtils
      IMPLICIT NONE
      CHARACTER(LEN=MAX_NAME_LEN),INTENT(OUT) :: OUTPUT_FName_total,OUTPUT_FName_binz,&
                 OUTPUT_FName_binx,OUTPUT_FName_nodes
      CHARACTER(LEN=MAX_NAME_LEN) ::NamesFile_total,NamesFile_binx,NamesFile_binz,OUTPUT_FName,&
                 OUTPUT_FName_D='glacier_Scalar_OUTPUT',NamesFile_nodes
      CHARACTER(LEN=MAX_NAME_LEN) :: DateStr 
      TYPE(ValueList_t), POINTER :: SolverParams
      LOGICAL :: Found
      INTEGER :: i
      SolverParams=>GetSolverParams(Solver)
      OUTPUT_FName = ListGetString(SolverParams,'File Name',Found)
      IF (.NOT.Found) OUTPUT_FName=OUTPUT_FName_D
      OUTPUT_FName_total=TRIM(OUTPUT_FName) // TRIM("_total.dat")
      OUTPUT_FName_binx=TRIM(OUTPUT_FName) // TRIM("_binx.dat")
      OUTPUT_FName_binz=TRIM(OUTPUT_FName) // TRIM("_binz.dat")
      OUTPUT_FName_nodes=TRIM(OUTPUT_FName) // TRIM("_nodes.dat")
      NamesFile_total = TRIM(OUTPUT_FName_total) // '.' // TRIM("names")
      NamesFile_binx = TRIM(OUTPUT_FName_binx) // '.' // TRIM("names")
      NamesFile_binz = TRIM(OUTPUT_FName_binz) // '.' // TRIM("names")
      NamesFile_nodes = TRIM(OUTPUT_FName_nodes) // '.' // TRIM("names")
      IF ((ParEnv % PEs >1).AND.(ParEnv%MyPe.NE.0)) RETURN
      DateStr = FormatDate()

      OPEN(io,file=TRIM(NamesFile_total))
      WRITE(io,'(A)') 'File started at: '//TRIM(DateStr)
      WRITE(io,'(A)') ' '
      WRITE(io,'(A)') 'Elmer version: '//TRIM(GetVersion())
      WRITE(io,'(A)') 'Elmer revision: '//TRIM(GetRevision())
      WRITE(io,'(A)') 'Elmer Compilation Date: '//TRIM(GetCompilationDate())
      WRITE(io,'(A)') ' '
      WRITE(io,'(A)') 'Variables in columns of matrix:'//TRIM(OUTPUT_FName_total)
      WRITE(io,'(I4,": ",A)') 1,'Time'
      DO i=1,NVal_t
        WRITE(io,'(I4,": ",A)') i+1,TRIM(ValueNames_total(i))
      END DO
      CLOSE(io)
      if(save_2D)then
        OPEN(io,file=TRIM(NamesFile_binx))
        WRITE(io,'(A)') 'File started at: '//TRIM(DateStr)
        WRITE(io,'(A)') ' '
        WRITE(io,'(A)') 'Elmer version: '//TRIM(GetVersion())
        WRITE(io,'(A)') 'Elmer revision: '//TRIM(GetRevision())
        WRITE(io,'(A)') 'Elmer Compilation Date: '//TRIM(GetCompilationDate())
        WRITE(io,'(A)') ' '
        WRITE(io,'(A)') 'Variables in columns of matrix:'//TRIM(OUTPUT_FName_binx)
        WRITE(io,'(I4,": ",A)') 1,'Time'
        DO i=1,NVAL_x*Nbinx
          WRITE(io,'(I4,": ",A)') i+1,TRIM(ValueNames_binx(i))
        END DO
        CLOSE(io)
        OPEN(io,file=TRIM(NamesFile_nodes))
        WRITE(io,'(A)') 'File started at: '//TRIM(DateStr)
        WRITE(io,'(A)') ' '
        WRITE(io,'(A)') 'Elmer version: '//TRIM(GetVersion())
        WRITE(io,'(A)') 'Elmer revision: '//TRIM(GetRevision())
        WRITE(io,'(A)') 'Elmer Compilation Date: '//TRIM(GetCompilationDate())
        WRITE(io,'(A)') ' '
        WRITE(io,'(A)') 'Variables in columns of matrix:'//TRIM(OUTPUT_FName_nodes)
        WRITE(io,'(I4,": ",A)') 1,'Time'
        DO i=1,NVal_n
          WRITE(io,'(I4,": ",A)') i+1,TRIM(ValueNames_nodes(i))
        END DO
        CLOSE(io)
      endif
      OPEN(io,file=TRIM(NamesFile_binz))
      WRITE(io,'(A)') 'File started at: '//TRIM(DateStr)
      WRITE(io,'(A)') ' '
      WRITE(io,'(A)') 'Elmer version: '//TRIM(GetVersion())
      WRITE(io,'(A)') 'Elmer revision: '//TRIM(GetRevision())
      WRITE(io,'(A)') 'Elmer Compilation Date: '//TRIM(GetCompilationDate())
      WRITE(io,'(A)') ' '
      WRITE(io,'(A)') 'Variables in columns of matrix:'//TRIM(OUTPUT_FName_binz)
      WRITE(io,'(I4,": ",A)') 1,'Time'
      DO i=1,NVAL_z*Nbinz
        WRITE(io,'(I4,": ",A)') i+1,TRIM(ValueNames_binz(i))
      END DO
      CLOSE(io)
      END SUBROUTINE INIT_OUTPUT_FILE

      SUBROUTINE GET_VARIABLES()
      LOGICAL :: Found
      CHARACTER(LEN=MAX_NAME_LEN) :: PVarName
      TYPE(ValueList_t), POINTER :: SolverParams

      SolverParams=>GetSolverParams(Solver)
      Firn    => VariableGet(Solver%Mesh%Variables,'Firn',UnfoundFatal=.TRUE.)
      HVar    => VariableGet(Solver%Mesh%Variables,'Height',UnfoundFatal=.TRUE.)
      ZsVar => VariableGet(Solver%Mesh%Variables,'Zs Top',UnfoundFatal=.TRUE.)
      ZsLoads => VariableGet(Solver%Mesh%Variables,'Zs Top Loads',UnfoundFatal=.TRUE.)
      DzDtVar => VariableGet(Solver%Mesh%Variables,'Zs Top Velocity',UnfoundFatal=.TRUE.)
      SMBVar => VariableGet(Solver%Mesh%Variables,'Mass Balance',UnfoundFatal=.TRUE.)
      AccuVar => VariableGet(Solver%Mesh%Variables,'Accu',UnfoundFatal=.TRUE.)
      Permutation => Solver%Variable%Perm
      BottomTempVar    => VariableGet(Solver%Mesh%Variables,'OutBedTemp',UnfoundFatal=.TRUE.) 
      TempVar    => VariableGet(Solver%Mesh%Variables,'Temperature',UnfoundFatal=.TRUE.) 
      VelVar1    => VariableGet(Solver%Mesh%Variables,'Velocity 1',UnfoundFatal=.TRUE.) 
      VelVar2    => VariableGet(Solver%Mesh%Variables,'Velocity 2',UnfoundFatal=.TRUE.) 
      VelVar3    => VariableGet(Solver%Mesh%Variables,'Velocity 3',UnfoundFatal=.TRUE.) 
      BottomPresVar => VariableGet(Solver % Mesh %Variables,'OutBedPres',UnfoundFatal=.TRUE.)
      BottomVelVar1 => VariableGet(Solver % Mesh %Variables,'OutBedVel1',UnfoundFatal=.TRUE.)
      BottomVelVar2 => VariableGet(Solver % Mesh %Variables,'OutBedVel2',UnfoundFatal=.TRUE.)
      BottomVelVar3 => VariableGet(Solver % Mesh %Variables,'OutBedVel3',UnfoundFatal=.TRUE.)
      PVarName = ListGetString(SolverParams,'Passive Variable Name',Found)
      IF (Found) THEN
        EVar => VariableGet(Solver%Mesh%Variables,TRIM(PVarName),UnFoundFatal=.TRUE.)
        IF (EVar % Type .NE. Variable_on_elements) &
          CALL FATAL(TRIM(SolverName),'Passive Variable should be on elements')
          EVar % Values = +1._dp
      ELSE
        EVar => NULL()
      END IF
      IceMask => VariableGet(Solver%Mesh%Variables,'IcyMask',UnfoundFatal=.TRUE.)
      END SUBROUTINE GET_VARIABLES

      SUBROUTINE COMPUTE_NodeArea(NodeArea)
      IMPLICIT NONE
      REAL(KIND=dp),INTENT(OUT) :: NodeArea(:)
      TYPE(Element_t), POINTER :: Element
      TYPE(Nodes_t),SAVE :: ElementNodes
      TYPE(GaussIntegrationPoints_t) :: IntegStuff
      REAL(KIND=dp) :: U,V,W,SqrtElementMetric
      INTEGER,POINTER :: Indexes(:)
      INTEGER :: n
      INTEGER :: t,i
      LOGICAL :: stat
      NodeArea=0._dp
      Do t=1,GetNOFActive()
        Element => GetActiveElement(t)
        n = GetElementNOFNodes(Element)
        Indexes => Element % NodeIndexes
        CALL GetElementNodes( ElementNodes, Element )
        ElementNodes % z =0._dp
        IntegStuff = GaussPoints( Element )
        Do i=1,IntegStuff % n
          U = IntegStuff % u(i)
          V = IntegStuff % v(i)
          W = 0._dp ! IntegStuff % w(i)
          stat = ElementInfo(Element,ElementNodes,U,V,W,SqrtElementMetric, &
                      Basis,dBasisdx )
          NodeArea(Permutation(Indexes(1:n)))=NodeArea(Permutation(Indexes(1:n)))+&
              SqrtElementMetric*IntegStuff % s(i) * Basis(1:n)
        End do
      End do
      IF (ParEnv % PEs > 1 ) CALL ParallelSumVector( Solver % Matrix, NodeArea, 0 )
      END SUBROUTINE COMPUTE_NodeArea

      SUBROUTINE BODY_INTEGRATION(Volume,TotalArea,AblaARea,AccuArea,FirnArea,BottomTempArea,TotalSMB, &
                                  AblaSMB,AccuSMB,Hfront,Xfront,Yfront,ZsLoadsFlux,Vchange, &
                                  TotalSMBAlt,AreaAlt,BTempAlt,AltitudeBin,SpaceBin,Hfirn,XFirn,YFirn,&
                                  TotalSMBSpace,AreaSpace,BTempSpace,avg_Zs,space_index)
      IMPLICIT NONE
      REAL(KIND=dp),INTENT(OUT) :: Volume,TotalArea,&
                       AblaArea,AccuArea,FirnArea,BottomTempArea,TotalSMB,AblaSMB,AccuSMB,&
                       Hfront,Xfront,Yfront,ZsLoadsFlux,Vchange,Hfirn,XFirn,YFirn
      REAL(KIND=dp),dimension(:),INTENT(OUT) :: TotalSMBAlt,AreaAlt,BTempAlt,&
                       TotalSMBSpace,AreaSpace,BTempSpace,avg_Zs,space_index
      REAL(KIND=dp),dimension(:),INTENT(IN) :: AltitudeBin,SpaceBin
      REAL(KIND=dp),parameter :: Fsmall=100.0*EPSILON(1.0)
      TYPE(Mesh_t),POINTER :: Mesh
      TYPE(Element_t),POINTER :: Element
      TYPE(ValueList_t), POINTER :: Material,BodyForce
      TYPE(GaussIntegrationPoints_t) :: IntegStuff
      TYPE(Nodes_t),SAVE :: ElementNodes
      TYPE(ValueList_t), POINTER :: SolverParams
      REAL(KIND=dp) :: U,V,W,SqrtElementMetric
      REAL(KIND=dp) :: cellarea,XRefFront,YRefFront
      REAL(KIND=dp) :: HAtIP,SMBAtIP,BottomPresAtIP,BottomTempAtIP,FirnAtIP
      REAL(KIND=dp) :: s
      LOGICAL :: IceFree
      LOGICAL :: stat
      LOGICAL :: Found
      INTEGER,POINTER :: NodeIndexes(:)
      INTEGER :: t
      INTEGER :: i,k
      INTEGER :: n, nl
      INTEGER :: ne, nf

      ne=GetNOFActive()
      Volume=0._dp
      TotalArea = 0._dp
      AblaArea = 0._dp
      AccuArea = 0._dp
      FirnArea = 0._dp
      BottomTempArea = 0._dp
      TotalSMB = 0._dp
      AblaSMB = 0._dp
      AccuSMB = 0._dp
      TotalSMBAlt = 0._dp
      AreaAlt = 0._dp
      BTempAlt = 0._dp
      TotalSMBSpace = 0._dp
      AreaSpace = 0._dp
      BTempSpace = 0._dp
      Avg_Zs=0._dp
      space_index=0._dp
      Hfront = Huge(1.0)
      Hfirn  = Huge(1.0)
      ZsLoadsFlux = 0._dp
      Vchange = 0._dp

      DO t = 1,ne
        Element => GetActiveElement(t)
        n = GetElementNOFNodes(Element)
        NodeIndexes => Element % NodeIndexes
        CALL GetElementNodes( ElementNodes )
        ElementNodes % z = 0._dp
        IF ( CheckPassiveElement(Element) ) THEN
          IF (ASSOCIATED(EVar)) &
            EVar % Values (EVar % Perm (element % elementIndex)) = -1.0_dp
            CYCLE
        ENDIF
        Material => GetMaterial(Element,Found)
        IF (.NOT.Found) &
          CALL FATAL(SolverName,'No Material Found')
        BodyForce => GetBodyForce( Element, Found )
        IF (.NOT.Found) &
          CALL FATAL(SolverName,'No Body Force Found')
        NodalH(1:n) = HVar%Values(HVar%Perm(NodeIndexes(1:n)))
        NodalZs(1:n) = ZsVar%Values(ZsVar%Perm(NodeIndexes(1:n)))
        NodalFirn(1:n) = Firn%Values(Firn%Perm(NodeIndexes(1:n)))
        NodalDzDt(1:n) = DzDtVar%Values(DzDtVar%Perm(NodeIndexes(1:n)))
        NodalSMB(1:n) = SMBVar%Values(SMBVar%Perm(NodeIndexes(1:n)))
        NodalBottomPresVar(1:n)= BottomPresVar%Values(BottomPresVar%Perm(NodeIndexes(1:n)))
        NodalBottomTempVar(1:n)=BottomTempVar%Values(BottomTempVar%Perm(NodeIndexes(1:n)))
        !NodalSMB(1:n) = ListGetReal(BodyForce, 'Zs Top Accumulation Flux 3',n,NodeIndexes,UnfoundFatal=.TRUE.)
        !CELL IS NOT ACTIVE ALL H VALUES BELOW MinH Value
        IceFree=.FALSE.
        ! IF (ALL((NodalH(1:n)-MinH(1:n)).LE.Fsmall).AND.ALL(NodalSMB(1:n).LT.0._dp)) IceFree=.TRUE.
        IF (ALL(IceMask%Values(IceMask%Perm(NodeIndexes(1:n)))<0.1)) IceFree=.TRUE.
        ! GO TO INTEGRATION
        cellarea=0._dp
        LocalArea=0._dp
        IntegStuff = GaussPoints( Element )
        DO i=1,IntegStuff % n
          U = IntegStuff % u(i)
          V = IntegStuff % v(i)
          W = 0._dp  !IntegStuff % w(i)
          stat = ElementInfo(Element,ElementNodes,U,V,W,SqrtElementMetric, &
                       Basis,dBasisdx )
          s=SqrtElementMetric*IntegStuff % s(i)
          ! cell area
          cellarea=cellarea+s
          ! the area seen by each node
          LocalArea(1:n)=LocalArea(1:n)+ s * Basis(1:n)
          IF (IceFree) CYCLE
          ! Integrate H
          HAtIP=SUM(NodalH(1:n)*Basis(1:n))
          FirnAtIP=SUM(NodalFirn(1:n)*Basis(1:n))
          Volume=Volume+HAtIP*s
          Vchange=Vchange+SUM(NodalDzDt(1:n)*Basis(1:n))*s
          SMBAtIP=SUM(NodalSMB(1:n)*Basis(1:n))
          BottomPresAtIP=SUM(NodalBottomPresVar(1:n)*Basis(1:n))
          BottomTempAtIP=SUM(NodalBottomTempVar(1:n)*Basis(1:n))
          DO k=1,size(AltitudeBin,1)
            IF (ALL(NodalZs(1:n)<AltitudeBin(k)+(BINRESZ*0.5)).and.(ALL(NodalZs(1:n)>AltitudeBin(k)-(BINRESZ*0.5)))) THEN
              TotalSMBAlt(k) = TotalSMBAlt(k) + SMBAtIP*s
              AreaAlt(k) = AreaAlt(k) + s
              BTempAlt(k)=BTempAlt(k)+s*((BottomTempAtIP+ZeroCel)-(Ttriple-beta*(BottomPresAtIP+Psurf-Ptriple)))   
            ENDIF   
          ENDDO
          if(save_2D)then
            DO k=1,size(SpaceBin,1)
              IF((ALL(ElementNodes%x(1:n)<SpaceBin(k)+(0.5*BINRESX))).and.(ALL(ElementNodes%x(1:n)>SpaceBin(k)-(0.5*BINRESX)))) then
                avg_Zs(k)=avg_Zs(k)+SUM(NodalZs(1:n))/n
                space_index(k)=space_index(k)+1
                TotalSMBSpace(k) = TotalSMBSpace(k) + SMBAtIP*s
                Areaspace(k) = Areaspace(k) + s
                BTempSpace(k)=BTempSpace(k)+s*((BottomTempAtIP+ZeroCel)-(Ttriple-beta*(BottomPresAtIP+Psurf-Ptriple))) 
              END IF
            END DO
          endif
          IF (SMBAtIP>0.0_dp) THEN
            AccuSMB = AccuSMB + SMBAtIP*s
            AccuArea = AccuArea + s
          ELSE
            AblaSMB = AblaSMB + SMBAtIP*s
            AblaArea = AblaArea + s
          END IF
          if (FirnAtIP>0.0_dp) then
            FirnArea=FirnArea+s
          endif
          if (((BottomTempAtIP+ZeroCel)-(Ttriple-beta*(BottomPresAtIP+Psurf-Ptriple))).ge.-melt_tol) then
            BottomTempArea=BottomTempArea+s
          endif
          TotalArea = TotalArea + s
          TotalSMB = TotalSMB + SMBAtIP*s 
        END DO
        ! find the front elevation (min z for Not IceFree)
        IF (.NOT.IceFree) THEN
          Do i=1,n
            ZsLoadsFlux = ZsLoadsFlux + &
              ZsLoads%Values(ZsLoads%Perm(NodeIndexes(i)))*LocalArea(i)/NodeArea(Permutation(NodeIndexes(i)))
          End do
          IF (ANY(NodalZs(1:n)<Hfront)) THEN
            Hfront = MINVAL(NodalZs(1:n))
            nf = MINLOC(NodalZs(1:n), DIM=1)
            Xfront = Solver % Mesh % Nodes % x(NodeIndexes(nf))
            Yfront = Solver % Mesh % Nodes % y(NodeIndexes(nf))
          END IF
          IF(ANY(NodalFirn(1:n)>0) .and. ANY(NodalZs(1:n)<Hfirn)) then
            Hfirn = MINVAL(NodalZs(1:n))
            nl=MINLOC(NodalZs(1:n), DIM=1)
            Xfirn = Solver % Mesh % Nodes % x(NodeIndexes(nf))
            Yfirn = Solver % Mesh % Nodes % y(NodeIndexes(nf))
          end if
        END IF
      END DO 
      END SUBROUTINE BODY_INTEGRATION
      END
