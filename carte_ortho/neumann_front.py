import numpy as np
import os 
import sys

index=np.loadtxt('large_f7index.txt')
boundary_old=open('../TRbedlarge_rockice/mesh.boundary-o','r')
boundary=open('../TRbedlarge_rockice/mesh.boundary','w')

for line in boundary_old:
	blist=line.split()
	if (int(blist[1])==5):
		b1=index==int(blist[5])
		b2=index==int(blist[6])
		b3=index==int(blist[7])
		b4=index==int(blist[8])
		if (np.any(b1)*np.any(b2)*np.any(b3)*np.any(b4)):
			blist[1]='6'
			boundary.write(" ".join(blist)+'\n')
		else:
			boundary.write(line)
	else:
		boundary.write(line)

boundary.close()
