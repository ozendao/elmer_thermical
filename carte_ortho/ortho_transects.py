import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib
#from matplotlib_scalebar.scalebar import ScaleBar
from osgeo import gdal
from osgeo import osr
import cartopy.crs as ccrs


# Plot a map with all drillings name and location

# open background image
ds = gdal.Open('Ortho_Tete_rousse_juin2020_lambert2.tif')
gt = ds.GetGeoTransform()
proj = ds.GetProjection()
nc = ds.RasterXSize
nl = ds.RasterYSize
band_R = ds.GetRasterBand(1).ReadAsArray()
band_G = ds.GetRasterBand(2).ReadAsArray()
band_B = ds.GetRasterBand(3).ReadAsArray()
#ds = None

# open contour files

c_smg=np.loadtxt("contour_smg.dat")
c_2010=np.loadtxt("Contour_TR_glacier.dat")
cavite=np.loadtxt("cavite_sonar.dat")
accu=np.loadtxt("loc_accu2602.dat")

#c_sme=np.vstack((c_sme,c_sme[0,:]))
#c_smf=np.vstack((c_smf,c_smf[0,:]))
c_smg=np.vstack((c_smg,c_smg[0,:]))
#RGB = np.dstack([band_R/np.linalg.norm(band_R),band_G/np.linalg.norm(band_G), band_B/np.linalg.norm(band_B)])
RGB = np.dstack([band_R, band_G, band_B])
inproj = osr.SpatialReference()
inproj.ImportFromWkt(proj)
projcs = inproj.GetAuthorityCode('PROJCS')
projection = ccrs.epsg(projcs)

scale=np.asarray([[947950,2105200],[948000,2105200]])
scale_key=(947975,2105220)

coupeA=np.asarray([[948153,2105142],[948140,2104892]])
coupeB=np.asarray([[948054,2105154],[948036,2104902]])
coupeC=np.asarray([[947969,2105155],[947896,2104889]])
subplot_kw = dict(projection=projection)
fig,ax1 = plt.subplots(figsize = (14,8), subplot_kw=subplot_kw, sharex=False, sharey=False)
xmin = 947560
xmax = 948350
ymin = 2104880
ymax = 2105170
ax1.set_extent([xmin, xmax, ymin,ymax], crs=projection)
ax1.gridlines(draw_labels=True, dms=True)  #, x_inline=False, y_inline=False)
extent = (gt[0], gt[0] + nc * gt[1],
                  gt[3] + nl * gt[5], gt[3])
img1 = ax1.imshow(RGB,transform = projection, extent=extent, origin='upper')


ax1.plot(cavite[:,0],cavite[:,1],'-b')
ax1.plot(c_2010[:,0],c_2010[:,1],'-k')
ax1.plot(c_smg[24:c_smg.shape[0],0],c_smg[24:c_smg.shape[0],1],'-c')
ax1.plot(c_smg[10:25,0],c_smg[10:25,1],'-m')
ax1.plot(c_smg[0:11,0],c_smg[0:11,1],'-c')

ax1.plot(accu[0:2:,0],accu[0:2,1],'-g')
ax1.scatter(accu[2:3,0],accu[2:3,1],c='b')
ax1.plot(scale[:,0],scale[:,1],'-k', linewidth=3)
ax1.plot(coupeA[:,0],coupeA[:,1],'-y', linewidth=3)
ax1.plot(coupeB[:,0],coupeB[:,1],'-y', linewidth=3)
ax1.plot(coupeC[:,0],coupeC[:,1],'-y', linewidth=3)

ax1.text(accu[0,0],accu[0,1],'mask = 1.2', ha='center', va='top', color='g',fontsize=14)
ax1.text(accu[1,0],accu[1,1]-10,'mask = 0.75', ha='center', va='top', color='g',fontsize=14)
ax1.text(accu[2,0],accu[2,1]+20,'origine', ha='center', va='top', color='b',fontsize=14)
ax1.text(coupeA[1,0],coupeA[1,1],'Coupe A', ha='center', va='top', color='k',fontsize=14)
ax1.text(coupeB[1,0],coupeB[1,1],'Coupe B', ha='center', va='top', color='k',fontsize=14)
ax1.text(coupeC[1,0],coupeC[1,1],'Coupe C', ha='center', va='top', color='k',fontsize=14)
ax1.text(accu[1,0],accu[1,1]+20,'Coupe 2D', ha='center', va='top', color='k',fontsize=14)
ax1.text(947980,2105125,'cavité 2010', ha='center', va='top', color='b',fontsize=14)
ax1.text(948048,2104980,'contour 2007', ha='center', va='top', color='k',fontsize=14)
ax1.text(948200,2105140,'condition de flux nul', ha='center', va='top', color='m',fontsize=14)
ax1.text(948040,2104940,'condition de surface libre', ha='center', va='top', color='c',fontsize=14)


ax1.text(scale_key[0],scale_key[1],'50 m',ha='center', va='top', color='k',fontsize=14)


ax1.axis('equal')
ax1.set_xlim(xmin,xmax)
ax1.set_ylim(ymin,ymax)
#scalebar = ScaleBar(1, "m")
#ax1.add_artist(scalebar)
ax1.set_xlabel('X Lambert 2 [m]')
ax1.set_ylabel('Y Lambert 2 [m]')

fig.show()
fig.savefig("transects.pdf")
input()
