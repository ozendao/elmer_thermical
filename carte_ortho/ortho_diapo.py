import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib
#from matplotlib_scalebar.scalebar import ScaleBar
from osgeo import gdal
from osgeo import osr
import cartopy.crs as ccrs


# Plot a map with all drillings name and location

# open background image
ds = gdal.Open('Ortho_Tete_rousse_juin2020_lambert2.tif')
gt = ds.GetGeoTransform()
proj = ds.GetProjection()
nc = ds.RasterXSize
nl = ds.RasterYSize
band_R = ds.GetRasterBand(1).ReadAsArray()
band_G = ds.GetRasterBand(2).ReadAsArray()
band_B = ds.GetRasterBand(3).ReadAsArray()
#ds = None

# open contour files

c_smg=np.loadtxt("contour_smg.dat")
cavite=np.loadtxt("cavite_sonar.dat")
accu=np.loadtxt("loc_accu2602.dat")

c_smg=np.vstack((c_smg,c_smg[0,:]))
RGB = np.dstack([band_R, band_G, band_B])
inproj = osr.SpatialReference()
inproj.ImportFromWkt(proj)
projcs = inproj.GetAuthorityCode('PROJCS')
projection = ccrs.epsg(projcs)

scale=np.asarray([[947950,2105200],[948000,2105200]])
scale_key=(947975,2105220)

ba_smb={'MB 19':(948214,2105008),'MB 20':(948171,2105012),'MB 1':(948113,2105035),'MB 2':(948066,2105041),'MB 4':(947973,2105063),'MB 10':(947901,2105090)}

ba_temp={
         '2' :(947994.5075,2105054.455),
         '4' :(947927.3195,2105049.74 ),
         '5' :(948019.5137,2105027.943),
         '10' :(948050.6787,2105008.754),
         '13' :(947962.6118,2105052.789),
         '17' :(948143.1283,2105018.4  ),
         '18' :(947877.9994,2105062.178),
         '70' :(948080.476 ,2105042.552),
         '71' :(948067.5604,2105070.849),
         '72' :(948135.8666,2105039.779),
         '2201':(947878.325 ,2105061.382),
         '2202':(947928.286 ,2105050.534),
         '2203':(947962.687 ,2105052.894),
         '2204':(948007.568 ,2105047.296),
         '2205':(948019.836 ,2105027.984),
         '2206':(948051.002 ,2105008.716),
         '2207':(948051.23  ,2105077.152),
         '2208':(948080.949 ,2105042.286),
         '2209':(948109.777 ,2105067.609),
         '2210':(948143.086 ,2105019.322)}
subplot_kw = dict(projection=projection)
fig,ax1 = plt.subplots(figsize = (14,8), subplot_kw=subplot_kw, sharex=False, sharey=False)
xmin = 947560
xmax = 948350
ymin = 2104880
ymax = 2105170
ax1.set_extent([xmin, xmax, ymin,ymax], crs=projection)
ax1.gridlines(draw_labels=True, dms=True)  #, x_inline=False, y_inline=False)
extent = (gt[0], gt[0] + nc * gt[1],
                  gt[3] + nl * gt[5], gt[3])
img1 = ax1.imshow(RGB,transform = projection, extent=extent, origin='upper')


ax1.plot(cavite[:,0],cavite[:,1],'-b', linewidth=4)
ax1.plot(c_smg[24:c_smg.shape[0],0],c_smg[24:c_smg.shape[0],1],'-c', linewidth=4)
ax1.plot(c_smg[10:25,0],c_smg[10:25,1],'-m', linewidth=4)
ax1.plot(c_smg[0:11,0],c_smg[0:11,1],'-c', linewidth=4)

ax1.plot(accu[0:2:,0],accu[0:2,1],'-g', linewidth=4)
ax1.plot(accu[2,0],accu[2,1],'o',color='b',markersize=10)
ax1.plot(scale[:,0],scale[:,1],'-k', linewidth=6)


ax1.text(scale_key[0],scale_key[1]+10,'50 m',ha='center', va='top', color='k',fontsize=24)

for key in list(ba_smb):
	xp = ba_smb[key][0]
	yp = ba_smb[key][1]
	ax1.plot(xp,yp,'o', markersize=8, color='k')



i=0
for key in list(ba_temp):
	xp = ba_temp[key][0]
	yp = ba_temp[key][1]
	ax1.plot(xp,yp,'o', markersize=8, color='m')






ax1.axis('equal')
ax1.set_xlim(xmin,xmax)
ax1.set_ylim(ymin,ymax)
ax1.set_xlabel('X Lambert 2 [m]')
ax1.set_ylabel('Y Lambert 2 [m]')

fig.show()
fig.savefig("contour_diapo.pdf")
input()
