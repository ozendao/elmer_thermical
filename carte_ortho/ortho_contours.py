import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib
#from matplotlib_scalebar.scalebar import ScaleBar
from osgeo import gdal
from osgeo import osr
import cartopy.crs as ccrs


# Plot a map with all drillings name and location

# open background image
ds = gdal.Open('Ortho_Tete_rousse_juin2020_lambert2.tif')
gt = ds.GetGeoTransform()
proj = ds.GetProjection()
nc = ds.RasterXSize
nl = ds.RasterYSize
band_R = ds.GetRasterBand(1).ReadAsArray()
band_G = ds.GetRasterBand(2).ReadAsArray()
band_B = ds.GetRasterBand(3).ReadAsArray()
#ds = None

# open contour files

c_smg=np.loadtxt("contour_smg.dat")
c_smf=np.loadtxt("contour_smf.dat")
c_sme=np.loadtxt("contour_sme.dat")
c_smd=np.loadtxt("contour_smd.dat")
c_smb=np.loadtxt("contour_smb.dat")
c_sl=np.loadtxt("contour_sl.dat")
c_tradri=np.loadtxt("contour_tradri.dat")
c_gag2=np.loadtxt("contour_gag2.dat")
c_2010=np.loadtxt("Contour_TR_glacier.dat")
cavite=np.loadtxt("Contour_TR_cavity.dat")
accu=np.loadtxt("loc_accu2602.dat")

#c_sme=np.vstack((c_sme,c_sme[0,:]))
#c_smf=np.vstack((c_smf,c_smf[0,:]))
c_smg=np.vstack((c_smg,c_smg[0,:]))
#RGB = np.dstack([band_R/np.linalg.norm(band_R),band_G/np.linalg.norm(band_G), band_B/np.linalg.norm(band_B)])
RGB = np.dstack([band_R, band_G, band_B])
inproj = osr.SpatialReference()
inproj.ImportFromWkt(proj)
projcs = inproj.GetAuthorityCode('PROJCS')
projection = ccrs.epsg(projcs)

scale=np.asarray([[947950,2105200],[948000,2105200]])
scale_key=(947975,2105220)

ba_smb={'MB 19':(948214,2105008),'MB 20':(948171,2105012),'MB 1':(948113,2105035),'MB 2':(948066,2105041),'MB 4':(947973,2105063),'MB 10':(947901,2105090)}

ba_temp={
         '2' :(947994.5075,2105054.455),
         '4' :(947927.3195,2105049.74 ),
         '5' :(948019.5137,2105027.943),
         '10' :(948050.6787,2105008.754),
         '13' :(947962.6118,2105052.789),
         '17' :(948143.1283,2105018.4  ),
         '18' :(947877.9994,2105062.178),
         '70' :(948080.476 ,2105042.552),
         '71' :(948067.5604,2105070.849),
         '72' :(948135.8666,2105039.779),
         '2201':(947878.325 ,2105061.382),
         '2202':(947928.286 ,2105050.534),
         '2203':(947962.687 ,2105052.894),
         '2204':(948007.568 ,2105047.296),
         '2205':(948019.836 ,2105027.984),
         '2206':(948051.002 ,2105008.716),
         '2207':(948051.23  ,2105077.152),
         '2208':(948080.949 ,2105042.286),
         '2209':(948109.777 ,2105067.609),
         '2210':(948143.086 ,2105019.322)}
#CH={'CHN':(947994.5075-(200.00-171.05)*(36.446/25.834),2105054.455+(90.60-72.65)*(36.446/25.834))}
#coupeA=np.asarray([[947994.5075-(130.00-171.05)*(36.446/25.834),2105054.455+(90.60-28.98)*(36.446/25.834)],
#                  [947994.5075-(136.82-171.05)*(36.446/25.834),2105054.455+(90.60-152.31)*(36.446/25.834)]]
#                 )
#coupeB=np.asarray([[947994.5075-(180.65-171.05)*(36.446/25.834),2105054.455+(90.60-23.87)*(36.446/25.834)],
#                  [947994.5075-(189.56-171.05)*(36.446/25.834),2105054.455+(90.60-147.23)*(36.446/25.834)]]
#                 )
#coupeC=np.asarray([[947994.5075-(224.57-171.05)*(36.446/25.834),2105054.455+(90.60-22.46)*(36.446/25.834)],
#                  [947994.5075-(260.29-171.05)*(36.446/25.834),2105054.455+(90.60-155.38)*(36.446/25.834)]]
#                 )
#coupeD=np.asarray([[947994.5075-(293.52-171.05)*(36.446/25.834),2105054.455+(90.60-31.07)*(36.446/25.834)],
#                  [947994.5075-(301.65-171.05)*(36.446/25.834),2105054.455+(90.60-171.44)*(36.446/25.834)]]
#                 )
subplot_kw = dict(projection=projection)
fig,ax1 = plt.subplots(figsize = (14,8), subplot_kw=subplot_kw, sharex=False, sharey=False)
xmin = 947560
xmax = 948350
ymin = 2104880
ymax = 2105170
ax1.set_extent([xmin, xmax, ymin,ymax], crs=projection)
ax1.gridlines(draw_labels=True, dms=True)  #, x_inline=False, y_inline=False)
extent = (gt[0], gt[0] + nc * gt[1],
                  gt[3] + nl * gt[5], gt[3])
img1 = ax1.imshow(RGB,transform = projection, extent=extent, origin='upper')


ax1.plot(cavite[:,0],cavite[:,1],'-b')
ax1.plot(c_2010[:,0],c_2010[:,1],'-k')
#ax1.plot(c_gag2[:,0],c_gag2[:,1],'-b')
#ax1.plot(c_tradri[:,0],c_tradri[:,1],'-g')
#ax1.plot(c_sl[:,0],c_sl[:,1],'-r')
#ax1.plot(c_smb[:,0],c_smb[:,1],'-m')
#ax1.plot(c_smd[:,0],c_smd[:,1],'-m')
#ax1.plot(c_sme[0:19,0],c_sme[0:19,1],'-r')
#ax1.plot(c_sme[19:43,0],c_sme[19:43,1],'-c')
#ax1.plot(c_sme[42:46,0],c_sme[42:46,1],'-r')
#ax1.plot(c_sme[18:c_sme.shape[0],0],c_sme[18:c_sme.shape[0],1],'-c')
#ax1.plot(c_sme[0:19,0],c_sme[0:19,1],'-r')
#ax1.plot(c_smf[21:c_smf.shape[0],0],c_smf[21:c_smf.shape[0],1],'-b')
#ax1.plot(c_smf[0:22,0],c_smf[0:22,1],'-r')
ax1.plot(c_smg[24:c_smg.shape[0],0],c_smg[24:c_smg.shape[0],1],'-c')
ax1.plot(c_smg[10:25,0],c_smg[10:25,1],'-m')
ax1.plot(c_smg[0:11,0],c_smg[0:11,1],'-c')

ax1.plot(accu[0:2:,0],accu[0:2,1],'-g')
ax1.scatter(accu[2:3,0],accu[2:3,1],c='b')
ax1.plot(scale[:,0],scale[:,1],'-k', linewidth=3)
#ax1.plot(coupeA[:,0],coupeA[:,1],'-c', linewidth=3)
#ax1.plot(coupeB[:,0],coupeB[:,1],'-c', linewidth=3)
#ax1.plot(coupeC[:,0],coupeC[:,1],'-c', linewidth=3)
#ax1.plot(coupeD[:,0],coupeD[:,1],'-c', linewidth=3)

ax1.text(accu[0,0],accu[0,1],'mask = 1.2', ha='center', va='top', color='g',fontsize=12)
ax1.text(accu[1,0],accu[1,1]-10,'mask = 0.75', ha='center', va='top', color='g',fontsize=12)
ax1.text(accu[2,0],accu[2,1]+20,'origine', ha='center', va='top', color='b',fontsize=12)
ax1.text(947980,2105125,'cavité 2010', ha='center', va='top', color='b',fontsize=12)
ax1.text(948048,2104980,'contour 2007', ha='center', va='top', color='k',fontsize=12)
ax1.text(948200,2105140,'condition de flux nul', ha='center', va='top', color='m',fontsize=12)
ax1.text(948040,2104940,'condition de surface libre', ha='center', va='top', color='c',fontsize=12)

for key in list(ba_smb):
	xp = ba_smb[key][0]
	yp = ba_smb[key][1]
	ax1.plot(xp,yp,'o', markersize=2, color='k')
	ax1.text(xp,yp,key, ha='center', va='bottom', color='k',fontsize=10)

#for key in list(CH):
#	xp = CH[key][0]
#	yp = CH[key][1]
#	ax1.plot(xp,yp,'o', markersize=2, color='g')
#	ax1.text(xp,yp,key, ha='center', va='bottom', color='k',fontsize=10)


i=0
for key in list(ba_temp):
	xp = ba_temp[key][0]
	yp = ba_temp[key][1]
	ax1.plot(xp,yp,'o', markersize=2, color='m')
	if i<9:
		va='top'
		dy=-2
	else:
		va='bottom'
		dy=2
	ax1.text(xp,yp+dy,key, ha='center', va=va, color='m',fontsize=10)
	i+=1

ax1.text(scale_key[0],scale_key[1],'50 m',ha='center', va='top', color='k',fontsize=14)





ax1.axis('equal')
ax1.set_xlim(xmin,xmax)
ax1.set_ylim(ymin,ymax)
#scalebar = ScaleBar(1, "m")
#ax1.add_artist(scalebar)
ax1.set_xlabel('X Lambert 2 [m]')
ax1.set_ylabel('Y Lambert 2 [m]')

fig.show()
fig.savefig("contour_2204.pdf")
input()
