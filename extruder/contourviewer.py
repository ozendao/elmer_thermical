import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib


mnt2014=np.loadtxt("DataTeterousse/MNTBedInf2014.dat",delimiter="  ")
contgag=np.loadtxt("DataTeterousse/contour_large.dat",delimiter=" ")

matplotlib.rc('axes.formatter'  , limits=(-2,3))
matplotlib.rc('lines'           , markersize=6.0)
matplotlib.rc('scatter'         , marker="s")
matplotlib.rc('xtick'           , labelsize=18)
matplotlib.rc('ytick'           , labelsize=18)
matplotlib.rc('axes'            , titlesize=20)
matplotlib.rc('axes'            , labelsize=20)
matplotlib.rc('legend'          , fontsize=16)
matplotlib.rc('legend'          , title_fontsize=18)
matplotlib.rc('legend'          , frameon=False)
matplotlib.rc('text'            ,usetex=True)
matplotlib.rc('figure'          ,titlesize=24)

fig,axe=plt.subplots(nrows=1,ncols=1,figsize=(18,12))

fig.suptitle("Imposed mass balance Teterousse")
axe.set_xlabel(r'x (m) (Lambert II EXT)') 
axe.set_ylabel(r'y (m) (Lambert II EXT)')
axe.ticklabel_format(axis='x', style='sci')
axe.ticklabel_format(axis='y', style='sci')

axe.scatter(mnt2014[:,0],mnt2014[:,1],c=mnt2014[:,2], cmap='viridis')
axe.plot(contgag[:,0],contgag[:,1])

fig.show()

input()
