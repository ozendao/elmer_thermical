import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib

##################
##parametres à mettre en ligne de comande
#################
yearstart=2013
n1D=301
rows=2
cols=3
prefix_file='output_p'
##################
data=[]
for i in range(1,rows*cols+1):
	print("load file %s%03i.dat"%(prefix_file,i+1))
	data.append(np.loadtxt("%s%03i.dat"%(prefix_file,i+1),delimiter=" "))
	

max_time_index=math.floor(data[0].shape[0]/n1D)

matplotlib.rc('axes.formatter'  , limits=(-2,3))
matplotlib.rc('lines'           , markersize=0.3)
matplotlib.rc('xtick'           , labelsize=18)
matplotlib.rc('ytick'           , labelsize=18)
matplotlib.rc('axes'            , titlesize=20)
matplotlib.rc('axes'            , labelsize=20)
matplotlib.rc('legend'          , fontsize=16)
matplotlib.rc('legend'          , title_fontsize=18)
matplotlib.rc('legend'          , frameon=False)
matplotlib.rc('text'            ,usetex=True)
matplotlib.rc('image'           ,cmap='magma')
matplotlib.rc('scatter'         ,marker='s')

fig,axe=plt.subplots(nrows=rows,ncols=cols,figsize=(20*cols,20*rows))
fig.subplots_adjust(hspace=0.5, wspace=0.3) 

for i in range(rows):
	for j in range(cols):
		axe[i,j].set_xlabel(r't(year)') 
		axe[i,j].set_ylabel(r'$z$(m)')
		axe[i,j].ticklabel_format(axis='x', style='sci')
		axe[i,j].ticklabel_format(axis='y', style='sci')


for i in range(rows):
	for j in range(cols):
		qp=axe[i,j].scatter(data[i*cols+j][:,8]-yearstart,data[i*cols+j][:,1],c=data[i*cols+j][:,0])#,vmin=240)
		fig.colorbar(qp, ax=axe[i,j],orientation='horizontal',location='top',label=r'$T$(K)',fraction=0.05)


fig.show()

input()
