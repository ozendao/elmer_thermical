import numpy as np
import os 
import sys

def correction(x):
	gradp=0.0022
	h0=3139
	if   (0<=x):
		a=1
	else :
		a=np.nan
	return a+(x-h0)*gradp


surface=np.loadtxt('DataTeterousse/DEM_TR_surf.dat')
nline=surface.shape[0]
accu = open("DataTeterousse/Accu_simple.dat",'w')
accu_vis=open("DataTeterousse/Accu_simple.plot",'w')
for i in range(nline):
	accu.write("{0:f} {1:f} {2:f} \n".format(surface[i,0],surface[i,1],correction(surface[i,2])))
	accu_vis.write("{0:f} {1:f} \n".format(surface[i,2],correction(surface[i,2])))
accu.close()
