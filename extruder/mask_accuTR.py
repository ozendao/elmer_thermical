import numpy as np
import os 
import sys

def correction(x):
	gradp=0.0037
	h0=3139
	if   (0<=x<3139):
		a=1
	elif (x>=3139 and x<3163):
		a=1-0.00636*(x-3139)
	elif (x>=3163 and x<3186):
		a= 0.847+0.00304*(x-3139)
	elif (x>=3186 and x<3193):
		a= 0.917-0.00257*(x-3186)
	elif (x>=3193 and x<3213):
		a= 0.899+0.00472*(x-3193)
	elif (x>=3213 and x<3239):
		a= 0.994+0.000368*(x-3213)
	elif (x>=3239):
		a=1
	else :
		a=np.nan
	return a+(x-h0)*gradp


surface=np.loadtxt('DataTeterousse/DEM_TR_surf.dat')
nline=surface.shape[0]
accu = open("DataTeterousse/Accu_mean_grad.dat",'w')
accu_vis=open("DataTeterousse/Accu_mean_grad.plot",'w')
for i in range(nline):
	accu.write("{0:f} {1:f} {2:f} \n".format(surface[i,0],surface[i,1],correction(surface[i,2])))
	accu_vis.write("{0:f} {1:f} \n".format(surface[i,2],correction(surface[i,2])))
accu.close()
