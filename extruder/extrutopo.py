import numpy as np
import os 
import sys

seqindex=[]
for line in open(sys.argv[1],'r'):
	wlist=line.split()
	print(wlist)
	if wlist[0]=="data_dir":
		data_dir=wlist[1]
	if wlist[0]=="dem_contour":
		dem_contour=wlist[1]
	if wlist[0]=="dem_surf":
		dem_surf=wlist[1]
	if wlist[0]=="dem_bed":
		dem_bed=wlist[1]
	if wlist[0]=="rock_thick":
		rock_thick=float(wlist[1])
	if wlist[0]=="noval":
		noval=float(wlist[1])
	if wlist[0]=="glacier_name":
		extrude_config_p=wlist[1]+"_config_paral"
		extrude_config_s=wlist[1]+"_config_serial"
		output_mesh=wlist[1]+"_2D"
		mesh_extruded=wlist[1]+"_rockice"
	if wlist[0]=="el_size":
		el_size=float(wlist[1])
	if wlist[0]=="spline":
		spline=int(wlist[1])==1
	if wlist[0]=="nb_proc":
		nb_proc=int(wlist[1])
	if wlist[0]=="mesh_extruder":
		mesh_extruder=wlist[1]
	if wlist[0]=="cutoff":
		cutoff=float(wlist[1])
	if wlist[0]=="wexp":
		wexp=float(wlist[1])
	if wlist[0]=="nb_levels":
		nb_levels_rock=int(wlist[1])
		nb_levels_ice=int(wlist[2])
	if wlist[0]=="min_depth":
		min_depth_rock=float(wlist[1])
		min_depth_ice=float(wlist[2])
	if wlist[0]=="GL":
		GL_b_rock=int(wlist[1])
		GL_s_rock=int(wlist[2])
		GL_b_ice=int(wlist[3])
		GL_s_ice=int(wlist[4])
	if wlist[0]=="ratio":
		ratio_b_rock=float(wlist[1])
		ratio_s_rock=float(wlist[2])
		ratio_b_ice=float(wlist[3])
		ratio_s_ice=float(wlist[4])
	if wlist[0]=="seqindex":
		seqindex = [float(i) for i in wlist[1:]]

np_nouille=len(seqindex)

###
sif_visu=open("visu_{0}.sif".format(mesh_extruded),'w')
sif_visu.write("check keywords warn\necho on\n")
sif_visu.write("$Step = \"{0}_mesh_\"\n".format(mesh_extruded))
sif_visu.write("Header\n   Mesh DB \".\" \"{0}\"\nEnd\n".format(mesh_extruded))
sif_visu.write("Simulation\n   Coordinate System  =  Cartesian 3D\n   Simulation Type = Steady\n")
sif_visu.write("  Timestep Intervals = 1\n  Timestep Sizes = 1\n  Output Intervals = 1\n")
sif_visu.write("  Steady State Min Iterations = 1\n  Steady State Max Iterations = 1\n  Output File = \"$Step\".result\"\n")
sif_visu.write("  Post File = \"$Step\".vtu\"\nEnd\n")
sif_visu.write("Body 1\n  Equation = 1\n  Body Force = 1\n  Material = 1\n  Initial Condition = 1\nEnd\n")
sif_visu.write("Body 2\n  Equation = 1\n  Body Force = 1\n  Material = 1\n  Initial Condition = 1\nEnd\n")
sif_visu.write("Initial Condition 1\nEnd\n")
sif_visu.write("Body Force 1\nEnd\n")
sif_visu.write("Material 1\nEnd\n")
sif_visu.write("Solver 1\n  Equation = \"Flowdepth\"\n  Procedure = File \"ElmerIceSolvers\" \"FlowDepthSolver\"\n")
sif_visu.write("  Variable = String \"Thick\"\n  Variable DOFs = 1\n  Linear System Solver = \"Direct\"\n")
sif_visu.write("  Gradient = Real 1.0\n End\n")
sif_visu.write("Equation 1\n  Active Solvers(1) = 1\n  Flow Solution Name = String \"thickness\"\nEnd\n")
sif_visu.write("! ground\nBoundary Condition 1\n  Target Boundaries = 1\nEnd\n")
sif_visu.write("! interface=bedrock\nBoundary Condition 2\n  Target Boundaries = 2\n  thick=Real 0.0\nEnd\n")
sif_visu.write("!ice free surface\nBoundary Condition 3\n  Target Boundaries = 3\nEnd\n")
ipart=np_nouille+1
sifind=4
while ipart >0:
	sif_visu.write("! lateral rock border\nBoundary Condition {0:.0f}\n  Target Boundaries = {0:.0f}\nEnd\n".format(sifind))
	sif_visu.write("! lateral ice border\nBoundary Condition {0:.0f}\n  Target Boundaries = {0:.0f}\nEnd\n".format(sifind+1))
	ipart-=1
	sifind+=2
sif_visu.close()

###
os.system("mkdir -p {0}/EXT_total".format(data_dir))
os.system("cp {0}/{1} {0}/EXT_total/surf.xyz".format(data_dir,dem_surf))
os.system("cp {0}/{1} {0}/EXT_total/interface.xyz".format(data_dir,dem_bed))
surf=np.loadtxt("{0}/EXT_total/interface.xyz".format(data_dir))
bed=surf
for i in range(len(bed)):
	if bed[i,2]>noval:
		bed[i,2]-=rock_thick

np.savetxt("{0}/EXT_total/bed.xyz".format(data_dir),bed)
###
Contour = np.loadtxt("{0}/{1}".format(data_dir,dem_contour))
x = Contour[:,0]
y = Contour[:,1]
if x[0]==x[-1] and y[0]==y[-1]:
	print('Same first and last points in contour file')
	Npt = len(x)-1
else:
	Npt = len(x)    
# Open the output file
geo = open("{0}.geo".format(output_mesh), 'w')
geo.write("// This a a geo file created using the python script Makegeo.py // \n")
geo.write("Mesh.Algorithm=11; \n")
geo.write("// To controle the element size, one can directly modify the lc value in the geo file // \n")
geo.write("lc = {0} ; \n".format(el_size))
# write the points coordinates (x,y,0,lc)
np=0
index_nouille=1
list_nouille=[]
l=[]
for j in range(0,Npt):
	np+=1
	geo.write('Point({0}) = '.format(np)+r'{'+' {0}, {1}, 0.0, lc'.format(x[j],y[j])+r'}'+'; \n')
	l.append(np)
	if len(seqindex)>0 and seqindex[0]==np:
		list_nouille.append(l)
		l=[int(seqindex.pop(0))]
l.append(1)
list_nouille.append(l)
spline_index=1
list_loop=[]
# if spline
if spline: 
	for lst in list_nouille:
		geo.write('Spline('+str(spline_index)+') = {'+','.join([str(i) for i in lst])+'};\n')
		list_loop.append(str(spline_index))
		spline_index+=1
	geo.write('Line Loop('+str(np_nouille+2)+') = {'+','.join(list_loop)+'}; \n')
	geo.write('Plane Surface('+str(np_nouille+3)+') = {'+str(np_nouille+2)+'}; \n')
	i=0
	for st_ind in list_loop:
		geo.write('Physical Line('+str(np_nouille+4+i)+') = {'+str(i+1)+'}; \n')
		i+=1
	geo.write('Physical Surface('+str(np_nouille+4+i)+') = {'+str(np_nouille+3)+'}; \n')

# else it is lines, as a spline might not work in all case
else:
	nl=0
	for j in range(0,Npt-1):
		nl=nl+1
		geo.write('Line({0}) = '.format(nl)+r'{'+'{0},{1}'.format(j+1,j+2)+r'}'+'; \n')
	geo.write('Line({0}) = '.format(nl+1)+r'{'+'{0},{1}'.format(j+2,1)+r'}'+'; \n')    
	geo.write('Line Loop({0}) = '.format(nl+2)+r'{')
	for j in range(0,Npt-1):
		geo.write('{0}, '.format(j+1))
	geo.write('{0}'.format(j+2)+'}; \n')    
	geo.write('Plane Surface({0}) = '.format(nl+3)+r'{'+'{0}'.format(nl+2)+r'};'+' \n')
	phys_index=nl+4
	for lst in list_nouille:
		geo.write('Physical Line('+str(phys_index)+') = {'+','.join([str(i) for i in lst])+'};\n')
		phys_index+=1
	geo.write('Physical Surface('+str(phys_index)+') = {'+str(nl+3)+'}; \n')

geo.close()
###
os.system("gmsh {0}.geo -1 -2".format(output_mesh))
os.system("ElmerGrid 14 2 {0}.msh -autoclean".format(output_mesh))
os.system("ElmerGrid 14 5 {0}.msh -autoclean".format(output_mesh))
EM_config_s=open(extrude_config_s,'w')
EM_config_s.write("{0:d} {1:d} {2:f} {3:f} 1\n".format(nb_levels_rock,nb_levels_ice,min_depth_rock,min_depth_ice))
EM_config_s.write("0\n")
EM_config_s.write("{0:d} {1:d} {2:d} {3:d} {4:f} {5:f} {6:f} {7:f} 0 0 0 0\n".format(GL_b_rock,GL_s_rock,GL_b_ice,GL_s_ice,ratio_b_rock,ratio_s_rock,ratio_b_ice,ratio_s_ice))
EM_config_s.write("{0:f} {1:f} {2:f}\n".format(cutoff,wexp,noval))
EM_config_s.write("0\n")
EM_config_s.close()
os.system(mesh_extruder+" "+output_mesh+" "+mesh_extruded+" "+extrude_config_s+" {0}/EXT_total".format(data_dir))
os.system("ElmerGrid 2 5 "+mesh_extruded+" -autoclean")	
os.system("ElmerSolver "+sif_visu.name)

if nb_proc>1:
	os.system("ElmerGrid 2 2 {0} -autoclean -metis {1:d}".format(output_mesh,nb_proc))
	EM_config_p=open(extrude_config_p,'w')
	EM_config_p.write("{0:d} {1:d} {2:f} {3:f} {4:d}\n".format(nb_levels_rock,nb_levels_ice,min_depth_rock,min_depth_ice,nb_proc))
	EM_config_p.write("0\n")
	EM_config_p.write("{0:d} {1:d} {2:d} {3:d} {4:f} {5:f} {6:f} {7:f} 0 0 0 0\n".format(GL_b_rock,GL_s_rock,GL_b_ice,GL_s_ice,ratio_b_rock,ratio_s_rock,ratio_b_ice,ratio_s_ice))
	EM_config_p.write("{0:f} {1:f} {2:f}\n".format(cutoff,wexp,noval))
	EM_config_p.write("0\n")
	EM_config_p.close()
	os.system(mesh_extruder+" "+output_mesh+" "+mesh_extruded+" "+extrude_config_p+" {0}/EXT_total".format(data_dir))
	os.system("mpirun -np {0:d} --bind-to hwthread ElmerSolver ".format(nb_proc)+sif_visu.name)

