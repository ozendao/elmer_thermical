import numpy as np
import os 
import sys

def correction(x,y):
	x_h=948273
	y_h=2104999
	x_b=947901
	y_b=2105095
	m_min=0.75
	m_max=1.2
	HX_dot_HB=(x-x_h)*(x_b-x_h)+(y-y_h)*(y_b-y_h)
	HB_dot_HB=(x_b-x_h)*(x_b-x_h)+(y_b-y_h)*(y_b-y_h)
	if (HX_dot_HB <= 0):
		mask=m_max
	elif (HX_dot_HB >= HB_dot_HB):
		mask=m_min
	else:
		mask=m_max+(m_min-m_max)*HX_dot_HB/HB_dot_HB
	return mask

surface=np.loadtxt('DataTeterousse/DEMSurf2019.dat')
nline=surface.shape[0]
accu = open("DataTeterousse/Accu2602_xy.dat",'w')
accu_alt = open("DataTeterousse/Accu2602_alt_xy.dat",'w')
for i in range(nline):
	accu.write("{0:f} {1:f} {2:f} \n".format(surface[i,0],surface[i,1],correction(surface[i,0],surface[i,1])))
	if (surface[i,2] < 0):
		z_out=np.nan
	else:
		z_out=surface[i,2]
	accu_alt.write("{0:f} {1:f} {2:f} {3:f}\n".format(surface[i,0],surface[i,1],z_out,correction(surface[i,0],surface[i,1])))
accu.close()
