gcc ExtrudeMeshRockIce.c -o ExtrudeMeshRockIce -lm
python3 makeextrusion.py param_teterousse
mpirun -np 6 --bind-to hwthread ElmerSolver init_stokes.sif
elmerf90 -o bin/EnthalpySolver SRC/EnthalpySolver.f90
elmerf90 -o bin/HeatSolver SRC/HeatSolve.f90
elmerf90 -o bin/MassBalance SRC/TransientMassBalance_MaskRelief.F90 -fcheck=all
elmerf90 -o bin/MassBalance_cpr SRC/TransientMassBalance_MaskRelief_cpr.F90 -fcheck=all
elmerf90 -o bin/MassBalance_tr SRC/TransientMassBalance_MaskRelief_tr.F90 -fcheck=all
elmerf90 -o bin/Scalar_OUTPUT_Glacier SRC/Scalar_OUTPUT_Glacier_altMB.f90
elmerf90 -o bin/NsVec SRC/IncompressibleNSVec.F90
elmerf90 -o bin/PercolSolver1D SRC/percol_1D_solver.f90
elmerf90 -o bin/IcyMaskSolver SRC/IcyMaskSolver.F90
elmerf90 -o bin/FreeSurface SRC/FreeSurfaceSolver.F90
elmerf90 -o bin/Compute2DNodalGradient SRC/Compute2DNodalGradient.F90
elmerf90 -o bin/MassBalance_pr SRC/TransientMassBalance_MaskRelief_potrad.F90 -fcheck=all
