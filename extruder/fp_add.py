import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib



data=np.loadtxt("OUT_1D_TR_fp_smb_MB.dat",delimiter=" ")
obs=np.loadtxt("DataTeterousse/TR_smb_2013-2022.txt",delimiter=" ")

#minlength=min(data_inc.shape[0],obs_inc.shape[0])

#data=data_inc[0:minlength]
#obs=obs_inc[0:minlength]
nb_bins=3
firn_density=0.38
stepsperyear=30
start_time=data[0,0]
index_min_calc=20
index_min_obs=7*36
times=start_time+(np.asarray(range(obs.shape[0])))/365.25
labels=['average']
for i in range(nb_bins):
	labels.append(str(data[0,25+3*i]))

matplotlib.rc('axes.formatter'  , limits=(-2,3))
matplotlib.rc('lines'           , markersize=0.3)
matplotlib.rc('xtick'           , labelsize=18)
matplotlib.rc('ytick'           , labelsize=18)
matplotlib.rc('axes'            , titlesize=20)
matplotlib.rc('axes'            , labelsize=20)
matplotlib.rc('legend'          , fontsize=16)
matplotlib.rc('legend'          , title_fontsize=18)
matplotlib.rc('legend'          , frameon=False)
matplotlib.rc('text'            ,usetex=True)
matplotlib.rc('figure'          ,titlesize=24)

fig,axe=plt.subplots(nrows=1,ncols=1,figsize=(18,12))

fig.suptitle("mass balance Teterousse")
axe.set_xlabel(r't(year)') 
axe.set_ylabel(r'Mass Balance (m of water)')
axe.ticklabel_format(axis='x', style='sci')
axe.ticklabel_format(axis='y', style='sci')
print(data.shape[0])
cumul_MB=np.zeros((data.shape[0],nb_bins+1))
prev=np.zeros(nb_bins+1)
for i in range(data.shape[0]):
	cumul_MB[i,0]=prev[0]+(data[i,6]/data[i,3]*firn_density)/stepsperyear
	prev[0]+=(data[i,6]/data[i,3]*firn_density)/stepsperyear
	for j in range(nb_bins):
		cumul_MB[i,j+1]=prev[j+1]+(data[i,26+3*j]/data[i,27+3*j]*firn_density)/stepsperyear
		prev[j+1]+=(data[i,26+3*j]/data[i,27+3*j]*firn_density)/stepsperyear
axe.plot(times[index_min_obs-8:],obs[index_min_obs-8:]-obs[index_min_obs],label="3168 + Deg Jr")
for i,key in zip(range(nb_bins+1),labels):
	axe.plot(data[index_min_calc:,0],cumul_MB[index_min_calc:,i]-cumul_MB[index_min_calc,i],label=key)

axe.legend(loc="lower left")
fig.show()

input()
