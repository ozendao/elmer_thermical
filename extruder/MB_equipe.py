import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib


data_m=[]
data_m.append(np.loadtxt("Dataclimat/bilanDGhomo_1801_1998.txt",delimiter=" "))
data_m.append(np.loadtxt("Dataclimat/bilanDGlyonbesse1908_2007.txt",delimiter=" "))
arg=np.loadtxt("Dataclimat/arg_cumul.txt",delimiter=" ")
arg[:,1]=arg[:,1]-2.2+0.08*(arg[:,0]-1976)
data_m.append(arg)

data_s=[]
data_s.append(np.loadtxt("Dataclimat/bilanCT1901_1950_2007.txt",delimiter=" "))

data_d=[]
lost_array=np.asarray([[float(k) for k in line.split()] for line in open('TRbedlarge_rockice/B_lostmass_secular_11c_np4.dat','r')])
data_d.append([np.loadtxt("TRbedlarge_rockice/OUT_1D_secular_11c.dat",delimiter=" "),lost_array])
labels_m=[]
labels_m.append("DG homogenise")
labels_m.append("DG LyonBesse")
labels_m.append("DG Argentiere")

labels_s=[]
labels_s.append("Topo TR")


labels_d=[]
labels_d.append("Computed Mass Balance")
ice_density=0.917
stepsperyear=30
indinit_homo=110
indinit_lyon=3
mean_init=0.5*(data_m[0][indinit_homo,1]+data_m[1][indinit_lyon,1])
indinit_res=4*stepsperyear+22

matplotlib.rc('axes.formatter'  , limits=(-2,3))
matplotlib.rc('lines'           , markersize=6.0)
matplotlib.rc('scatter'         , marker="s")
matplotlib.rc('xtick'           , labelsize=18)
matplotlib.rc('ytick'           , labelsize=18)
matplotlib.rc('axes'            , titlesize=20)
matplotlib.rc('axes'            , labelsize=20)
matplotlib.rc('legend'          , fontsize=16)
matplotlib.rc('legend'          , title_fontsize=18)
matplotlib.rc('legend'          , frameon=False)
matplotlib.rc('text'            ,usetex=True)
matplotlib.rc('figure'          ,titlesize=24)

fig1,axe1=plt.subplots(nrows=1,ncols=1,figsize=(18,12))

fig1.suptitle(r'$\phi_i$ (1907-2010)')
axe1.set_xlabel(r't(year)') 
axe1.set_ylabel(r'Mass Balance (m. eq.  water)')
axe1.ticklabel_format(axis='x', style='sci')
axe1.ticklabel_format(axis='y', style='sci')

zpos=len(data_m)+len(data_d)
for tab,lab in zip (data_m,labels_m):
	axe1.plot(tab[:,0],tab[:,1],label=lab)


for tab,lab in zip (data_s,labels_s):
	zpos+=1
	axe1.scatter(tab[:,0],tab[:,1],label=lab,c='black',zorder=zpos)


for tab,lab in zip (data_d,labels_d):
	cumul_MB=np.zeros(tab[0].shape[0])
	cumul_flow=np.zeros(tab[1].shape[0])
	cumul_load=np.zeros(tab[0].shape[0])
	prev_m=0
	prev_f=0
	prev_l=0
	for i in range(tab[0].shape[0]):
		cumul_MB[i]=prev_m+((tab[0][i,6]*ice_density)/tab[0][i,3])/stepsperyear
		prev_m+=((tab[0][i,6]*ice_density)/tab[0][i,3])/stepsperyear
#		cumul_flow[i]=prev_f+((tab[1][i,1]*1e-3)/tab[0][i,3])/stepsperyear
#		prev_f+=((tab[1][i,1]*1e-3)/tab[0][i,3])/stepsperyear
		cumul_load[i]=prev_l+(tab[0][i,9]/tab[0][i,3])/stepsperyear
		prev_l+=(tab[0][i,9]/tab[0][i,3])/stepsperyear

	axe1.plot(tab[0][:,0],cumul_MB-cumul_MB[indinit_res]+(cumul_load-cumul_load[indinit_res])+mean_init,label=lab)


axe1.legend(loc="lower left")
fig1.show()

input()
