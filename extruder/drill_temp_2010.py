import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib
#########################################
data_dir='temp_vertical'
simu_name="m2204_f"
name='retour_dahu/verticaltemp_secular_'+simu_name+'.dat' 
simu_start=1907
plot_orig =2010
ys=30
yd=365.25
########################################
add_index =(plot_orig-simu_start)*yd
data_dict=dict([
# ('Drilling 02',(254 ,947994.5075,2105054.455,'temp10_02a',1 )),
 ('Drilling 04',(254 ,947927.3195,2105049.740,'temp10_04a',2 )),
 ('Drilling 05',(254 ,948019.5137,2105027.943,'temp10_05a',3 )),
 ('Drilling 10',(254 ,948050.6787,2105008.754,'temp10_10a',4 )),
# ('Drilling 13',(254 ,947962.6118,2105052.789,'temp10_13a',5 )),
# ('Drilling 17',(254 ,948143.1283,2105018.400,'temp10_17a',6 )),
# ('Drilling 18',(254 ,947877.9994,2105062.178,'temp10_18a',7 )),
])

result=np.asarray([[ float(k) for k in line.split()] for line in open(name,'r')])

colors=['tab:blue','tab:orange','tab:green','tab:red','tab:purple','tab:brown','tab:pink','tab:gray','tab:olive','tab:cyan']

#########################################
matplotlib.rc('axes.formatter'  ,limits=(-2,3))
matplotlib.rc('lines'           ,markersize=6.0)
matplotlib.rc('scatter'         ,marker="s")
matplotlib.rc('xtick'           ,labelsize=24)
matplotlib.rc('ytick'           ,labelsize=24)
matplotlib.rc('axes'            ,titlesize=22)
matplotlib.rc('axes'            ,labelsize=22)
matplotlib.rc('legend'          ,fontsize=20)
matplotlib.rc('legend'          ,title_fontsize=22)
matplotlib.rc('legend'          ,frameon=False)
matplotlib.rc('text'            ,usetex=True)
matplotlib.rc('figure'          ,titlesize=36)
matplotlib.rc('savefig'  ,format="pdf")
matplotlib.rc('savefig'  ,bbox="tight")
##########################################

fig,axe=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
fig.suptitle('Temperature comparison for year 2010')
axe.set_xlabel(r'T (C)')
axe.set_ylabel(r'Depth (m)')
axe.set_xlim([-3.0,0.2])
axe.set_ylim([-70.0,-10.0])
axe.ticklabel_format(axis='x', style='plain')
axe.ticklabel_format(axis='y', style='plain')	
for key,point in data_dict.items():
	time=round((add_index+point[0])/ys)
	file_t=np.loadtxt(data_dir+'/'+point[3],delimiter=';',comments='#')
	res_mask=(result[:,0]==time)*(result[:,4]==point[1])*(result[:,5]==point[2])*(result[:,2]==point[4])
	axe.plot(result[res_mask,40],result[res_mask,6]-np.max(result[res_mask,6]),label=key+" Simu.",color=colors[point[4]-1])
	axe.plot(file_t[:,1],file_t[:,0],label=key+" Data",linestyle='dashed',color=colors[point[4]-1])
fig.show()
axe.legend()
fig.savefig('TempDril_2010'+simu_name+'.pdf')
#######################################
input()	
