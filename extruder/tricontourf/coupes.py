import numpy as np
import math
import sys, os,operator
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.tri as tri
import scipy.interpolate as inter
#####################################
data_str=["bed","1950","2007","2019"]
comput_str=["1341","3021","3381"]
colors=['tab:blue','tab:orange','tab:green']
PA=[]
PB=[]
PC=[]
SA=[]
SB=[]
SC=[]
RA=(948153,2105143)
RB=(948054,2105154)
RC=(947969,2105155)

for i in data_str: 
    PA.append(np.loadtxt('PA_'+i+'.dat',comments='#',delimiter=','))
    PB.append(np.loadtxt('PB_'+i+'.dat',comments='#',delimiter=','))
    PC.append(np.loadtxt('PC_'+i+'.dat',comments='#',delimiter=','))

for i in comput_str:
    SA.append(np.loadtxt('PA_secular_'+sys.argv[1]+'_t'+i+'.txt',comments='#',delimiter=','))
    SB.append(np.loadtxt('PB_secular_'+sys.argv[1]+'_t'+i+'.txt',comments='#',delimiter=','))
    SC.append(np.loadtxt('PC_secular_'+sys.argv[1]+'_t'+i+'.txt',comments='#',delimiter=','))

#####################################
matplotlib.rc('axes.formatter'  ,limits=(-2,3))
matplotlib.rc('lines'           ,markersize=6.0)
matplotlib.rc('scatter'         ,marker="s")
matplotlib.rc('scatter'         ,edgecolors='none')
matplotlib.rc('xtick'           ,labelsize=24)
matplotlib.rc('ytick'           ,labelsize=24)
matplotlib.rc('axes'            ,titlesize=36)
matplotlib.rc('axes'            ,labelsize=36)
matplotlib.rc('legend'          ,fontsize=24)
matplotlib.rc('legend'          ,title_fontsize=24)
matplotlib.rc('legend'          ,frameon=False)
matplotlib.rc('text'            ,usetex=True)
matplotlib.rc('figure'          ,titlesize=36)
matplotlib.rc('savefig'  ,format="pdf")
matplotlib.rc('savefig'  ,bbox="tight")
#######################################
figA,axeA=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
figA.suptitle(r'Profils de surface libre coupe A')
axeA.set_ylabel(r'Altitude (m)')
axeA.set_xlabel(r'Distance (m)')
axeA.ticklabel_format(axis='both',style='plain')
axeA.set_ylim(ymin=3150,ymax=3280)
########
figB,axeB=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
figB.suptitle(r'Profils de surface libre coupe B')
axeB.set_ylabel(r'Altitude (m)')
axeB.set_xlabel(r'Distance (m)')
axeB.ticklabel_format(axis='both',style='plain')
axeB.set_ylim(ymin=3100,ymax=3250)
########
figC,axeC=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
figC.suptitle(r'Profils de surface libre coupe C')
axeC.set_ylabel(r'Altitude (m)')
axeC.set_xlabel(r'Distance (m)')
axeC.ticklabel_format(axis='both',style='plain')
axeC.set_ylim(ymin=3080,ymax=3200)
#####################################

for arr in SA:
    filtre=arr[:,3]!=0.0
    surf_new=np.transpose(np.vstack((((arr[filtre,65]-RA[0])**2+(arr[filtre,66]-RA[1])**2)**0.5,arr[filtre,67])))
    PA.append(surf_new[surf_new[:,0].argsort()])

for arr in SB:
    filtre=arr[:,3]!=0.0
    surf_new=np.transpose(np.vstack((((arr[filtre,65]-RB[0])**2+(arr[filtre,66]-RB[1])**2)**0.5,arr[filtre,67])))
    PB.append(surf_new[surf_new[:,0].argsort()])

for arr in SC:
    filtre=arr[:,3]!=0.0 
    surf_new=np.transpose(np.vstack((((arr[filtre,65]-RC[0])**2+(arr[filtre,66]-RC[1])**2)**0.5,arr[filtre,67])))
    PC.append(surf_new[surf_new[:,0].argsort()])

axeA.plot(PA[0][:,0],PA[0][:,1],'-k',label="Lit rocheux",lw=3)
axeB.plot(PB[0][:,0],PB[0][:,1],'-k',label="Lit rocheux",lw=3)
axeC.plot(PC[0][:,0],PC[0][:,1],'-k',label="Lit rocheux",lw=3)

imax=len(comput_str)

for i in range(imax):
    axeA.plot(PA[i+imax+1][:,0],PA[i+imax+1][:,1],label="Simulation "+data_str[i+1],color=colors[i],lw=2)
    axeA.plot(PA[i+1][:,0],PA[i+1][:,1],label="Mesures "+data_str[i+1],color=colors[i],lw=2,linestyle='dashed')
    axeB.plot(PB[i+imax+1][:,0],PB[i+imax+1][:,1],label="Simulation "+data_str[i+1],color=colors[i],lw=2)
    axeB.plot(PB[i+1][:,0],PB[i+1][:,1],label="Mesures "+data_str[i+1],color=colors[i],lw=2,linestyle='dashed')
    axeC.plot(PC[i+imax+1][:,0],PC[i+imax+1][:,1],label="Simulation "+data_str[i+1],color=colors[i],lw=2)
    axeC.plot(PC[i+1][:,0],PC[i+1][:,1],label="Mesures "+data_str[i+1],color=colors[i],lw=2,linestyle='dashed')


figA.show()
figA.legend(ncol=2,bbox_to_anchor=(0.8, 0.8))
figA.savefig("coupeA"+sys.argv[1]+".pdf")
figB.show()
figB.legend(ncol=2,bbox_to_anchor=(0.8, 0.85))
figB.savefig("coupeB"+sys.argv[1]+".pdf")
figC.show()
figC.legend(ncol=2,bbox_to_anchor=(0.9, 0.35))
figC.savefig("coupeC"+sys.argv[1]+".pdf")
input()
