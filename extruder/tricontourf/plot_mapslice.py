import numpy as np
import math
import sys, os,operator
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.tri as tri
import scipy.interpolate as inter
#####################################
data=np.loadtxt('ice_secular_'+sys.argv[1]+'.txt',comments='#',delimiter=',')
bed=np.loadtxt('bed_secular_'+sys.argv[1]+'.txt',comments='#',delimiter=',')
surf=np.loadtxt('surf_secular_'+sys.argv[1]+'.txt',comments='#',delimiter=',')
#####################################
matplotlib.rc('axes.formatter'  ,limits=(-2,3))
matplotlib.rc('lines'           ,markersize=6.0)
matplotlib.rc('scatter'         ,marker="s")
matplotlib.rc('scatter'         ,edgecolors='none')
matplotlib.rc('xtick'           ,labelsize=24)
matplotlib.rc('ytick'           ,labelsize=24)
matplotlib.rc('axes'            ,titlesize=36)
matplotlib.rc('axes'            ,labelsize=36)
matplotlib.rc('legend'          ,fontsize=24)
matplotlib.rc('legend'          ,title_fontsize=24)
matplotlib.rc('legend'          ,frameon=False)
matplotlib.rc('text'            ,usetex=True)
matplotlib.rc('figure'          ,titlesize=36)
matplotlib.rc('savefig'  ,format="pdf")
matplotlib.rc('savefig'  ,bbox="tight")
#######################################
figslice,axeslice=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
figslice.suptitle(r'Température '+sys.argv[2])
axeslice.set_aspect(aspect=1)
axeslice.set_ylabel(r'Altitude (m)')
axeslice.set_xlabel(r'Distance (m)')
axeslice.ticklabel_format(axis='both',style='plain')
axeslice.set_ylim(ymin=2980,ymax=3300)
############
figvelo,axevelo=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
figvelo.suptitle(r'Vitesse '+sys.argv[2])
axevelo.set_aspect(aspect=1)
axevelo.set_ylabel(r'Altitude (m)')
axevelo.set_xlabel(r'Distance (m)')
axevelo.ticklabel_format(axis='both',style='plain')
axevelo.set_ylim(ymin=2980,ymax=3300)
#####################################
tmin=-3.0
tmax=0.0
velmin=0.0
velmax=1.5
indup=np.argmax(data[:,1])
x_init=data[indup,65]
y_init=data[indup,66]

print(x_init)
print(y_init)

surf_new=np.transpose(np.vstack((((surf[:,65]-x_init)**2+(surf[:,66]-y_init)**2)**0.5,surf[:,67])))
surf_sorted=surf_new[surf_new[:,0].argsort()]
bed_new=np.transpose(np.vstack((((bed[:,65]-x_init)**2+(bed[:,66]-y_init)**2)**0.5,bed[:,67])))
bed_sorted=bed_new[bed_new[:,0].argsort()]

axeslice.tricontourf(((data[:,65]-x_init)**2+(data[:,66]-y_init)**2)**0.5,data[:,67],data[:,46],cmap='plasma',vmin=tmin,vmax=tmax,levels=12)
axeslice.fill_between(surf_sorted[:,0],surf_sorted[:,1],np.amax(surf_sorted[:,1]),color='w')
axeslice.fill_between(bed_sorted[:,0],np.amin(bed_sorted[:,1]),bed_sorted[:,1],color='w')
figslice.colorbar(matplotlib.cm.ScalarMappable(matplotlib.colors.Normalize(vmin=tmin,vmax=tmax),cmap='plasma'),location='top',orientation='horizontal',label='°C')

axevelo.tricontourf(((data[:,65]-x_init)**2+(data[:,66]-y_init)**2)**0.5,data[:,67],(data[:,62]**2+data[:,63]**2+data[:,64]**2)**0.5,cmap='plasma',vmin=velmin,vmax=velmax,levels=12)
axevelo.fill_between(surf_sorted[:,0],surf_sorted[:,1],np.amax(surf_sorted[:,1]),color='w')
axevelo.fill_between(bed_sorted[:,0],np.amin(bed_sorted[:,1]),bed_sorted[:,1],color='w')
figvelo.colorbar(matplotlib.cm.ScalarMappable(matplotlib.colors.Normalize(vmin=velmin,vmax=velmax),cmap='plasma'),location='top',orientation='horizontal',label='m/a')



figslice.show()
figslice.savefig("temp_secular_"+sys.argv[1]+".pdf")
figvelo.show()
figvelo.savefig("velo_secular_"+sys.argv[1]+".pdf")
input()
