import numpy as np

SB_2019=np.loadtxt("PB_2019.txt",delimiter=",")
xr=948045
yr=2105030
filter=(SB_2019[:,1]<=xr)*(SB_2019[:,2]<=yr)
NSB_2019=np.zeros((SB_2019[filter,0].shape[0],2))
NSB_2019[:,0]=((SB_2019[filter,1]-xr)**2+(SB_2019[filter,2]-yr)**2)**0.5
NSB_2019[:,1]=SB_2019[filter,3]
np.savetxt("PB_2019.dat",NSB_2019[NSB_2019[:,0].argsort()],delimiter=",")
