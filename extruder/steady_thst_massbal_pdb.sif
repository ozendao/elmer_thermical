check keywords warn
echo on
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
$Step           ="teterousse_steady_thst_pdb"
#yearinsec      =365.25*24*60*60
#Cp_rock        =820.0
#rock_k         =3.2
#diff_rock      =rock_k/Cp_rock*yearinsec
#L_heat         =334000.0
#T_ref_enthalpy =200
#E_Difw         =1.045e-4
#geoFlux        =0.02
#rhos           =350.0
#rhoi           =917.0
#rhow           =1000.0
#rhor           =2800.0
#gravity        =-9.81
#zeroCel        =273.15 
#H_Cap_A        =7.253 
#H_Cap_B        =146.3
#gazpc          =8.314
#shift_temp     =263.15
#fluidity_correc=3
#cpice_MB       =2050.0
#cprock_MB      =2000.0
!!!!!!  Prefactor from Cuffey and Paterson (2010) in MPa^{-3} a^{-1}
#A1             =2.89165e-13*yearinsec*1.0e18 
#A2             =2.42736e-02*yearinsec*1.0e18 
#Q1             =60.0e3
#Q2             =115.0e3 
!!!!!! Calone 2019
#rho_transition =450 
!---LUA BEGIN
!
!function diffusivity_calc(d,t)
!  theta=1.0/(1+math.exp(-2.0*0.02*(d-rho_transition)))
!  kfirn=2.107+0.003618*(d*rhoi-rho_transition)
!  ksnow=0.024-1.23e-4*d*rhoi+2.5e-6*(d*rhoi)^2
!  g=(1.0-theta)*ksnow+theta*kfirn
!  g=g*(9.828*math.exp(-5.7e-3*(t+zeroCel)))/2.1*yearinsec
!  g=g/(H_Cap_B+H_Cap_A*(t+zeroCel))
!  return g
!end
!
!function calc_temp(PCE,Enthalpy_h)
!  if (Enthalpy_h<PCE) then
!    g=(-H_Cap_B+(H_Cap_B^2+H_Cap_A*(H_Cap_A*T_ref_enthalpy^2+2*(H_Cap_B*T_ref_enthalpy+Enthalpy_h)))^0.5) / H_Cap_A
!  else
!    g=(-H_Cap_B+(H_Cap_B^2+H_Cap_A*(H_Cap_A*T_ref_enthalpy^2+2*(H_Cap_B*T_ref_enthalpy+PCE)))^0.5) / H_Cap_A
!  end
!  return g
!end
!
!function  fluidity_calc(t)
!  temp=t+zeroCel
!  if (temp<shift_temp) then
!   return  2.0*A1*math.exp(-Q1/(gazpc*temp))*fluidity_correc
!  else
!    return 2.0*A2*math.exp(-Q2/(gazpc*temp))*fluidity_correc
!  end
!end
!
!function dot_n(n1,n2,v1,v2,v3)
!  return (n1*v1+n2*v2+v3)/(1+n1^2.0+n2^2.0)^0.5
!end
!
!---LUA END
!!!!
#bfg3    =gravity*yearinsec^2*1e-18
#DensR   =rhor/(1.0e6*yearinsec^2)
#DensI   =rhoi/(1.0e6*yearinsec^2)
#EntWD   =E_Difw*yearinsec
#EHF     =geoFlux*yearinsec
#HFlux   =geoFlux*1.0e-6*yearinsec         !MPa m a-1
#Cp_r    =820.0*yearinsec^2
#rock_cap=rock_k*1.0e-6*yearinsec^2
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Header
   Mesh DB "." "teterousse_rockice"
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Constants
   T_ref_enthalpy= real #T_ref_enthalpy
   L_heat= real #L_heat
  ! Cp(T) = A*T + B
  Enthalpy Heat Capacity A= real #H_Cap_A
  Enthalpy Heat Capacity B= real #H_Cap_B
  P_triple= real 0.061173 !Triple point pressure for water (MPa)
  T_triple= real #zeroCel !Triple point temperature for water (K)
  P_surf= real 0.1013 ! Surface atmospheric pressure(MPa)
  beta_clapeyron= real 0.0974 ! clausus clapeyron relationship (K MPa-1)
  Pressure Variable= String "Pressure"
  yearstartdata = real 1907.0
  MoyAirTemperatureFile= File "Dataclimat/TempLyonMoy1907_1930.dat"
  MaskReliefFile = File "DataTeterousse/MaskRelief_TeteRousse.dat"
  MaskAccuFile = File "DataTeterousse/Accu_mean.dat"
  MaskAccuNx=integer 268
  MaskAccuNy=integer 118
  MaskReliefNx = integer 933
  MaskReliefNy = integer 726
  MaskReliefRadStep= real 500.0
  MaskReliefRadStepNb = integer 20 
  OutputFile = File "DataTeterousse/TR_outputpoints.dat"
  TempCorrec= real 0.0            ! Shift on temperature - Initial value = 0.0 
  Precip= real #1.518
  Latitude= real 46.0    ! Latitude for potential radiation computation
  rho_ice= real #rhoi
  rho_rock= real #rhor
  rho_water= real #rhow
  rho_surf= real #rhos
  rho_transition= real #rho_transition
  cpice_MB= real #cpice_MB
  cprock_MB=real #cprock_MB
  z_temp= real 2469.0	  ! Altitude of temperature measurement in file
  z_precip= real 3139.0  ! Altitude of precip measurement in file
  !Gas Constant = Real 8.314 !Joule/mol x  K
  seuil_precip= real 2.0  ! Threshold temperature for snow/rain transition
  RadFact= real 0.7        ! Ice Radiation factor correction  
  Albedo_ice= real 0.3     ! Ice Albedo
  Albedo_snow= real 0.71     ! Snow Albedo
  ksnow = real 5.0     	! Energy input for snow per degree (W/m2 K-1)
  kice = real 5.0     	! Energy input for ice per degree (W/m2 K-1)
  k0 = real -10.0			! Energy balance at 0°C (W/m2) 
  firn_param=real 10.0   ! Densification parameter to compute firn thickness
  GradTemp= real 0.006   ! Air temperature Lapse Rate
  GradPrecip= real 0.0037  ! Precipitation Lapse Rate (multiplicative factor)
  MaxAltFact= real 2.0   ! Maximum multiplicative factor for altitude
  MinAltFact= real 0.5   ! Minimum multiplicative factor for altitude
  Residual Saturation= real 0.005
end
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Simulation
  Coordinate System  = Cartesian 3D 
  Simulation Type = Transient
  Timestepping Method = BDF
  BDF order = 2

  Timestep Intervals = 1
  Timestep Sizes = 1

 
  Steady State Min Iterations = 1
  Steady State Max Iterations = 30
  Output Intervals = 1
  Output File = "$Step".result"
  Post File = "$Step".vtu"
  max output level = 3
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Body 1
  Equation = 1
  Body Force = 1
  Material = 1
  Initial Condition = 1
End
!!!!
Body 2
  Equation = 2
  Body Force = 2
  Material = 2
  Initial Condition = 2
End
Body 3
  Equation = 3
  Material =2
  Body force=2
  Initial Condition=3
end

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Initial Condition 1
  Velocity 1 = real 0.0
  Velocity 2 = real 0.0
  Velocity 3 = real 0.0
  Pressure = real 0.0
 !rock_temperature= real -5.0
 rock_temperature= real -2.0
End
!!!!
Initial Condition 2
  Velocity 1 = real 0.0
  Velocity 2 = real 0.0
  Velocity 3 = real 0.0
  Pressure = real 0.0
  !ice_temperature= real -5.0 
  temperature= real -2.0 
  DensRel = real 1.0
  !enthalpy_h = real 125700.0
  enthalpy_h = real 132000.0
End
Initial Condition 3
!  Firn = real 2.0
!  DensRel = real 1.0
  Zs Top = Variable Coordinate 3
    Real Procedure "ElmerIceUSF" "ZsTopIni"

  Ref Zs Top = Variable Coordinate 3
    Real Procedure "ElmerIceUSF" "ZsTopIni"
end
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Body Force 1
  rock_temperature = Equals BedTemperatureExp
  rock_temperature Condition = Variable Depth
    real LUA "17-tx[0]"
End
!!!!!
Body Force 2
  Flow BodyForce 1 = Real 0.0    
  Flow BodyForce 2 = Real 0.0            
  Flow BodyForce 3 = Real #bfg3 

   Zs Top Accumulation Flux 1 = Real 0.0e0
   Zs Top Accumulation Flux 2 = Real 0.0e0
!   Zs Top Accumulation Flux 3 = Equals Accu
   Zs Top Accumulation Flux 3 = Real 0.0e0

!  Dens Source = variable StrainRate 7
!     Real LUA "tx[0]*0.0+1.0"
  DensRel lower limit = real 0.3
  DensRel upper limit = real 1.0
  Enthalpy Heat Source = variable Flow Solution Deformational Heat,DensRel
    real LUA "tx[0]*(1.0e6*yearinsec^2)/(rhoi*tx[1])"
  Enthalpy_h Upper Limit = Variable Phase Change Enthalpy
	real LUA "tx[0] +0.03*L_heat"
  Enthalpy_h Lower Limit = real 0.0
  Enthalpy_h = Equals SurfaceEnthExp
  Enthalpy_h Condition = Variable Depth
 	real LUA "17-tx[0]"
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Material 1
  !Density =  Real #DensR 
  !Heat Capacity = Real #Cp_r
  !Heat Conductivity = real #rock_cap
  Density = Real #rhor
  Heat Capacity = Real #Cp_rock
  Heat Conductivity = real #rock_k
  Convection Velocity 1 =  real 0.0
  Convection Velocity 2 =  real 0.0
  Convection Velocity 3 =  real 0.0
  Young Modulus = Real 1.0
  Poisson Ratio = real 0.3
End
!!!!
Material 2 
  Density = Real #DensI 
  !Density = Real #rhoi 
  Viscosity Model = String "Glen"
  Viscosity = Real 1.0 
  Glen Exponent = Real 3.0
  Critical Shear Rate = Real 1.0e-10
  Rate Factor 1 = Real #A1  
  Rate Factor 2 = Real #A2
  Activation Energy 1 = Real #Q1
  Activation Energy 2 = Real #Q2  
  Glen Enhancement Factor = Real 1.0
  Limit Temperature = Real -10.0
  !Relative Temperature = variable temperature !Real 0.0
  !  real LUA "tx[0]"
  !Constant Temperature = variable temperature
  !  real LUA "tx[0]"
  Enthalpy Density = Variable DensRel
	real LUA "tx[0]*rhoi"
  Enthalpy Heat Diffusivity = variable Densrel, temperature
     Real LUA "diffusivity_calc(tx[0],tx[1])"
  Enthalpy Water Diffusivity = real #EntWD 

  Young Modulus = Real 1.0
  Poisson Ratio = real 0.3
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 1
  exec solver = before simulation
  Bubbles in Global System = FALSE
  Equation = "Flowdepthinidepth"
  Procedure = File "bin/Flowdepth" "FlowDepthSolver"
  !Procedure = File "ElmerIceSolvers" "FlowDepthSolver"
  Variable = String "Depth"
  Variable DOFs = 1
  Linear System Solver = "Direct"
  Gradient = Real -1.0E00 
  Calc Free Surface = Logical True
  Freesurf Name = String "Surf"   
End
!!!!
Solver 2
   exec solver = before simulation
  Bubbles in Global System = FALSE
   Equation = "Flowdepthiniheight"
   Procedure = File "ElmerIceSolvers" "FlowDepthSolver"
   Variable = String "Height"
   Variable DOFs = integer 1
   Linear System Solver = "Direct"
   Gradient = Real 1.0E00
   Calc Free Surface = Logical True
   Freesurf Name = String "Bedr"  
End
!!!!
Solver 3
  Equation = String "Mesh Update"
  Bubbles in Global System = FALSE
  Linear System Solver = String "Direct"
  Linear System Direct Method = String "MUMPS"
  Steady State Convergence Tolerance = Real 1.0e-04
  Mumps percentage increase working space = Integer 600
End
!!!!!
Solver 4
  Equation = "Flowdepthdepth"
  Bubbles in Global System = FALSE
  Procedure = File "bin/Flowdepth" "FlowDepthSolver"
  !Procedure = File "ElmerIceSolvers" "FlowDepthSolver"
  Variable = String "Depth"
  Variable DOFs = 1
  Linear System Solver = "Direct"
  Gradient = Real -1.0E00 
  Calc Free Surface = Logical True
  Freesurf Name = String "Surf"
End
!!!!!!!!
Solver 5
   Equation = "Flowdepthheight"
   Bubbles in Global System = FALSE
   Procedure = File "ElmerIceSolvers" "FlowDepthSolver"
   Variable = String "Height"
   Variable DOFs = integer 1
   Linear System Solver = "Direct"
   Gradient = Real 1.0E00
   Calc Free Surface = Logical True
   Freesurf Name = String "Bedr"  
End
!!!!!!
!Solver 6
!  !Exec Interval = 10  ! Need to be set if transient MB
!  Timestep Scale = Real 1.0 ! Need to be set if transient MB
!  exec solver = never
!  Transient MB = Logical False
!  Equation = SurfBoundary
!  Variable = -dofs 1 "Mass Balance"
!  Procedure = File "bin/MassBalance" "TransientMassBalance"
!
!  Exported Variable 1 = -dofs 1 "melting" 
!  Exported Variable 2 = -dofs 1 "rain" 
!  Exported Variable 3 = -dofs 1 "Firn" 
!  Exported Variable 4 = -dofs 1 "Accu" 
!  Exported Variable 5 = -dofs 1 "Surface Enthalpy" 
!  Exported Variable 6 = -dofs 1 "PotRad" 
!  ! Possibility to export more variable : Firn, accu, rain, melting ... see in the code
!End
Solver 6
  !Exec Interval = 10 
  Equation = "ExportVertically1"
  Bubbles in Global System = FALSE
  Procedure = File "ElmerIceSolvers" "ExportVertically"
  Variable = String "BedTemperatureExp"
  Variable DOFs = 1
  Linear System Solver = "Direct"
  Linear System Direct Method = MUMPS
  Mumps percentage increase working space = Integer 600
End
Solver 7
  !Exec Interval = 10 
  Equation = "ExportVertically2"
  Bubbles in Global System = FALSE
  Procedure = File "ElmerIceSolvers" "ExportVertically"
  Variable = String "SurfaceEnthExp"
  Variable DOFs = 1
  Linear System Solver = "Direct"
  Linear System Direct Method = MUMPS
  Mumps percentage increase working space = Integer 600
End
Solver 8
  !exec solver = never
  !exec interval = integer 10
  Transient Simu = logical false
  Nb_steady_simu = integer 1000000
  Bubbles in Global System = FALSE
  Variable = string "rock_temperature"
  Equation = String "Heat Equation"
  Procedure = File "bin/HeatSolver" "Heatsolver"
  Linear System Solver = "Direct"
  Linear System Direct Method = "MUMPS"
  Mumps percentage increase working space = Integer 600
  Steady State Convergence Tolerance = 1.0E-04
  Nonlinear System Convergence Tolerance = 1.0E-05
  Nonlinear System Max Iterations = 50
  Nonlinear System Relaxation Factor = Real 9.999E-01
  Apply Dirichlet = Logical True
  Stabilize = True
End
Solver 9
  !Exec interval = integer 10
  !exec solver = never
  Equation = string "Flux Solver"
  Bubbles in Global System = FALSE
  Procedure = File "FluxSolver" "FluxSolver"
  Target Variable = String "rock_temperature"
  Calculate Flux = True
  Flux Coefficient = String "Heat Conductivity"
  Linear System Solver = "Direct"
  Linear System Direct Method = "MUMPS"
  Mumps percentage increase working space = Integer 600
End
Solver 10
  !exec solver=never
  !Equation = "Navier-Stokes"
  !Stabilization Method = String Stabilized
  Equation = "Stokes-Vec"
  Procedure = "IncompressibleNSVec" "IncompressibleNSSolver"
  Div-Curl Discretization = Logical False
  Stokes Flow = logical true
  Flow Model = Stokes
  Exported Variable 1 = -dofs 1 "dSdt" 
  Exported Variable 2 = -dofs 1 "dS" 
  Linear System Solver = Direct         
  Linear System Direct Method = MUMPS
  Mumps percentage increase working space = Integer 600
  Nonlinear System Max Iterations = 50
  Nonlinear System Convergence Tolerance  = 1.0e-5
  Nonlinear System Newton After Iterations = 5
  Nonlinear System Newton After Tolerance = 1.0e-05
  Nonlinear System Reset Newton = Logical True
  Nonlinear System Relaxation Factor = 1.00
  Steady State Convergence Tolerance = Real 1.0e-5
  Relative Integration Order = -1
  Constant-Viscosity Start = Logical False
  Calculate Loads = Logical True
  End
Solver 11
  !exec solver=never
  Equation = "Strain Rate"
  Bubbles in Global System = FALSE
  Procedure = "ElmerIceSolvers" "ComputeStrainRate"
  Variable = "Eij"
  Variable DOFs = Integer 1
  Exported Variable 1 = "StrainRate"
  Exported Variable 1 DOFs = Integer 7
  Flow Solver Name = String "Flow Solution"
  StrainRate Variable Name = String "StrainRate"
  Linear System Solver = String "Direct"
  Linear System Direct Method = String "MUMPS"
  Mumps percentage increase working space = Integer 600
End

Solver 12
  !exec solver=never
  Equation="DeformationalHeat"
  Bubbles in Global System = FALSE
  procedure =  "ElmerIceSolvers" "DeformationalHeatSolver"
  Variable="Flow Solution Deformational Heat"
  Variable DOFs=1
  Linear System Solver = Direct
  Linear System Direct Method = MUMPS
  Mumps percentage increase working space = Integer 600
  Flow Solver Name = String "Flow Solution"
End
Solver 13
  !exec solver=never
  !exec interval = integer 10
  Transient Simu = logical false
  Bubbles in Global System = FALSE
  Nb_steady_simu = integer 1000000
  Equation = String "Enthalpy Equation"
  Procedure = File "bin/EnthalpySolver" "EnthalpySolver"
  Variable = String "Enthalpy_h"
  Linear System Solver = String "Direct"         
  Linear System Direct Method = String "MUMPS"
  Mumps percentage increase working space = Integer 600
  Steady State Convergence Tolerance = Real 1.0E-04
  Nonlinear System Convergence Tolerance = Real 1.0E-04
  Nonlinear System Max Iterations = integer 6
  Nonlinear System Relaxation Factor = Real 1.0 
  Apply Limiter = Logical true
  Apply Dirichlet = Logical True
  Stabilize = Logical True
  Exported Variable 1 = String "Phase Change Enthalpy"
  Exported Variable 1 DOFs = integer 1
  Exported Variable 2 = String "water content"
  Exported Variable 2 DOFs = integer 1
  Exported Variable 3 = String "temperature"
  Exported Variable 3 DOFs = integer 1
  Flow Solver Name = String "Flow Solution"
  Flow Loads Name = String "Flow Solution Loads"
  Pressure Variable = String "Pressure"
End

Solver 14
!  exec solver=never
  Equation =  String "Free Surface Evolution"
  Variable = -dofs 1 "Zs Top"
  Procedure = "FreeSurfaceSolver" "FreeSurfaceSolver"
   Flow Solution Name = String "Flow Solution"
  ! calculate dz/dt 
  Calculate Velocity = Logical False
  ! apply internal limiters
  Apply Dirichlet = Logical true
  Linear System Max Iterations  = 10000
  Linear System Convergence Tolerance = 1.0e-08
  Free Surface Convergence Tolerance = 1.0e-12
  Free Surface Max Iterations = 100
  ! non-linear system settings
  Nonlinear System Max Iterations = 100 ! variational inequality needs more than one round
  Nonlinear System Min Iterations = 2
  Nonlinear System Convergence Tolerance = 1.0e-6 
  ! convergence on timelevel
  Steady State Convergence Tolerance = 1.0e-4
  Stabilization Method = Stabilized
  ! loads also takes into account dirichlet conditions to compute residual flux
  calculate loads = Logical True
  Exported Variable 1 = "Zs Top Residual"
  Exported Variable 2 = "Ref Zs Top"
End
!Solver 16
!  Procedure = "SaveData" "SaveLine"
!  Equation = "1Dsave ice"
!  Filename = "verticaltemp.dat"
!  Output Directory = "."
!  Polyline Coordinates (2,3)= 948050 2105000 3100 948050 2105000 3300
!  Variable 1 = "temperature"
!  Parallel Reduce = True
!End
!Solver 15
!  Equation = "Save 1D Vars"
!  Procedure = File "bin/Scalar_OUTPUT_Glacier" "Scalar_OUTPUT"
!  Variable = -nooutput "savescal"
!  File Name = File "OUT_1D_TR_st_thst_MB.dat"
!  XRefFrontPosition = real 9.552320e+05
!  YRefFrontPosition = real 1.196998e+05
!  SMB altitude Range (2) = real 1600.0 3600.0  
!End


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Equation 1
  Active Solvers (8)= 1 2 3 4 5 6 8 9
  Flow Solution Name = String "Flow Solution"
  Convection = String Constant
End
Equation 2
  Active Solvers(10) = 1 2 3 4 5 7 10 11 12 13 
  Flow Solution Name = String "Flow Solution"
  Navier-Stokes = Logical True
  Convection = String Computed
End
Equation 3
 Active Solvers(1) = 14
  Flow Solution Name = String "Flow Solution"
  Convection = String Computed
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ground 
Boundary Condition 1
  Target Boundaries = 1
  Mesh Update 1 = Real 0.0
  Mesh Update 2 = Real 0.0
  Mesh Update 3 = Real 0.0  
  Heat Flux BC = logical True
  !Heat Flux = real  #HFlux
  Heat Flux = real #geoFlux 
!  DensRel = real 1.0
End
! ice Bed 
Boundary Condition 2
  Target Boundaries = 2
  Velocity 1 = real 0.0
  Velocity 2 = real 0.0
  Velocity 3 = real 0.0
  Height = real 0.0
  Flowdepth Skip = Logical True
  Mesh Update 1 = Real 0.0
  Mesh Update 2 = Real 0.0
  Mesh Update 3 = Real 0.0
!  DensRel = real 1.0
  rock_temperature = Equals temperature
  BedTemperatureExp = Equals temperature
  Enthalpy Heat Flux BC = logical True
  Enthalpy heat flux = Variable bedrGrad1,bedrGrad2,rock_temperature Flux 1,rock_temperature Flux 2,rock_temperature Flux 3
    Real LUA "dot_n(tx[0],tx[1],tx[2],tx[3],tx[4])"
End
! ice Upper Surface
Boundary Condition 3
  Target Boundaries = 3
    Body Id = 3
  Mesh Update 1 = Real 0.0
  Mesh Update 2 = Real 0.0
  Mesh Update 3 = Variable Zs Top
   Real Procedure "ElmerIceUSF" "ZsTopMzsIni"
!  DensRel = real 0.38
  Enthalpy Heat Flux BC = logical True
  Enthalpy Heat Flux = real 0.0
  !SurfaceEnthExp = Equals Surface Enthalpy
   SurfaceEnthExp = variable coordinate 3
    real MATC "25000.0/150.0*(tx-3250)+140000.0"
  Depth= Real 0.0
End
!ice lateral border
Boundary Condition 4
  Target Boundaries = 4
  Normal-Tangential Velocity = Logical True 
  Velocity 1 = real 0.0
  Mesh Update 1 = Real 0.0
  Mesh Update 2 = Real 0.0
End
!rock lateral border
Boundary Condition 5
  Target Boundaries = 5
  Heat Flux BC = logical True
  Heat Flux = 0.0
  Mesh Update 1 = Real 0.0
  Mesh Update 2 = Real 0.0
End
