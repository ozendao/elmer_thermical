import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib


data=[]
data.append(np.loadtxt("OUT_1D_TR_tr_secular_gradfssa.dat",delimiter=" "))
data.append(np.loadtxt("OUT_1D_TR_tr_secular_ganfssa.dat",delimiter=" "))
data.append(np.loadtxt("OUT_1D_TR_tr_secular_redfssa.dat",delimiter=" "))
data.append(np.loadtxt("OUT_1D_TR_tr_secular_renfssa.dat",delimiter=" "))
data.append(np.loadtxt("OUT_1D_TR_tr_secular_renfssarad.dat",delimiter=" "))
data.append(np.loadtxt("OUT_1D_TR_tr_secular_renfssarsv.dat",delimiter=" "))
labels=[]
labels.append("dirichlet climat calé")
labels.append("neumann climat calé")
labels.append("dirichlet fonte +")
labels.append("neumann fonte +")
labels.append("neumann fonte + rad +")
labels.append("neumann fonte + sans NS")


ice_density=0.917
stepsperyear=30


matplotlib.rc('axes.formatter'  , limits=(-2,3))
matplotlib.rc('lines'           , markersize=0.3)
matplotlib.rc('xtick'           , labelsize=18)
matplotlib.rc('ytick'           , labelsize=18)
matplotlib.rc('axes'            , titlesize=20)
matplotlib.rc('axes'            , labelsize=20)
matplotlib.rc('legend'          , fontsize=16)
matplotlib.rc('legend'          , title_fontsize=18)
matplotlib.rc('legend'          , frameon=False)
matplotlib.rc('text'            ,usetex=True)
matplotlib.rc('figure'          ,titlesize=24)

fig,axe=plt.subplots(nrows=1,ncols=1,figsize=(18,12))

fig.suptitle("mass balance FSSA Teterousse")
axe.set_xlabel(r't(year)') 
axe.set_ylabel(r'Mass Balance (m of water)')
axe.ticklabel_format(axis='x', style='sci')
axe.ticklabel_format(axis='y', style='sci')

for tab,lab in zip (data,labels):
	cumul_MB=np.zeros(tab.shape[0])
	prev=0
	for i in range(tab.shape[0]):
		cumul_MB[i]=prev+(tab[i,6]/tab[i,3]*ice_density)/stepsperyear
		prev+=(tab[i,6]/tab[i,3]*ice_density)/stepsperyear
		
	axe.plot(tab[:,0],cumul_MB,label=lab)

axe.legend(loc="lower center")
fig.show()

input()
