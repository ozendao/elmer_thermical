import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib


name_simu=sys.argv[1]
year_init=1907
stepsperyear=30
indBM=22
yearoffset=1950
#indoffset=3000+indBM
indoffset=(yearoffset-year_init)*stepsperyear+indBM


data_m=[]
data_m.append(np.loadtxt("Dataclimat/bilanDGhomo_1801_1998.txt",delimiter=" "))
data_m.append(np.loadtxt("Dataclimat/bilanDGlyonbesse1908_2007.txt",delimiter=" "))
data_m.append(np.loadtxt("Dataclimat/bilanDGargentiere_1975_2022.txt",delimiter=" "))
data_m.append(np.loadtxt("Dataclimat/TR_CTOP_2010_2022.dat",delimiter=" "))
data_m.append(np.loadtxt("Dataclimat/TR_CARG_2010_2022.dat",delimiter=" "))

topo_array=np.loadtxt("Dataclimat/bilanCT1901_1950_2007_2019.txt",delimiter=" ")

labels_m=[]
labels_m.append("D.J. homogenise")
labels_m.append("D.J. Lyon-Besse")
labels_m.append("D.J. Argentiere")
labels_m.append("D.J. TR topo")
labels_m.append("D.J. TR Arg")

topo_label="Estimation Topo."


matplotlib.rc('axes.formatter'  , limits=(-2,3))
matplotlib.rc('lines'           , markersize=6.0)
matplotlib.rc('scatter'         , marker="s")
matplotlib.rc('xtick'           , labelsize=24)
matplotlib.rc('ytick'           , labelsize=24)
matplotlib.rc('axes'            , titlesize=26)
matplotlib.rc('axes'            , labelsize=26)
matplotlib.rc('legend'          , fontsize=20)
matplotlib.rc('legend'          , title_fontsize=24)
matplotlib.rc('legend'          , frameon=False)
matplotlib.rc('text'            ,usetex=True)
matplotlib.rc('figure'          ,titlesize=36)
matplotlib.rc('savefig'         ,format='pdf')
matplotlib.rc('savefig'         ,bbox='tight')

#scout_array=np.asarray([[float(k) for k in line.split()] for line in open("retour_dahu/scout_secular_"+name_simu+"_total.dat",'r')])
#mixed_array=np.asarray([[float(k) for k in line.split()] for line in open(name1+"_3d/scout_secular_"+name1+"mixed_total.dat",'r')])


ice_density=0.917
fig1,axe1=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
fig1.suptitle(r'Evolution de la masse')
axe1.set_xlabel(r't(année)') 
axe1.set_ylabel(r'Bilande masse cumulé (m. eq. eau)')
axe1.ticklabel_format(axis='x', style='plain')
axe1.ticklabel_format(axis='y', style='plain')
axe1.set_xlim((1900,2024))
#fig2,axe2=plt.subplots(nrows=1,ncols=2,figsize=(24,12))
#fig2.suptitle(r'TODO')
#axe2[0].set_xlabel(r't(year)')
#axe2[1].set_xlabel(r't(year)')
#axe2[0].set_ylabel(r'Altitulde (m)')
#axe2[1].set_ylabel(r'Area (\%)')


#tmask=((year_init+(np.floor((scout_array[:,0]-year_init)/365.25)+(21/stepsperyear)))<scout_array[:,0])*((year_init+(np.floor((scout_array[:,0]-year_init)/365.25)+(23/#stepsperyear)))>scout_array[:,0])
#print(tmask)

#axe2[0].plot(scout_array[:,0],scout_array[:,16],label="Firn front")
#axe2[1].plot(scout_array[:,0],100*scout_array[:,7]/scout_array[:,3],label="Bottom temperate ice")
#cumul_dzdt=np.zeros(scout_array.shape[0])
#cumul_dzdt_mixed=np.zeros(mixed_array.shape[0])
#prev_dzdt=0
#prev_dzdt_mixed=0
#for i in range(scout_array.shape[0]):
#	cumul_dzdt[i]=prev_dzdt+((scout_array[i,2]*ice_density)/scout_array[i,3])/stepsperyear
#	prev_dzdt+=((scout_array[i,2]*ice_density)/scout_array[i,3])/stepsperyear			
	#cumul_dzdt_mixed[i]=prev_dzdt_mixed+((mixed_array[i,2]*ice_density)/mixed_array[i,3])/stepsperyear
	#prev_dzdt_mixed+=((mixed_array[i,2]*ice_density)/mixed_array[i,3])/stepsperyear			

#offset=cumul_dzdt[indoffset]-topo_array[1,1]
#offset=-3.7
#offset_mixed=cumul_dzdt_mixed[indoffset]-topo_array[2,1]
#print(cumul_dzdt[indoffset]-topo_array[2,1])
#axe1.plot(scout_array[:,0],cumul_dzdt-offset,label=r'$$\int_{1907}^t\partial_t Z_s {\rm d} t\ {\rm (Simulation)}$$')
#axe1.plot(mixed_array[:,0],cumul_dzdt_mixed-offset_mixed,label="Computation IC shape")
for tab,lab in zip (data_m,labels_m):
	axe1.plot(tab[:,0],tab[:,1],label=lab)

axe1.scatter(topo_array[:,0],topo_array[:,1],label=topo_label,c='black',zorder=len(data_m)+2)



axe1.legend(loc="lower left")
#axe2[0].legend(loc="upper right")
#axe2[1].legend(loc="upper right")
fig1.savefig('MB_'+name_simu+'.pdf')
#fig2.savefig('RA_'+name1+name2+'.pdf')
fig1.show()
#fig2.show()
input()
