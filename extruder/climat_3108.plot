set title "Volume divisé par l'aire, resp colonnes 2 et 4 de la sortie de scalar output"
set xlabel "Date (années)"
set ylabel "Hauteur d'eau relative (m)"

plot "OUT_1D_TR_tr_ganrecul.dat" u 1:($2/$4-40.0) w l t "neuman 2*minh" lw 2, \
     "OUT_1D_TR_tr_gradrecul.dat" u 1:($2/$4-40.0) w l t "dirichlet 2*minh" lw 2, \
     "OUT_1D_TR_tr_ganrecul_moy.dat" u 1:($2/$4-33.9) w l t "neuman minh" lw 2, \
     "OUT_1D_TR_tr_gradrecul_moy.dat" u 1:($2/$4-33.9) w l t "dirichlet minh" lw 2, \
     "OUT_1D_TR_tr_ganrecul_fin.dat" u 1:($2/$4-31.5) w l t "neuman minh/2" lw 2, \
     "OUT_1D_TR_tr_gradrecul_fin.dat" u 1:($2/$4-31.5) w l t "dirichlet minh/2" lw 2, \
     "DataTeterousse/TR_smb_2013-2022.txt" u (2013.0+$0/365.25):($1+2.96512) \
      t "bilan manu" w l lw 3 dt "-"

pause -1
