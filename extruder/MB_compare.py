import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib


data_m=[]
data_m.append(np.loadtxt("Dataclimat/bilanDGhomo_1801_1998.txt",delimiter=" "))
data_m.append(np.loadtxt("Dataclimat/bilanDGlyonbesse1908_2007.txt",delimiter=" "))
data_m.append(np.loadtxt("Dataclimat/bilanDGargentiere_1975_2007.txt",delimiter=" "))
data_m.append(np.loadtxt("Dataclimat/TR_cumul_2010_2022.dat",delimiter=" "))

data_s=[]
data_s.append(np.loadtxt("Dataclimat/bilanCT1901_1950_2007.txt",delimiter=" "))

data_c=[]
data_c.append(np.loadtxt("OUT_1D_TR_tr_secular_mgt.dat",delimiter=" "))
data_c.append(np.loadtxt("OUT_1D_TR_tr_secular_mga.dat",delimiter=" "))
data_c.append(np.loadtxt("OUT_1D_TR_tr_secular_mgb.dat",delimiter=" "))
data_c.append(np.loadtxt("OUT_1D_TR_tr_secular_mgx.dat",delimiter=" "))
data_c.append(np.loadtxt("OUT_1D_TR_tr_secular_mgy.dat",delimiter=" "))
data_c.append(np.loadtxt("OUT_1D_TR_tr_secular_mgz.dat",delimiter=" "))

labels_m=[]
labels_m.append("DG homogenise")
labels_m.append("DG LyonBesse")
labels_m.append("DG Argentiere")
labels_m.append("DG TR -15.85")

labels_s=[]
labels_s.append("Topo TR")

labels_c=[]
labels_c.append("Fonte A bubb Firn 15")
labels_c.append("Fonte A stab Firn 15")
labels_c.append("Fonte A stab Firn 30")
labels_c.append("Fonte B bubb Firn 15")
labels_c.append("Fonte B stab Firn 15")
labels_c.append("Fonte B stab Firn 30")

ice_density=0.917
stepsperyear=30
indinit_homo=110
indinit_lyon=3
mean_init=0.5*(data_m[0][indinit_homo,1]+data_m[1][indinit_lyon,1])
indinit_res=4*stepsperyear+22

matplotlib.rc('axes.formatter'  , limits=(-2,3))
matplotlib.rc('lines'           , markersize=6.0)
matplotlib.rc('scatter'         , marker="s")
matplotlib.rc('xtick'           , labelsize=18)
matplotlib.rc('ytick'           , labelsize=18)
matplotlib.rc('axes'            , titlesize=20)
matplotlib.rc('axes'            , labelsize=20)
matplotlib.rc('legend'          , fontsize=16)
matplotlib.rc('legend'          , title_fontsize=18)
matplotlib.rc('legend'          , frameon=False)
matplotlib.rc('text'            ,usetex=True)
matplotlib.rc('figure'          ,titlesize=24)

fig1,axe1=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
fig2,axe2=plt.subplots(nrows=1,ncols=1,figsize=(18,12))

fig1.suptitle("Imposed mass balance Teterousse")
axe1.set_xlabel(r't(year)') 
axe1.set_ylabel(r'Mass Balance (m of water)')
axe1.ticklabel_format(axis='x', style='sci')
axe1.ticklabel_format(axis='y', style='sci')

fig2.suptitle("Real mass balance Teterousse")
axe1.set_xlabel(r't(year)') 
axe1.set_ylabel(r'Mass Balance (m of water)')
axe1.ticklabel_format(axis='x', style='sci')
axe1.ticklabel_format(axis='y', style='sci')

zpos=len(data_m)+len(data_c)
for tab,lab in zip (data_m,labels_m):
	axe1.plot(tab[:,0],tab[:,1],label=lab)
	axe2.plot(tab[:,0],tab[:,1],label=lab)


for tab,lab in zip (data_s,labels_s):
	zpos+=1
	axe1.scatter(tab[:,0],tab[:,1],label=lab,c='black',zorder=zpos)
	axe2.scatter(tab[:,0],tab[:,1],label=lab,c='black',zorder=zpos)


for tab,lab in zip (data_c,labels_c):
	cumul_MB=np.zeros(tab.shape[0])
	prev=0
	for i in range(tab.shape[0]):
		cumul_MB[i]=prev+(tab[i,6]/tab[i,3]*ice_density)/stepsperyear
		prev+=(tab[i,6]/tab[i,3]*ice_density)/stepsperyear
		
	axe1.plot(tab[:,0],cumul_MB-cumul_MB[indinit_res]+mean_init,label=lab)
	axe2.plot(tab[:,0],(tab[:,1]/tab[:,3]-tab[indinit_res,1]/tab[indinit_res,3])*ice_density+mean_init
              ,label=lab)


axe1.legend(loc="lower left")
axe2.legend(loc="lower left")
fig1.show()
fig2.show()

input()
