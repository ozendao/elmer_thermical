import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib
#########################################
data_dir='temp_vertical'
simu_name=sys.argv[1]
#name='retour_dahu/verticaltemp_secular_'+simu_name+'.dat' 
simu_start=1907
plot_orig =2010
ys=30
yd=365.25
########################################
add_index =(plot_orig-simu_start)*yd
data_dict10=dict([
 ('F. 02',(254 ,947994.5075,2105054.455,'temp10_02a',1 )),
 ('F. 04',(254 ,947927.3195,2105049.740,'temp10_04a',2 )),
 ('F. 05',(254 ,948019.5137,2105027.943,'temp10_05a',3 )),
 ('F. 10',(254 ,948050.6787,2105008.754,'temp10_10a',4 )),
 ('F. 13',(254 ,947962.6118,2105052.789,'temp10_13a',5 )),
 ('F. 17',(254 ,948143.1283,2105018.400,'temp10_17a',6 )),
 ('F. 18',(254 ,947877.9994,2105062.178,'temp10_18a',7 )),
])
data_dict15=dict([
 ('F. 10',(2136,948050.6787,2105008.754,'temp15_10a',4 )),
 ('F. 13',(2136,947962.6118,2105052.789,'temp15_13a',5 )),
])
data_dict16=dict([
 ('F. 70 Jul.',(2398,948080.476 ,2105042.552,'temp16_70a',8 )),
 ('F. 71 Jul.',(2398,948067.5604,2105070.849,'temp16_71a',9 )),
 ('F. 72 Jul.',(2398,948135.8666,2105039.779,'temp16_72a',10)),
 ('F. 70 Sep.',(2436,948080.476 ,2105042.552,'temp16_70b',8 )),
 ('F. 71 Sep.',(2436,948067.5604,2105070.849,'temp16_71b',9 )),
 ('F. 72 Sep.',(2436,948135.8666,2105039.779,'temp16_72b',10)),
])
data_dict17=dict([
 ('F. 70',(2791,948080.476 ,2105042.552,'temp17_70a',8 )),
 ('F. 71',(2791,948067.5604,2105070.849,'temp17_71a',9 )),
 ('F. 72',(2791,948135.8666,2105039.779,'temp17_72a',10)),
])
data_dict18=dict([
 ('F. 70',(3255,948080.476 ,2105042.552,'temp18_70a',8 )),
 ('F. 71',(3255,948067.5604,2105070.849,'temp18_71a',9 )),
])
data_dict19=dict([
 ('F. 70',(3561,948080.476 ,2105042.552,'temp19_70a',8 )),
 ('F. 71',(3561,948067.5604,2105070.849,'temp19_71a',9 )),
 ('F. 72',(3561,948135.8666,2105039.779,'temp19_72a',10)),
])
data_dict23=dict([
 ('F. 2204',(4987,948007.568 ,2105047.296,'temp23_02a',14)),
 ('F. 2202',(4987,947928.286 ,2105050.534,'temp23_04a',12)),
 ('F. 2205',(4987,948019.836 ,2105027.984,'temp23_05a',15)),
 ('F. 2209',(4987,948109.777 ,2105067.609,'temp23_09a',19)),
 ('F. 2206',(4987,948051.002 ,2105008.716,'temp23_10a',16)),
 ('F. 2210',(4987,948143.086 ,2105019.322,'temp23_17a',20)),
 ('F. 2201',(4987,947878.325 ,2105061.382,'temp23_18a',11)),
 ('F. 2208',(4987,948080.949 ,2105042.286,'temp23_70a',18)),
 ('F. 2207',(4987,948051.23  ,2105077.152,'temp23_71a',17))
])

#result=np.asarray([[ float(k) for k in line.split()] for line in open(name,'r')])

colors=['tab:blue','tab:orange','tab:green','tab:red','tab:purple','tab:brown','tab:pink','tab:gray','tab:olive','tab:cyan']

#########################################
matplotlib.rc('axes.formatter'  ,limits=(-2,3))
matplotlib.rc('lines'           ,markersize=6.0)
matplotlib.rc('scatter'         ,marker="s")
matplotlib.rc('xtick'           ,labelsize=24)
matplotlib.rc('ytick'           ,labelsize=24)
matplotlib.rc('axes'            ,titlesize=22)
matplotlib.rc('axes'            ,labelsize=22)
matplotlib.rc('legend'          ,fontsize=20)
matplotlib.rc('legend'          ,title_fontsize=22)
matplotlib.rc('legend'          ,frameon=False)
matplotlib.rc('text'            ,usetex=True)
matplotlib.rc('figure'          ,titlesize=36)
matplotlib.rc('savefig'  ,format="pdf")
matplotlib.rc('savefig'  ,bbox="tight")
##########################################
fig10,axe10=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
fig10.suptitle('Comparaison de température 2010')
axe10.set_xlabel(r'T (C)')
axe10.set_ylabel(r'Profondeur (m)')
axe10.set_xlim([-4.0,0.2])
axe10.set_ylim([-80.0,0.0])
axe10.ticklabel_format(axis='x', style='plain')
axe10.ticklabel_format(axis='y', style='plain')	
####
fig15,axe15=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
fig15.suptitle('Comparaison de température 2015')
axe15.set_xlabel(r'T (C)')
axe15.set_ylabel(r'Profondeur (m)')
axe15.set_xlim([-4.0,0.2])
axe15.set_ylim([-80.0,0.0])
axe15.ticklabel_format(axis='x', style='plain')
axe15.ticklabel_format(axis='y', style='plain')	
####
fig16,axe16=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
fig16.suptitle('Comparaison de température 2016')
axe16.set_xlabel(r'T (C)')
axe16.set_ylabel(r'Profondeur (m)')
axe16.set_xlim([-4.0,0.2])
axe16.set_ylim([-80.0,0.0])
axe16.ticklabel_format(axis='x', style='plain')
axe16.ticklabel_format(axis='y', style='plain')	
####
fig17,axe17=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
fig17.suptitle('Comparaison de température 2017')
axe17.set_xlabel(r'T (C)')
axe17.set_ylabel(r'Profondeur (m)')
axe17.set_xlim([-4.0,0.2])
axe17.set_ylim([-80.0,0.0])
axe17.ticklabel_format(axis='x', style='plain')
axe17.ticklabel_format(axis='y', style='plain')	
####
fig18,axe18=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
fig18.suptitle('Comparaison de température 2018')
axe18.set_xlabel(r'T (C)')
axe18.set_ylabel(r'Profondeur (m)')
axe18.set_xlim([-4.0,0.2])
axe18.set_ylim([-80.0,0.0])
axe18.ticklabel_format(axis='x', style='plain')
axe18.ticklabel_format(axis='y', style='plain')	
####
fig19,axe19=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
fig19.suptitle('Comparaison de température 2019')
axe19.set_xlabel(r'T (C)')
axe19.set_ylabel(r'Profondeur (m)')
axe19.set_xlim([-4.0,0.2])
axe19.set_ylim([-80.0,0.0])
axe19.ticklabel_format(axis='x', style='plain')
axe19.ticklabel_format(axis='y', style='plain')	
####
fig23,axe23=plt.subplots(nrows=1,ncols=1,figsize=(18,12))
fig23.suptitle('Comparaison de température 2023')
axe23.set_xlabel(r'T (C)')
axe23.set_ylabel(r'Profondeur (m)')
axe23.set_xlim([-4.0,0.2])
axe23.set_ylim([-80.0,0.0])
axe23.ticklabel_format(axis='x', style='plain')
axe23.ticklabel_format(axis='y', style='plain')	
#######################################
for key,point in data_dict10.items():
	time=round((add_index+point[0])/ys)
	file_t=np.loadtxt(data_dir+'/'+point[3],delimiter=';',comments='#')
#	res_mask=(result[:,0]==time)*(result[:,4]==point[1])*(result[:,5]==point[2])*(result[:,2]==point[4])
#	ice_mask=result[:,8]<=result[:,6]
#	axe10.plot(result[res_mask*ice_mask,7],result[res_mask*ice_mask,6]-np.max(result[res_mask*ice_mask,6]),label=key+" Simulation",color=colors[point[4]-1])
	axe10.plot(file_t[:,1],file_t[:,0],label=key+" Mesures",linestyle='dashed',color=colors[point[4]-1])
####
for key,point in data_dict15.items():
	time=round((add_index+point[0])/ys)
	file_t=np.loadtxt(data_dir+'/'+point[3],delimiter=';',comments='#')
#	res_mask=(result[:,0]==time)*(result[:,4]==point[1])*(result[:,5]==point[2])*(result[:,2]==point[4])
#	ice_mask=result[:,8]<=result[:,6]
#	axe15.plot(result[res_mask*ice_mask,7],result[res_mask*ice_mask,6]-np.max(result[res_mask*ice_mask,6]),label=key+" Simulation",color=colors[point[4]-4])
	axe15.plot(file_t[:,1],file_t[:,0],label=key+" Mesures",linestyle='dashed',color=colors[point[4]-4])
####
k16=0
for key,point in data_dict16.items():
	time=round((add_index+point[0])/ys)
	file_t=np.loadtxt(data_dir+'/'+point[3],delimiter=';',comments='#')
#	res_mask=(result[:,0]==time)*(result[:,4]==point[1])*(result[:,5]==point[2])*(result[:,2]==point[4])
#	ice_mask=result[:,8]<=result[:,6]
#	axe16.plot(result[res_mask*ice_mask,7],result[res_mask*ice_mask,6]-np.max(result[res_mask*ice_mask,6]),label=key+" Simulation",color=colors[k16])
	axe16.plot(file_t[:,1],file_t[:,0],label=key+" Mesures",linestyle='dashed',color=colors[k16])
	k16+=1
####
for key,point in data_dict17.items():
	time=round((add_index+point[0])/ys)
	file_t=np.loadtxt(data_dir+'/'+point[3],delimiter=';',comments='#')
#	res_mask=(result[:,0]==time)*(result[:,4]==point[1])*(result[:,5]==point[2])*(result[:,2]==point[4])
#	ice_mask=result[:,8]<=result[:,6]
#	axe17.plot(result[res_mask*ice_mask,7],result[res_mask*ice_mask,6]-np.max(result[res_mask*ice_mask,6]),label=key+" Simulation",color=colors[point[4]-8])
	axe17.plot(file_t[:,1],file_t[:,0],label=key+" Mesures",linestyle='dashed',color=colors[point[4]-8])
####
for key,point in data_dict18.items():
	time=round((add_index+point[0])/ys)
	file_t=np.loadtxt(data_dir+'/'+point[3],delimiter=';',comments='#')
#	res_mask=(result[:,0]==time)*(result[:,4]==point[1])*(result[:,5]==point[2])*(result[:,2]==point[4])
#	ice_mask=result[:,8]<=result[:,6]
#	axe18.plot(result[res_mask*ice_mask,7],result[res_mask*ice_mask,6]-np.max(result[res_mask*ice_mask,6]),label=key+" Simulation",color=colors[point[4]-8])
	axe18.plot(file_t[:,1],file_t[:,0],label=key+" Mesures",linestyle='dashed',color=colors[point[4]-8])
####
for key,point in data_dict19.items():
	time=round((add_index+point[0])/ys)
	file_t=np.loadtxt(data_dir+'/'+point[3],delimiter=';',comments='#')
#	res_mask=(result[:,0]==time)*(result[:,4]==point[1])*(result[:,5]==point[2])*(result[:,2]==point[4])
#	ice_mask=result[:,8]<=result[:,6]
#	axe19.plot(result[res_mask*ice_mask,7],result[res_mask*ice_mask,6]-np.max(result[res_mask*ice_mask,6]),label=key+" Simulation",color=colors[point[4]-8])
	axe19.plot(file_t[:,1],file_t[:,0],label=key+" Mesures",linestyle='dashed',color=colors[point[4]-8])
####
for key,point in data_dict23.items():
	time=round((add_index+point[0])/ys)
	file_t=np.loadtxt(data_dir+'/'+point[3],delimiter=';',comments='#')
#	res_mask=(result[:,0]==time)*(result[:,4]==point[1])*(result[:,5]==point[2])*(result[:,2]==point[4])
#	ice_mask=((result[:,6]-result[:,8])>=0)
#	axe23.plot(result[res_mask*ice_mask,7],result[res_mask*ice_mask,6]-np.max(result[res_mask*ice_mask,6]),label=key+" Simulation",color=colors[point[4]-14])
	axe23.plot(file_t[:,1],file_t[:,0],label=key+" Mesures",linestyle='dashed',color=colors[point[4]-14])
##############
fig10.show()
axe10.legend()
fig10.savefig('Drill_2010'+simu_name+'.pdf')
####
fig15.show()
axe15.legend()
fig15.savefig('Drill_2015'+simu_name+'.pdf')
####
fig16.show()
axe16.legend()
fig16.savefig('Drill_2016'+simu_name+'.pdf')
####
fig17.show()
axe17.legend()
fig17.savefig('Drill_2017'+simu_name+'.pdf')
####
fig18.show()
axe18.legend()
fig18.savefig('Drill_2018'+simu_name+'.pdf')
####
fig19.show()
axe19.legend()
fig19.savefig('Drill_2019'+simu_name+'.pdf')
####
fig23.show()
axe23.legend()
fig23.savefig('Drill_2023'+simu_name+'.pdf')
#######################################
input()	
