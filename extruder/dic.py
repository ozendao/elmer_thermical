#origine des dates 01-01-2010 corrdonnées lambert II Ext
#dictionnaire : key = annee_indexbalise value=(jour,posX,poY,file,index_sif)
#ne fonctionne pas si on a plusieurs itérations de point fixe
import numpy as np
import math
import sys, os
import matplotlib.pyplot as plt
import matplotlib
#########################################
data_dir='temp_vertical'
<<<<<<< HEAD
result_files=[]
result_files.append('/media/ozenda/Expansion/TRbedlarge_rockice_15_11/temp_secular_0711.dat')
result_files.append('TRbedlarge_rockice/temp_secular_11c.dat')
result_files.append('TRbedlarge_rockice/temp_secular_1811.dat')
result_files.append('TRbedlarge_rockice/temp_secular_2011.dat')
=======
result='TRbedlarge_rockice/temp_secular_0711.dat'
>>>>>>> dc2fd4369ee4815fe8f23a3be7423009636b100a
simu_start=1907
plot_orig =2010
ys=30
yd=365.25
########################################
add_index =(plot_orig-simu_start)*yd
<<<<<<< HEAD
data_dict=dict([
 ('10_02a',(254 ,947994.5075,2105054.455,'temp10_02a',1 )),
 ('10_04a',(254 ,947927.3195,2105049.740,'temp10_04a',2 )),
 ('10_05a',(254 ,948019.5137,2105027.943,'temp10_05a',3 )),
 ('10_10a',(254 ,948050.6787,2105008.754,'temp10_10a',4 )),
 ('10_13a',(254 ,947962.6118,2105052.789,'temp10_13a',5 )),
 ('10_17a',(254 ,948143.1283,2105018.400,'temp10_17a',6 )),
 ('10_18a',(254 ,947877.9994,2105062.178,'temp10_18a',7 )),
 ('15_10a',(2136,948050.6787,2105008.754,'temp15_10a',4 )),
 ('15_13a',(2136,947962.6118,2105052.789,'temp15_13a',5 )),
 ('16_70a',(2398,948080.476 ,2105042.552,'temp16_70a',8 )),
 ('16_71a',(2398,948067.5604,2105070.849,'temp16_71a',9 )),
 ('16_72a',(2398,948135.8666,2105039.779,'temp16_72a',10)),
 ('16_70b',(2436,948080.476 ,2105042.552,'temp16_70b',8 )),
 ('16_71b',(2436,948067.5604,2105070.849,'temp16_71b',9 )),
 ('16_72b',(2436,948135.8666,2105039.779,'temp16_72b',10)),
 ('17_70a',(2791,948080.476 ,2105042.552,'temp17_70a',8 )),
 ('17_71a',(2791,948067.5604,2105070.849,'temp17_71a',9 )),
 ('17_72a',(2791,948135.8666,2105039.779,'temp17_72a',10)),
 ('18_70a',(3255,948080.476 ,2105042.552,'temp18_70a',8 )),
 ('18_71a',(3255,948067.5604,2105070.849,'temp18_71a',9 ))
# '19_70a':(3561,948080.476 ,2105042.552,'temp19_70a',8 ),
# '19_71a':(3561,948067.5604,2105070.849,'temp19_71a',9 ),
# '19_72a':(3561,948135.8666,2105039.779,'temp19_72a',10),
# '23_02a':(4987,948007.568 ,2105047.296,'temp23_02a',14),
# '23_04a':(4987,947928.286 ,2105050.534,'temp23_04a',12),
# '23_05a':(4987,948019.836 ,2105027.984,'temp23_05a',15),
# '23_09a':(4987,948109.777 ,2105067.609,'temp23_09a',19),
# '23_10a':(4987,948051.002 ,2105008.716,'temp23_10a',16),
# '23_17a':(4987,948143.086 ,2105019.322,'temp23_17a',20),
# '23_18a':(4987,947878.325 ,2105061.382,'temp23_18a',11),
# '23_70a':(4987,948080.949 ,2105042.286,'temp23_70a',18),
# '23_71a':(4987,948051.23  ,2105077.152,'temp23_71a',17)
])
result=[]
for name in result_files:
	result.append(np.asarray([[ float(k) for k in line.split()] for line in open(name,'r')]))

labels=[]
labels.append('Simu A')
labels.append('Simu B')
labels.append('Simu C')
labels.append('Simu D')
#########################################
matplotlib.rc('axes.formatter'  ,limits=(-2,3))
matplotlib.rc('lines'           ,markersize=6.0)
matplotlib.rc('scatter'         ,marker="s")
matplotlib.rc('xtick'           ,labelsize=18)
matplotlib.rc('ytick'           ,labelsize=18)
matplotlib.rc('axes'            ,titlesize=20)
matplotlib.rc('axes'            ,labelsize=20)
matplotlib.rc('legend'          ,fontsize=16)
matplotlib.rc('legend'          ,title_fontsize=18)
matplotlib.rc('legend'          ,frameon=False)
matplotlib.rc('text'            ,usetex=True)
matplotlib.rc('figure'          ,titlesize=24)
matplotlib.rc('savefig'  ,format="pdf")
matplotlib.rc('savefig'  ,bbox="tight")
##########################################
plot_list=[]
file_list=[]
for key,point in data_dict.items():
	file_list.append(np.loadtxt(data_dir+'/'+point[3],delimiter=';',comments='#'))
=======
data_dict={
 '10_02a':(254 ,947994.5075,2105054.455,'temp10_02a',1 ),
 '10_04a':(254 ,947927.3195,2105049.740,'temp10_04a',2 ),
 '10_05a':(254 ,948019.5137,2105027.943,'temp10_05a',3 ),
 '10_10a':(254 ,948050.6787,2105008.754,'temp10_10a',4 ),
 '10_13a':(254 ,947962.6118,2105052.789,'temp10_13a',5 ),
 '10_17a':(254 ,948143.1283,2105018.400,'temp10_17a',6 ),
 '10_18a':(254 ,947877.9994,2105062.178,'temp10_18a',7 ),
 '15_10a':(2136,948050.6787,2105008.754,'temp15_10a',4 ),
 '15_13a':(2136,947962.6118,2105052.789,'temp15_13a',5 ),
 '16_70a':(2398,948080.476 ,2105042.552,'temp16_70a',8 ),
 '16_71a':(2398,948067.5604,2105070.849,'temp16_71a',9 ),
 '16_72a':(2398,948135.8666,2105039.779,'temp16_72a',10),
 '16_70b':(2436,948080.476 ,2105042.552,'temp16_70b',8 ),
 '16_71b':(2436,948067.5604,2105070.849,'temp16_71b',9 ),
 '16_72b':(2436,948135.8666,2105039.779,'temp16_72b',10),
 '17_70a':(2791,948080.476 ,2105042.552,'temp17_70a',8 ),
 '17_71a':(2791,948067.5604,2105070.849,'temp17_71a',9 ),
 '17_72a':(2791,948135.8666,2105039.779,'temp17_72a',10),
 '18_70a':(3255,948080.476 ,2105042.552,'temp18_70a',8 ),
 '18_71a':(3255,948067.5604,2105070.849,'temp18_71a',9 ),
 '19_70a':(3561,948080.476 ,2105042.552,'temp19_70a',8 ),
 '19_71a':(3561,948067.5604,2105070.849,'temp19_71a',9 ),
 '19_72a':(3561,948135.8666,2105039.779,'temp19_72a',10),
 '23_02a':(4987,948007.568 ,2105047.296,'temp23_02a',14),
 '23_04a':(4987,947928.286 ,2105050.534,'temp23_04a',12),
 '23_05a':(4987,948019.836 ,2105027.984,'temp23_05a',15),
 '23_09a':(4987,948109.777 ,2105067.609,'temp23_09a',19),
 '23_10a':(4987,948051.002 ,2105008.716,'temp23_10a',16),
 '23_17a':(4987,948143.086 ,2105019.322,'temp23_17a',20),
 '23_18a':(4987,947878.325 ,2105061.382,'temp23_18a',11),
 '23_70a':(4987,948080.949 ,2105042.286,'temp23_70a',18),
 '23_71a':(4987,948051.23  ,2105077.152,'temp23_71a',17)
}
result_array=np.asarrray([[ float(k) for k in line.split()] for line in open(result,'r')])
#########################################
plot_list=[]
file_list=[]
for key,point in data_dict:
	file_list.append(np.loadtxt(data_dir+'/'+point[3],delimiter=','))
>>>>>>> dc2fd4369ee4815fe8f23a3be7423009636b100a
	time=round((add_index+point[0])/ys)
	plot_list.append(plt.subplots(nrows=1,ncols=1,figsize=(18,12)))
	plot_list[-1][0].suptitle('Temperature comparison '+key)
	plot_list[-1][1].set_xlabel(r'T (C)')
	plot_list[-1][1].set_ylabel(r'Depth (m)')
<<<<<<< HEAD
	plot_list[-1][1].set_xlim([-3.0,0.0])
	plot_list[-1][1].set_ylim([0.0,90.0])
	plot_list[-1][1].ticklabel_format(axis='x', style='sci')
	plot_list[-1][1].ticklabel_format(axis='y', style='sci')
	for res,lab in zip(result,labels): 
		res_mask=(res[:,0]==time)*(res[:,4]==point[1])*(res[:,5]==point[2])*(res[:,2]==point[4])
		plot_list[-1][1].plot(res[res_mask,8],res[res_mask,7],label=lab)
	plot_list[-1][1].plot(file_list[-1][:,1],-file_list[-1][:,0],label='measurements')
	plot_list[-1][0].show()
	plot_list[-1][1].legend()
	plot_list[-1][0].savefig('fig_'+key+'.pdf')
=======
	plot_list[-1][1].ticklabel_format(axis='x', style='sci')
	plot_list[-1][1].ticklabel_format(axis='y', style='sci')
    result_mask=(result_array[:,0]==time)*(result_array[:,4]==point[1])
               *(result_array[:,5]==point[2])*(result_array[:,2]==point[4])
	plot_list[-1][1].plot(result_array[result_mask,8],result_array[result_mask,7])
	plot_list[-1][0].show()
>>>>>>> dc2fd4369ee4815fe8f23a3be7423009636b100a
#######################################
input()	
