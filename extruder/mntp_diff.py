import numpy as np
import os 
import sys

tempmanu = np.loadtxt("Dataclimat/temp_TR_2013_2022_manu.dat")
templyon = np.loadtxt("Dataclimat/TempLyonMoy2013-2022.dat")
compmaxi= np.loadtxt("Dataclimat/maxi_2013_2022.dat",comments="#")

manu=np.mean(tempmanu,where= np.logical_not(np.isnan(tempmanu)))
lyon=np.mean(templyon)
max_mean=np.mean(compmaxi[:,1]-compmaxi[:,0])
print(manu,lyon,lyon-manu,max_mean)

