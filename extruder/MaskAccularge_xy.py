import numpy as np
import os 
import sys

def correction(x,y):
	x0=947901
	y0=2105090
	a=0.259665
	b=2351223
	amx=0.001303
	bmx=1.445181
	amy=0.005211
	bmy=1.335391
	fmx=amx*((x-x0+a*(y-y0))/(1+a**2))+bmx
	fmy=amy*((a*(x-x0)+a*a*(y-y0))/(1+a**2))+bmy	
	return (fmy+fmx)*0.5


def correction_alt(x,y,z):
	x0=947901
	y0=2105090
	a=0.259665
	b=2351223
	amx=0.001303
	bmx=1.445181
	amy=0.005211
	bmy=1.335391
	fmx=amx*((x-x0+a*(y-y0))/(1+a**2))+bmx
	fmy=amy*((a*(x-x0)+a*a*(y-y0))/(1+a**2))+bmy	
	if   (0<=z):
		a=(fmy+fmx)*0.5
	else :
		a=np.nan
	return a


surface=np.loadtxt('DataTeterousse/DEMSurf2019.dat')

nline=surface.shape[0]
accu = open("DataTeterousse/Accularge_xy.dat",'w')
accu_alt = open("DataTeterousse/Accularge_alt_xy.dat",'w')
for i in range(nline):
	accu.write("{0:f} {1:f} {2:f} \n".format(surface[i,0],surface[i,1],correction(surface[i,0],surface[i,1])))
	accu_alt.write("{0:f} {1:f} {2:f} {3:f}\n".format(surface[i,0],surface[i,1],surface[i,2],correction_alt(surface[i,0],surface[i,1],surface[i,2])))
accu.close()
