set ylabel "MB cumul (m eq W)"
set xlabel "year"

plot \
  "bilanDGhomo_1801_1998.txt" w l lw 2 lc 1,\
  "bilanDGlyonbesse1908_2007.txt" w l lw 2 lc 2,\
  "bilanDGargentiere_1975_2007.txt" w l lw 2 lc 3,\
  "bilanCT1901_1950_2007.txt" dt 1 lw 6 lc 0


pause -1
