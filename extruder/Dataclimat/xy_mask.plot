f(x)=a*x+b
a=-0.1
b=200000
fit f(x) "TR_WB_manu.txt" u 2:3 via a,b
x0=947901
y0=2105090
z0=3139
px0=(x0+a*y0-a*b)/(1+a**2)
py0=(a*x0+a*a*y0+b)/(1+a**2)

f1x(x,y)=a1x*((x+a*y-a*b)/(1+a**2)-px0)+b1x
f1y(x,y)=a1y*((a*x+a*a*y+b)/(1+a**2)-py0)+b1y
f2x(x,y)=a2x*((x+a*y-a*b)/(1+a**2)-px0)+b2x
f2y(x,y)=a2y*((a*x+a*a*y+b)/(1+a**2)-py0)+b2y
f3x(x,y)=a3x*((x+a*y-a*b)/(1+a**2)-px0)+b3x
f3y(x,y)=a3y*((a*x+a*a*y+b)/(1+a**2)-py0)+b3y
f4x(x,y)=a4x*((x+a*y-a*b)/(1+a**2)-px0)+b4x
f4y(x,y)=a4y*((a*x+a*a*y+b)/(1+a**2)-py0)+b4y
f5x(x,y)=a5x*((x+a*y-a*b)/(1+a**2)-px0)+b5x
f5y(x,y)=a5y*((a*x+a*a*y+b)/(1+a**2)-py0)+b5y
f6x(x,y)=a6x*((x+a*y-a*b)/(1+a**2)-px0)+b6x
f6y(x,y)=a6y*((a*x+a*a*y+b)/(1+a**2)-py0)+b6y
f7x(x,y)=a7x*((x+a*y-a*b)/(1+a**2)-px0)+b7x
f7y(x,y)=a7y*((a*x+a*a*y+b)/(1+a**2)-py0)+b7y
f8x(x,y)=a8x*((x+a*y-a*b)/(1+a**2)-px0)+b8x
f8y(x,y)=a8y*((a*x+a*a*y+b)/(1+a**2)-py0)+b8y
f9x(x,y)=a9x*((x+a*y-a*b)/(1+a**2)-px0)+b9x
f9y(x,y)=a9y*((x+a*y-a*b)/(1+a**2)-py0)+b9y
f10x(x,y)=a10x*((a*x+a*a*y+b)/(1+a**2)-px0)+b10x
f10y(x,y)=a10y*((a*x+a*a*y+b)/(1+a**2)-py0)+b10y
f1z(x)=a1z*(x-z0)+b1z
f2z(x)=a2z*(x-z0)+b2z
f3z(x)=a3z*(x-z0)+b3z
f4z(x)=a4z*(x-z0)+b4z
f5z(x)=a5z*(x-z0)+b5z
f6z(x)=a6z*(x-z0)+b6z
f7z(x)=a7z*(x-z0)+b7z
f8z(x)=a8z*(x-z0)+b8z
f9z(x)=a9z*(x-z0)+b9z
f10z(x)=a10z*(x-z0)+b10z

a1x=0.0001
a1y=0.0001
a1z=0.001
b1x=1.0
b1y=1.0
b1z=1.0
a2x=0.0001
a2y=0.0001
a2z=0.001
b2x=1.0
b2y=1.0
b2z=1.0
a3x=0.0001
a3y=0.0001
a3z=0.001
b3x=1.0
b3y=1.0
b3z=1.0
a4x=0.0001
a4y=0.0001
a4z=0.001
b4x=1.0
b4y=1.0
b4z=1.0
a5x=0.001
a5y=0.001
b5x=1.0
b5y=1.0
b5z=1.0
a6x=0.0001
a6y=0.0001
a6z=0.001
b6x=1.0
b6y=1.0
b6z=1.0
a7x=0.0001
a7y=0.0001
a7z=0.001
b7x=1.0
b7y=1.0
b7z=1.0
a8x=0.0001
a8y=0.0001
a8z=0.001
b8x=1.0
b8y=1.0
b8z=1.0
a9x=0.0001
a9y=0.0001
a9z=0.001
b9x=1.0
b9y=1.0
b9z=1.0
a10x=0.0001
a10y=0.0001
a10z=0.001
b10x=1.0
b10y=1.0
b10z=1.0

fit f1x(x,y)  "TR_xyz_accu.dat" i 2  u 1:2:4 via a1x,b1x
fit f2x(x,y)  "TR_xyz_accu.dat" i 3  u 1:2:4 via a2x,b2x
fit f3x(x,y)  "TR_xyz_accu.dat" i 4  u 1:2:4 via a3x,b3x
fit f4x(x,y)  "TR_xyz_accu.dat" i 5  u 1:2:4 via a4x,b4x
fit f5x(x,y)  "TR_xyz_accu.dat" i 6  u 1:2:4 via a5x,b5x
fit f6x(x,y)  "TR_xyz_accu.dat" i 7  u 1:2:4 via a6x,b6x
fit f7x(x,y)  "TR_xyz_accu.dat" i 8  u 1:2:4 via a7x,b7x
fit f8x(x,y)  "TR_xyz_accu.dat" i 9  u 1:2:4 via a8x,b8x
fit f9x(x,y)  "TR_xyz_accu.dat" i 10 u 1:2:4 via a9x,b9x
fit f10x(x,y) "TR_xyz_accu.dat" i 11 u 1:2:4 via a10x,b10x
fit f1y(x,y)  "TR_xyz_accu.dat" i 2  u 1:2:4 via a1y,b1y
fit f2y(x,y)  "TR_xyz_accu.dat" i 3  u 1:2:4 via a2y,b2y
fit f3y(x,y)  "TR_xyz_accu.dat" i 4  u 1:2:4 via a3y,b3y
fit f4y(x,y)  "TR_xyz_accu.dat" i 5  u 1:2:4 via a4y,b4y
fit f5y(x,y)  "TR_xyz_accu.dat" i 6  u 1:2:4 via a5y,b5y
fit f6y(x,y)  "TR_xyz_accu.dat" i 7  u 1:2:4 via a6y,b6y
fit f7y(x,y)  "TR_xyz_accu.dat" i 8  u 1:2:4 via a7y,b7y
fit f8y(x,y)  "TR_xyz_accu.dat" i 9  u 1:2:4 via a8y,b8y
fit f9y(x,y)  "TR_xyz_accu.dat" i 10 u 1:2:4 via a9y,b9y
fit f10y(x,y) "TR_xyz_accu.dat" i 11 u 1:2:4 via a10y,b10y
fit f1z(x)  "TR_xyz_accu.dat" i 2  u 3:4 via a1z,b1z
fit f2z(x)  "TR_xyz_accu.dat" i 3  u 3:4 via a2z,b2z
fit f3z(x)  "TR_xyz_accu.dat" i 4  u 3:4 via a3z,b3z
fit f4z(x)  "TR_xyz_accu.dat" i 5  u 3:4 via a4z,b4z
fit f5z(x)  "TR_xyz_accu.dat" i 6  u 3:4 via a5z,b5z
fit f6z(x)  "TR_xyz_accu.dat" i 7  u 3:4 via a6z,b6z
fit f7z(x)  "TR_xyz_accu.dat" i 8  u 3:4 via a7z,b7z
fit f8z(x)  "TR_xyz_accu.dat" i 9  u 3:4 via a8z,b8z
fit f9z(x)  "TR_xyz_accu.dat" i 10 u 3:4 via a9z,b9z
fit f10z(x) "TR_xyz_accu.dat" i 11 u 3:4 via a10z,b10z


amx=(a1x+a2x+a3x+a4x+a5x+a6x+a7x+a8x+a9x+a10x)/10.0
bmx=(b1x+b2x+b3x+b4x+b5x+b6x+b7x+b8x+b9x+b10x)/10.0
fmx(x,y)=amx*((x+a*y-a*b)/(1+a**2)-px0)+bmx
amy=(a1y+a2y+a3y+a4y+a5y+a6y+a7y+a8y+a9y+a10y)/10.0
bmy=(b1y+b2y+b3y+b4y+b5y+b6y+b7y+b8y+b9y+b10y)/10.0
fmy(x,y)=amy*((a*x+a*a*y+b)/(1+a**2)-py0)+bmy
amz=(a1z+a2z+a3z+a4z+a5z+a6z+a7z+a8z+a9z+a10z)/10.0
bmz=(b1z+b2z+b3z+b4z+b5z+b6z+b7z+b8z+b9z+b10z)/10.0
fmz(x)=amz*(x-z0)+bmz


set title "Winter mass accumulation fit x TR"
set xlabel "x pos (lamb2)"
set ylabel "y pos (lamb2)"
set zlabel "meters of water"
splot \
f1x(x,y)  w l lw 3 lc rgb "#c000ff" t sprintf("2013 a=%4f b=%4f",a1x,b1x),   "TR_xyz_accu.dat" i 2  u 1:2:4 w lp lc rgb "#c000ff" t "2013", \
f2x(x,y)  w l lw 3 lc rgb "#c04000" t sprintf("2014 a=%4f b=%4f",a2x,b2x),   "TR_xyz_accu.dat" i 3  u 1:2:4 w lp lc rgb "#c04000" t "2014", \
f3x(x,y)  w l lw 3 lc rgb "#008040" t sprintf("2015 a=%4f b=%4f",a3x,b3x),   "TR_xyz_accu.dat" i 4  u 1:2:4 w lp lc rgb "#008040" t "2015", \
f4x(x,y)  w l lw 3 lc rgb "#8b0000" t sprintf("2016 a=%4f b=%4f",a4x,b4x),   "TR_xyz_accu.dat" i 5  u 1:2:4 w lp lc rgb "#8b0000" t "2016", \
f5x(x,y)  w l lw 3 lc rgb "#ffc020" t sprintf("2017 a=%4f b=%4f",a5x,b5x),   "TR_xyz_accu.dat" i 6  u 1:2:4 w lp lc rgb "#ffc020" t "2017", \
f6x(x,y)  w l lw 3 lc rgb "#f03232" t sprintf("2018 a=%4f b=%4f",a6x,b6x),   "TR_xyz_accu.dat" i 7  u 1:2:4 w lp lc rgb "#f03232" t "2018", \
f7x(x,y)  w l lw 3 lc rgb "#000080" t sprintf("2019 a=%4f b=%4f",a7x,b7x),   "TR_xyz_accu.dat" i 8  u 1:2:4 w lp lc rgb "#000080" t "2019", \
f8x(x,y)  w l lw 3 lc rgb "#a08020" t sprintf("2020 a=%4f b=%4f",a8x,b8x),   "TR_xyz_accu.dat" i 9  u 1:2:4 w lp lc rgb "#a08020" t "2020", \
f9x(x,y)  w l lw 3 lc rgb "#0080ff" t sprintf("2021 a=%4f b=%4f",a9x,b9x),   "TR_xyz_accu.dat" i 10 u 1:2:4 w lp lc rgb "#0080ff" t "2021", \
f10x(x,y) w l lw 3 lc rgb "#1a1a1a" t sprintf("2022 a=%4f b=%4f",a10x,b10x), "TR_xyz_accu.dat" i 11 u 1:2:4 w lp lc rgb "#1a1a1a" t "2022", \
fmx(x,y) w l lw 3 dt "-" lc rgb "#905040" t sprintf("mean linear model %4f*(xp-%4f)+%4f",amx,px0,bmx)
unset xlabel
unset ylabel
unset zlabel
unset title

pause -1

set title "Winter mass accumulation fit y TR"
set xlabel "x pos (lamb2)"
set ylabel "y pos (lamb2)"
set zlabel "meters of water"
splot \
f1y(x,y)  w l lw 3 lc rgb "#c000ff" t sprintf("2013 a=%4f b=%4f",a1y,b1y),   "TR_xyz_accu.dat" i 2  u 1:2:4 w lp lc rgb "#c000ff" t "2013", \
f2y(x,y)  w l lw 3 lc rgb "#c04000" t sprintf("2014 a=%4f b=%4f",a2y,b2y),   "TR_xyz_accu.dat" i 3  u 1:2:4 w lp lc rgb "#c04000" t "2014", \
f3y(x,y)  w l lw 3 lc rgb "#008040" t sprintf("2015 a=%4f b=%4f",a3y,b3y),   "TR_xyz_accu.dat" i 4  u 1:2:4 w lp lc rgb "#008040" t "2015", \
f4y(x,y)  w l lw 3 lc rgb "#8b0000" t sprintf("2016 a=%4f b=%4f",a4y,b4y),   "TR_xyz_accu.dat" i 5  u 1:2:4 w lp lc rgb "#8b0000" t "2016", \
f5y(x,y)  w l lw 3 lc rgb "#ffc020" t sprintf("2017 a=%4f b=%4f",a5y,b5y),   "TR_xyz_accu.dat" i 6  u 1:2:4 w lp lc rgb "#ffc020" t "2017", \
f6y(x,y)  w l lw 3 lc rgb "#f03232" t sprintf("2018 a=%4f b=%4f",a6y,b6y),   "TR_xyz_accu.dat" i 7  u 1:2:4 w lp lc rgb "#f03232" t "2018", \
f7y(x,y)  w l lw 3 lc rgb "#000080" t sprintf("2019 a=%4f b=%4f",a7y,b7y),   "TR_xyz_accu.dat" i 8  u 1:2:4 w lp lc rgb "#000080" t "2019", \
f8y(x,y)  w l lw 3 lc rgb "#a08020" t sprintf("2020 a=%4f b=%4f",a8y,b8y),   "TR_xyz_accu.dat" i 9  u 1:2:4 w lp lc rgb "#a08020" t "2020", \
f9y(x,y)  w l lw 3 lc rgb "#0080ff" t sprintf("2021 a=%4f b=%4f",a9y,b9y),   "TR_xyz_accu.dat" i 10 u 1:2:4 w lp lc rgb "#0080ff" t "2021", \
f10y(x,y) w l lw 3 lc rgb "#1a1a1a" t sprintf("2022 a=%4f b=%4f",a10y,b10y), "TR_xyz_accu.dat" i 11 u 1:2:4 w lp lc rgb "#1a1a1a" t "2022", \
fmy(x,y) w l lw 3 dt "-" lc rgb "#905040" t sprintf("mean linear model %4f*(yp-%4f)+%4f",amy,py0,bmy)
unset xlabel
unset ylabel
unset zlabel
unset title

pause -1

set title "Winter mass accumulation fit z TR"
set xlabel "altitude z (m)"
set ylabel "meters of water"
plot \
f1z(x)  w l lw 3 lc rgb "#c000ff" t sprintf("2013 a=%4f b=%4f",a1z,b1z),   "TR_xyz_accu.dat" i 2  u 3:4 w lp lc rgb "#c000ff" t "2013", \
f2z(x)  w l lw 3 lc rgb "#c04000" t sprintf("2014 a=%4f b=%4f",a2z,b2z),   "TR_xyz_accu.dat" i 3  u 3:4 w lp lc rgb "#c04000" t "2014", \
f3z(x)  w l lw 3 lc rgb "#008040" t sprintf("2015 a=%4f b=%4f",a3z,b3z),   "TR_xyz_accu.dat" i 4  u 3:4 w lp lc rgb "#008040" t "2015", \
f4z(x)  w l lw 3 lc rgb "#8b0000" t sprintf("2016 a=%4f b=%4f",a4z,b4z),   "TR_xyz_accu.dat" i 5  u 3:4 w lp lc rgb "#8b0000" t "2016", \
f5z(x)  w l lw 3 lc rgb "#ffc020" t sprintf("2017 a=%4f b=%4f",a5z,b5z),   "TR_xyz_accu.dat" i 6  u 3:4 w lp lc rgb "#ffc020" t "2017", \
f6z(x)  w l lw 3 lc rgb "#f03232" t sprintf("2018 a=%4f b=%4f",a6z,b6z),   "TR_xyz_accu.dat" i 7  u 3:4 w lp lc rgb "#f03232" t "2018", \
f7z(x)  w l lw 3 lc rgb "#000080" t sprintf("2019 a=%4f b=%4f",a7z,b7z),   "TR_xyz_accu.dat" i 8  u 3:4 w lp lc rgb "#000080" t "2019", \
f8z(x)  w l lw 3 lc rgb "#a08020" t sprintf("2020 a=%4f b=%4f",a8z,b8z),   "TR_xyz_accu.dat" i 9  u 3:4 w lp lc rgb "#a08020" t "2020", \
f9z(x)  w l lw 3 lc rgb "#0080ff" t sprintf("2021 a=%4f b=%4f",a9z,b9z),   "TR_xyz_accu.dat" i 10 u 3:4 w lp lc rgb "#0080ff" t "2021", \
f10z(x) w l lw 3 lc rgb "#1a1a1a" t sprintf("2022 a=%4f b=%4f",a10z,b10z), "TR_xyz_accu.dat" i 11 u 3:4 w lp lc rgb "#1a1a1a" t "2022", \
fmz(x) w l lw 3 dt "-" lc rgb "#905040" t sprintf("mean linear model %4f*(z-%4f)+%4f",amz,z0,bmz)
unset xlabel
unset ylabel
unset zlabel
unset title

pause -1

set title "position des balises TR"
set xlabel "x pos (lamb2)"
set ylabel "y pos (lamb2)"
plot \
"TR_WB_manu.txt" u 2:3  w p dt 1 lw 2 lc rgb "#c000ff" t "position balises",\
f(x) w l lw 3 dt "-" lc rgb "#905040" t sprintf("gradient direction : y=%4f x+%4f",a,b)
unset xlabel
unset ylabel
unset title



pause -1

set title "comparaison fitx et fit y"
set xlabel "x pos (lamb2)"
set ylabel "y pos (lamb2)"
set zlabel "meters of water"
splot \
 "TR_xyz_accu.dat" i 2  u 1:2:4 w lp lc rgb "#c000ff" t "2013", \
 "TR_xyz_accu.dat" i 3  u 1:2:4 w lp lc rgb "#c04000" t "2014", \
 "TR_xyz_accu.dat" i 4  u 1:2:4 w lp lc rgb "#008040" t "2015", \
 "TR_xyz_accu.dat" i 5  u 1:2:4 w lp lc rgb "#8b0000" t "2016", \
 "TR_xyz_accu.dat" i 6  u 1:2:4 w lp lc rgb "#ffc020" t "2017", \
 "TR_xyz_accu.dat" i 7  u 1:2:4 w lp lc rgb "#f03232" t "2018", \
 "TR_xyz_accu.dat" i 8  u 1:2:4 w lp lc rgb "#000080" t "2019", \
 "TR_xyz_accu.dat" i 9  u 1:2:4 w lp lc rgb "#a08020" t "2020", \
 "TR_xyz_accu.dat" i 10 u 1:2:4 w lp lc rgb "#0080ff" t "2021", \
 "TR_xyz_accu.dat" i 11 u 1:2:4 w lp lc rgb "#1a1a1a" t "2022", \
  fmx(x,y) w l lw 3 dt "-" lc rgb "#905040" t sprintf("mean linear model %4f*(xp-%4f)+%4f",amx,px0,bmx), \
  fmy(x,y) w l lw 3 dt "-" lc rgb "#8060ff" t sprintf("mean linear model %4f*(yp-%4f)+%4f",amy,py0,bmy)
unset xlabel
unset ylabel
unset zlabel
unset title


pause -1


set title "comparaison fit x et fit y"
set xlabel "x pos (lamb2)"
set ylabel "y pos (lamb2)"
set zlabel "meters of water"
splot \
 "TR_xyz_accu.dat" i 2  u 1:2:4 w lp lc rgb "#c000ff" t "2013", \
 "TR_xyz_accu.dat" i 3  u 1:2:4 w lp lc rgb "#c04000" t "2014", \
 "TR_xyz_accu.dat" i 4  u 1:2:4 w lp lc rgb "#008040" t "2015", \
 "TR_xyz_accu.dat" i 5  u 1:2:4 w lp lc rgb "#8b0000" t "2016", \
 "TR_xyz_accu.dat" i 6  u 1:2:4 w lp lc rgb "#ffc020" t "2017", \
 "TR_xyz_accu.dat" i 7  u 1:2:4 w lp lc rgb "#f03232" t "2018", \
 "TR_xyz_accu.dat" i 8  u 1:2:4 w lp lc rgb "#000080" t "2019", \
 "TR_xyz_accu.dat" i 9  u 1:2:4 w lp lc rgb "#a08020" t "2020", \
 "TR_xyz_accu.dat" i 10 u 1:2:4 w lp lc rgb "#0080ff" t "2021", \
 "TR_xyz_accu.dat" i 11 u 1:2:4 w lp lc rgb "#1a1a1a" t "2022", \
 "TR_xyz_accu.dat" i 2  u 1:2:(fmx($1,$2)) w l lw 3 dt "-" lc rgb "#905040" \
  t sprintf("mean linear model %4f*(xp-%4f)+%4f",amx,px0,bmx), \
 "TR_xyz_accu.dat" i 2  u 1:2:(fmy($1,$2)) w l lw 3 dt "-" lc rgb "#8060ff" \
  t sprintf("mean linear model %4f*(yp-%4f)+%4f",amy,py0,bmy)
unset xlabel
unset ylabel
unset zlabel
unset title


pause -1

set title "comparaison fit x et fit y"
set xlabel "x pos (lamb2)"
set ylabel "meters of water"
plot \
 "TR_xyz_accu.dat" i 2  u 1:4 w lp lw 3 lc rgb "#c000ff" t "2013", \
 "TR_xyz_accu.dat" i 3  u 1:4 w lp lw 3 lc rgb "#c04000" t "2014", \
 "TR_xyz_accu.dat" i 4  u 1:4 w lp lw 3 lc rgb "#008040" t "2015", \
 "TR_xyz_accu.dat" i 5  u 1:4 w lp lw 3 lc rgb "#8b0000" t "2016", \
 "TR_xyz_accu.dat" i 6  u 1:4 w lp lw 3 lc rgb "#ffc020" t "2017", \
 "TR_xyz_accu.dat" i 7  u 1:4 w lp lw 3 lc rgb "#f03232" t "2018", \
 "TR_xyz_accu.dat" i 8  u 1:4 w lp lw 3 lc rgb "#000080" t "2019", \
 "TR_xyz_accu.dat" i 9  u 1:4 w lp lw 3 lc rgb "#a08020" t "2020", \
 "TR_xyz_accu.dat" i 10 u 1:4 w lp lw 3 lc rgb "#0080ff" t "2021", \
 "TR_xyz_accu.dat" i 11 u 1:4 w lp lw 3 lc rgb "#1a1a1a" t "2022", \
 "TR_xyz_accu.dat" i 2  u 1:(fmx($1,$2)) w l lw 5 dt "-" lc rgb "#905040" \
  t sprintf("mean linear model %4f*(xp-%4f)+%4f",amx,px0,bmx), \
 "TR_xyz_accu.dat" i 2  u 1:(fmy($1,$2)) w l lw 5 dt "-" lc rgb "#8060ff" \
  t sprintf("mean linear model %4f*(yp-%4f)+%4f",amy,py0,bmy)
unset xlabel
unset ylabel
unset title


pause -1


set title "comparaison fit x et fit y"
set xlabel "y pos (lamb2)"
set ylabel "meters of water"
plot \
 "TR_xyz_accu.dat" i 2  u 2:4 w lp lw 3 lc rgb "#c000ff" t "2013", \
 "TR_xyz_accu.dat" i 3  u 2:4 w lp lw 3 lc rgb "#c04000" t "2014", \
 "TR_xyz_accu.dat" i 4  u 2:4 w lp lw 3 lc rgb "#008040" t "2015", \
 "TR_xyz_accu.dat" i 5  u 2:4 w lp lw 3 lc rgb "#8b0000" t "2016", \
 "TR_xyz_accu.dat" i 6  u 2:4 w lp lw 3 lc rgb "#ffc020" t "2017", \
 "TR_xyz_accu.dat" i 7  u 2:4 w lp lw 3 lc rgb "#f03232" t "2018", \
 "TR_xyz_accu.dat" i 8  u 2:4 w lp lw 3 lc rgb "#000080" t "2019", \
 "TR_xyz_accu.dat" i 9  u 2:4 w lp lw 3 lc rgb "#a08020" t "2020", \
 "TR_xyz_accu.dat" i 10 u 2:4 w lp lw 3 lc rgb "#0080ff" t "2021", \
 "TR_xyz_accu.dat" i 11 u 2:4 w lp lw 3 lc rgb "#1a1a1a" t "2022", \
 "TR_xyz_accu.dat" i 2  u 2:(fmx($1,$2)) w l lw 5 dt "-" lc rgb "#905040" \
  t sprintf("mean linear model %4f*(xp-%4f)+%4f",amx,px0,bmx), \
 "TR_xyz_accu.dat" i 2  u 2:(fmy($1,$2)) w l lw 5 dt "-" lc rgb "#8060ff" \
  t sprintf("mean linear model %4f*(yp-%4f)+%4f",amy,py0,bmy)
unset xlabel
unset ylabel
unset title


pause -1



