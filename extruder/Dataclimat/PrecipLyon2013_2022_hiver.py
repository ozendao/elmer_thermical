import numpy as np
import os 
import sys
import math

alt_precip=2400
alt_accu=3139
debut=292
fin=365+130
yearinday=365.25
precip=np.loadtxt('PrecipLyon2013_2022.dat')
nmoy=math.floor(precip.shape[0]/yearinday)
precip_hiver=np.zeros(fin-debut)
for i in range(nmoy):
	index=math.floor(i*yearinday)
	precip_hiver+=precip[index+debut:index+fin]
precip_hiver/=nmoy

p_moy=np.sum(precip_hiver)
accu=np.asarray([1.47,1.42,1.07,1.85,1.41,1.85,1.75,1.33,2.11,0.92])
accu_moy=np.mean(accu)
print((p_moy,accu_moy,accu_moy/p_moy))

gradalt=(accu_moy/p_moy-1)/(alt_accu-alt_precip)

print(gradalt)

