f1(x)=a1*(x-3139)+b1
f2(x)=a2*(x-3139)+b2
f3(x)=a3*(x-3139)+b3
f4(x)=a4*(x-3139)+b4
f5(x)=a5*(x-3139)+b5
f6(x)=a6*(x-3139)+b6
f7(x)=a7*(x-3139)+b7
f8(x)=a8*(x-3139)+b8
f9(x)=a9*(x-3139)+b9
f10(x)=a10*(x-3139)+b10

set xrange[3100:3250]

set key left top

fit f1(x)  "TR_alt_accu.dat" i 2  via a1,b1
fit f2(x)  "TR_alt_accu.dat" i 3  via a2,b2
fit f3(x)  "TR_alt_accu.dat" i 4  via a3,b3
fit f4(x)  "TR_alt_accu.dat" i 5  via a4,b4
fit f5(x)  "TR_alt_accu.dat" i 6  via a5,b5
fit f6(x)  "TR_alt_accu.dat" i 7  via a6,b6
fit f7(x)  "TR_alt_accu.dat" i 8  via a7,b7
fit f8(x)  "TR_alt_accu.dat" i 9  via a8,b8
fit f9(x)  "TR_alt_accu.dat" i 10 via a9,b9
fit f10(x) "TR_alt_accu.dat" i 11 via a10,b10

set title "Winter mass accumulation teterousse"
set xlabel "altitude z (m)"
set ylabel "meters of water"

am=(a1+a2+a4+a4+a5+a6+a7+a8+a9+a10)/10.0
bm=(b1+b2+b4+b4+b5+b6+b7+b8+b9+b10)/10.0

fm(x)=am*(x-3139)+bm

plot \
f1(x)  w l lw 3 lc rgb "#c000ff" t sprintf("2013 a=%4f b=%4f",a1,b1),   "TR_alt_accu.dat" i 2  w lp lc rgb "#c000ff" t "2013", \
f2(x)  w l lw 3 lc rgb "#c04000" t sprintf("2014 a=%4f b=%4f",a2,b2),   "TR_alt_accu.dat" i 3  w lp lc rgb "#c04000" t "2014", \
f3(x)  w l lw 3 lc rgb "#008040" t sprintf("2015 a=%4f b=%4f",a3,b3),   "TR_alt_accu.dat" i 4  w lp lc rgb "#008040" t "2015", \
f4(x)  w l lw 3 lc rgb "#8b0000" t sprintf("2016 a=%4f b=%4f",a4,b4),   "TR_alt_accu.dat" i 5  w lp lc rgb "#8b0000" t "2016", \
f5(x)  w l lw 3 lc rgb "#ffc020" t sprintf("2017 a=%4f b=%4f",a5,b5),   "TR_alt_accu.dat" i 6  w lp lc rgb "#ffc020" t "2017", \
f6(x)  w l lw 3 lc rgb "#f03232" t sprintf("2018 a=%4f b=%4f",a6,b6),   "TR_alt_accu.dat" i 7  w lp lc rgb "#f03232" t "2018", \
f7(x)  w l lw 3 lc rgb "#000080" t sprintf("2019 a=%4f b=%4f",a7,b7),   "TR_alt_accu.dat" i 8  w lp lc rgb "#000080" t "2019", \
f8(x)  w l lw 3 lc rgb "#a08020" t sprintf("2020 a=%4f b=%4f",a8,b8),   "TR_alt_accu.dat" i 9  w lp lc rgb "#a08020" t "2020", \
f9(x)  w l lw 3 lc rgb "#0080ff" t sprintf("2021 a=%4f b=%4f",a9,b9),   "TR_alt_accu.dat" i 10 w lp lc rgb "#0080ff" t "2021", \
f10(x) w l lw 3 lc rgb "#1a1a1a" t sprintf("2022 a=%4f b=%4f",a10,b10), "TR_alt_accu.dat" i 11 w lp lc rgb "#1a1a1a" t "2022", \
fm(x) w l lw 3 dt "-" lc rgb "#905040" t sprintf("mean linear model %4f*(z-3139)+%4f",am,bm)

pause -1
