clear all

nb_partition=4;
Nb_vertical_layer=29;
Nbelement_rock=8;


% Generate a body n=2 for the rock

for p=1:nb_partition

name=sprintf('Taco3D/partitioning.%d/part.%d.elements',nb_partition,p);
    
mesh=load(name);
load nodes_bedrock
load nodes_surf

for i=1:size(mesh,1)
    cont=0;
for j=1:6
for k=1:size(nodes_surf,1)
if mesh(i,3+j)==nodes_surf(k)
    cont=cont+1;
end
end
end
if cont==6
    mesh(i,2)=1;
end
end

for i=1:size(mesh,1)
    cont=0;
for j=1:6
for k=1:size(nodes_bedrock,1)
if mesh(i,3+j)==nodes_bedrock(k)
    cont=cont+1;
end
end
end
if cont>=3
    mesh(i,2)=2;
end
end

name=sprintf('NewBoundary/part.%d.elements',p);
dlmwrite(name,mesh,'delimiter',' ','precision','%.0f')

end

fid0=fopen(sprintf('Taco3D/partitioning.%d/mesh.header.global',nb_partition));
Head=textscan(fid0,'%f32%f32%f32\n','Headerlines',0);
Head=[Head{1:3}];


Nb_nodes=Head(1,1)/Nb_vertical_layer;
Nb_element=Head(1,2)/(Nb_vertical_layer-1);
Nb_newbound=0;
% Create boundary condition n=6 for the ice/rock interface

for i=1:nb_partition
    
clear new_boundary

name2=sprintf('Taco3D/partitioning.%d/part.%d.nodes',nb_partition,i);
fidnode=fopen(name2);
Nodes=textscan(fidnode,'%f32%f32%f32%f32%f32\n','Headerlines',0);
Nodes=[Nodes{1:5}];

name=sprintf('Taco3D/partitioning.%d/part.%d.boundary',nb_partition,i);

fid=fopen(name);
bound303=textscan(fid,'%f32%f32%f32%f32%f32%f32%f32%f32\n','Headerlines',0);
bound303=[bound303{1:8}];
bound303(end,:)=[];

fid2=fopen(name);
bound404=textscan(fid2,'%f32%f32%f32%f32%f32%f32%f32%f32%f32\n','Headerlines',size(bound303,1));
bound404=[bound404{1:9}];

name=sprintf('Taco3D/partitioning.%d/part.%d.header',nb_partition,i);
fid3=fopen(name);
HeadPart=textscan(fid3,'%f32%f32%f32\n','Headerlines',0);
HeadPart=[HeadPart{1:3}];

fclose('all');

cont=0;
for j=1:size(bound303,1)
if (bound303(j,2)==1)
    cont=cont+1;
    new_boundary(cont,:)=bound303(j,:);
end
end

for j=1:size(new_boundary,1)
    new_boundary(j,6:8)=new_boundary(j,6:8)+Nbelement_rock*Nb_nodes;
    new_boundary(j,2)=6;
    new_boundary(j,1)=max(bound404(:,1))+j;
    new_boundary(j,3)=Nb_element*(Nbelement_rock-1)+bound303(j,3);
    new_boundary(j,4)=Nb_element*Nbelement_rock+bound303(j,3);
end

name2=sprintf('NewBoundary/part.%d.boundary',i);
fid=fopen(name2,'w');
cont=0;

for j=1:size(bound303,1)
    cont=cont+1;
    fprintf(fid,'%6.0f %6.0f %6.0f %6.0f %6.0f %6.0f %6.0f %6.0f\n',cont,bound303(j,2),bound303(j,3),bound303(j,4),bound303(j,5),bound303(j,6),bound303(j,7),bound303(j,8));
end



for j=1:size(bound404,1)
    cont=cont+1;
    
     if ((bound404(j,3)<Nb_element*Nbelement_rock)&&(bound404(j,2)==4))
        fprintf(fid,'%6.0f %6.0f %6.0f %6.0f %6.0f %6.0f %6.0f %6.0f %6.0f\n',cont,7,bound404(j,3),bound404(j,4),bound404(j,5),bound404(j,6),bound404(j,7),bound404(j,8),bound404(j,9));

    elseif ((bound404(j,2)==5)&&(Nodes(Nodes(:,1)==bound404(j,6),3)<949785.4)&&(Nodes(Nodes(:,1)==bound404(j,7),3)<949785.4)&&(Nodes(Nodes(:,1)==bound404(j,8),3)<949785.4)&&(Nodes(Nodes(:,1)==bound404(j,9),3)<949785.4))
        
         fprintf(fid,'%6.0f %6.0f %6.0f %6.0f %6.0f %6.0f %6.0f %6.0f %6.0f\n',cont,8,bound404(j,3),bound404(j,4),bound404(j,5),bound404(j,6),bound404(j,7),bound404(j,8),bound404(j,9));
    else
        fprintf(fid,'%6.0f %6.0f %6.0f %6.0f %6.0f %6.0f %6.0f %6.0f %6.0f\n',cont,bound404(j,2),bound404(j,3),bound404(j,4),bound404(j,5),bound404(j,6),bound404(j,7),bound404(j,8),bound404(j,9));
    end
     
end

for j=1:size(new_boundary,1)
    cont=cont+1;
    fprintf(fid,'%6.0f %6.0f %6.0f %6.0f %6.0f %6.0f %6.0f %6.0f\n',cont,new_boundary(j,2),new_boundary(j,3),new_boundary(j,4),new_boundary(j,5),new_boundary(j,6),new_boundary(j,7),new_boundary(j,8));
end

fclose(fid);

name3=sprintf('NewBoundary/part.%d.header',i);
fid2=fopen(name3,'w');
    fprintf(fid2,'%8.0f %8.0f %8.0f\n',HeadPart(1,1),HeadPart(1,2),size(new_boundary,1)+HeadPart(1,3));
    fprintf(fid2,'%8.0f\n',3);
    fprintf(fid2,'%8.0f %8.0f\n',303,size(bound303,1)+size(new_boundary,1));
    fprintf(fid2,'%8.0f %8.0f\n',404,size(bound404,1));
    fprintf(fid2,'%8.0f %8.0f\n',706,HeadPart(1,2));
    fprintf(fid2,'%8.0f %8.0f\n',HeadPart(6,1),HeadPart(6,2));
fclose(fid2);




Nb_newbound=Nb_newbound+size(new_boundary,1);
end



fid3=fopen('NewBoundary/mesh.header.global','w');
    fprintf(fid3,'%8.0f %8.0f %8.0f\n',Head(1,1),Head(1,2),Nb_newbound+Head(1,3));
    fprintf(fid3,'%8.0f\n',3);
    fprintf(fid3,'%8.0f %8.0f\n',404,Head(3,2));
    fprintf(fid3,'%8.0f %8.0f\n',706,Head(1,2));
fclose(fid3);

