clear all

nb_partition=4;
Nb_vertical_layer=29;
load ../0_Data/dem_surf_lid
Z=smooth2a(Z,1);

for i=1:nb_partition

name1=sprintf('Taco3D/partitioning.%d/part.%d.boundary',nb_partition,i);
name2=sprintf('Taco3D/partitioning.%d/part.%d.nodes',nb_partition,i);

fid=fopen(name1);
bound303=textscan(fid,'%f32%f32%f32%f32%f32%f32%f32%f32\n','Headerlines',0);
bound303=[bound303{1:8}];
bound303(end,:)=[];

fid2=fopen(name1);
bound404=textscan(fid2,'%f32%f32%f32%f32%f32%f32%f32%f32%f32\n','Headerlines',size(bound303,1));
bound404=[bound404{1:9}];

fid3=fopen(name2);
NodesIni=textscan(fid3,'%f32%f32%f32%f32%f32\n','Headerlines',0);
NodesIni=[NodesIni{1:5}];

fid4=fopen(name2);
Nodes=textscan(fid4,'%f32%f32%f32%f32%f32\n','Headerlines',0);
Nodes=[Nodes{1:5}];

fclose('all');



for j=1:size(bound404,1)
if (bound404(j,2)==4)
    for k=1:4
        Nodes_nb=bound404(j,5+k);
        
        x=Nodes(Nodes(:,1)==Nodes_nb,3);
        z=Nodes(Nodes(:,1)==Nodes_nb,5);
        
        
        ynew=griddata(X(~isnan(Z)),Z(~isnan(Z)),Y(~isnan(Z)),double(x),double(z));
        Nodes(Nodes(:,1)==Nodes_nb,4)=ynew;
        
        
   MeshUpdate(Nodes_nb)=ynew-NodesIni(NodesIni(:,1)==Nodes_nb,4);
   
    end
end   
end
end

dlmwrite('MeshUpdateFront.dat',MeshUpdate','%.0f')
