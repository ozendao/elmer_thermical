FUNCTION topo_front(Model, n, f) RESULT(g)
USE DefUtils
TYPE(Model_t) :: Model
TYPE(Variable_t), POINTER :: Thick
INTEGER :: n, i,nb_line,io,n_global
REAL(KIND=dp) :: f, g, x, y, z
logical :: first_time=.true.
real,dimension(:),allocatable :: rock_y

save first_time, rock_y

z = model % nodes % z (n)
x = model % nodes % x (n)
y = model % nodes % y (n)



if (first_time) then

first_time=.false.

  OPEN(1,file='MeshUpdateFront.dat',status='old')
  nb_line = 0
  DO
    READ(1,*,iostat=io)
    nb_line = nb_line + 1
    IF (io/=0) EXIT
  END DO
  CLOSE(1)
  nb_line=nb_line-1

ALLOCATE(rock_y(nb_line))

open(1,file='MeshUpdateFront.dat')
do i=1,nb_line
read(1,*) rock_y(i)
enddo
close(1)

endif


Thick =>  VariableGet(Model % Variables,'Thick')

n_global=Model % Mesh % ParallelInfo % GlobalDOFs (n)

if (x>949910) then
g=rock_y(n_global)*(950100-x)/(950100-949910)-20
elseif (x<949670) then
g=rock_y(n_global)*(x-949500)/(949670-949500)-20
else
g=rock_y(n_global)-20
endif

if (f<20) then
g=g*f/20
endif

if (Thick % Values (Thick % Perm(n))<-99.0) then

g=g*0.95

endif

! g=rock_y(n_global)

if (g<0.0) then
g=0.0
endif




END FUNCTION topo_front


