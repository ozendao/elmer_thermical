clear all

nb_partition=4;

for i=1:nb_partition

    
name=sprintf('Taco3D/partitioning.%d/part.%d.header',nb_partition,i);
fid=fopen(name);
HeadPart=textscan(fid,'%f32%f32%f32\n','Headerlines',0);
HeadPart=[HeadPart{1:3}];

NbNodes=HeadPart(1,1);
    
name1=sprintf('Taco3D/maillage_new.result.%d',i-1);
fid2=fopen(name1);
MeshUp1=textscan(fid2,'%f32\n','Headerlines',14+NbNodes+NbNodes+2+NbNodes+2);
MeshUp1=[MeshUp1{1:1}];

name1=sprintf('Taco3D/maillage_new.result.%d',i-1);
fid2=fopen(name1);
MeshUp2=textscan(fid2,'%f32\n','Headerlines',14+NbNodes+NbNodes+2+NbNodes+2+NbNodes+2);
MeshUp2=[MeshUp2{1:1}];

name1=sprintf('Taco3D/maillage_new.result.%d',i-1);
fid2=fopen(name1);
MeshUp3=textscan(fid2,'%f32\n','Headerlines',14+NbNodes+NbNodes+2+NbNodes+2+NbNodes+2+NbNodes+2);
MeshUp3=[MeshUp3{1:1}];

fclose('all');

name2=sprintf('Taco3D/partitioning.%d/part.%d.nodes',nb_partition,i);
fid=fopen(name2);
Nodes=textscan(fid,'%f32%f32%f32%f32%f32\n','Headerlines',0);
Nodes=[Nodes{1:5}];

name3=sprintf('NewBoundary/part.%d.nodes',i);
fid2=fopen(name3,'w');

for j=1:size(Nodes,1)
    fprintf(fid2,'%6.0f %6.0f %6.1f %6.1f %6.1f\n',Nodes(j,1),Nodes(j,2),Nodes(j,3)+MeshUp1(j),Nodes(j,4)+MeshUp2(j),Nodes(j,5)+MeshUp3(j));
end

fclose('all');

end
