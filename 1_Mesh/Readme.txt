To build the mesh:

1- Export .geo to .msh with gmsh

2 - Export .msh to elmerformat with elmergrid

ElmerGrid 14 2 taco2D.msh -autoclean

3 - Make partition

ElmerGrid 2 2 taco2D -autoclean -metis 4

4 - Make 3D

ExtrudeMesh taco2D Taco3D 29 1 4 0 0 0 1

5 - Extrude  (look at vertical layer option in MSH_Glacier3D_Rock_SurfLayer.F90)

./MSH_Glacier3DGrille   

6 - Create front horizontal topo (generate_front_topo.m)

7 - Build boundaries and bodies (generate_body_and_boundary.m)

8 - Copy new mesh files in Taco3D

9 - Run deforme maillage.sif

10 - Update node file (generate_mesh_from_MeshUpdate.m)

11 - Copy new node files in mesh directory