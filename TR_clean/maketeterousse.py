import numpy as np
import os 

data_dir="Data"
dem_contour="Contour_TR_glacier.dat"
dem_surf="DEM_TR_surf.dat"
dem_bed="DEM_TR_bed.dat"
min_rock_thick=100
noval=-9999
output_mesh="teterousse_2D"
extrude_config_p="TR_config_paral"
extrude_config_s="TR_config_serial"


el_size = 18.0     
spline = True
nb_proc=6
mesh_extruder="./ExtrudeMeshRockIce"
cutoff=12
wexp=2
mesh_extruded="TR_rockice"
nb_levels_ice=30
GL_b_ice=10
GL_s_ice=10
ratio_b_ice=0.2
ratio_s_ice=0.2
min_depth_ice=1.0
nb_levels_rock=20
GL_b_rock=0
GL_s_rock=10
ratio_b_rock=0
ratio_s_rock=0.2
min_depth_rock=20.0
merge_extruded="TR_adapt"
merge_vsif="merge_adapt"
###
os.system("mkdir -p {0}/EXT_total".format(data_dir))
os.system("cp {0}/{1} {0}/EXT_total/surf.xyz".format(data_dir,dem_surf))
os.system("cp {0}/{1} {0}/EXT_total/interface.xyz".format(data_dir,dem_bed))
surf=np.loadtxt("{0}/EXT_total/interface.xyz".format(data_dir))
min_h=surf[:,2][surf[:,2]>0].min()-min_rock_thick
bed=surf
for i in range(len(bed)):
	if bed[i,2]>noval:
		bed[i,2]=min_h

np.savetxt("{0}/EXT_total/bed.xyz".format(data_dir),bed)
###
Contour = np.loadtxt("{0}/{1}".format(data_dir,dem_contour))
x = Contour[:,0]
y = Contour[:,1]
if x[0]==x[-1] and y[0]==y[-1]:
	print('Same first and last points in contour file')
	Npt = len(x)-1
else:
	Npt = len(x)    
# Open the output file
geo = open("{0}.geo".format(output_mesh), 'w')
geo.write("// This a a geo file created using the python script Makegeo.py // \n")
geo.write("Mesh.Algorithm=5; \n")
geo.write("// To controle the element size, one can directly modify the lc value in the geo file // \n")
geo.write("lc = {0} ; \n".format(el_size))
# write the points coordinates (x,y,0,lc)
np=0
for j in range(0,Npt):
	np=np+1
	geo.write('Point({0}) = '.format(np)+r'{'+' {0}, {1}, 0.0, lc'.format(x[j],y[j])+r'}'+'; \n')

# if spline
if spline: 
	geo.write('Spline(1) = {')
	for j in range(0,Npt):
		geo.write('{0},'.format(j+1))
	geo.write('1}; \n')
	geo.write('Line Loop(2) = {1}; \n')
	geo.write('Plane Surface(3) = {2}; \n')
	geo.write('Physical Line(4) = {1}; \n')
	geo.write('Physical Surface(5) = {3}; \n')

# else it is lines, as a spline might not work in all case
else:
	nl=0
	for j in range(0,Npt-1):
		nl=nl+1
		geo.write('Line({0}) = '.format(nl)+r'{'+'{0},{1}'.format(j+1,j+2)+r'}'+'; \n')
	geo.write('Line({0}) = '.format(nl+1)+r'{'+'{0},{1}'.format(j+2,1)+r'}'+'; \n')    
	geo.write('Compound Line({0}) = '.format(nl+2)+r'{')
	for j in range(0,Npt-1):
		geo.write('{0}, '.format(j+1))
	geo.write('{0}'.format(j+2)+'}; \n')    
	geo.write('Line Loop({0}) = '.format(nl+3)+r'{'+'{0}'.format(nl+2)+r'};'+' \n')
	geo.write('Plane Surface({0}) = '.format(nl+4)+r'{'+'{0}'.format(nl+3)+r'};'+' \n')
	geo.write('Physical Line({0}) = '.format(nl+5)+r'{'+'{0}'.format(nl+2)+r'};'+' \n')
	geo.write('Physical Surface({0}) = '.format(nl+6)+r'{'+'{0}'.format(nl+4)+r'};'+' \n')

geo.close()
###
os.system("gmsh {0}.geo -1 -2".format(output_mesh))
os.system("ElmerGrid 14 2 {0}.msh -autoclean".format(output_mesh))
os.system("ElmerGrid 14 5 {0}.msh -autoclean".format(output_mesh))
EM_config_s=open(extrude_config_s,'w')
EM_config_s.write("{0:d} {1:d} {2:f} {3:f} 1\n".format(nb_levels_rock,nb_levels_ice,min_depth_rock,min_depth_ice))
EM_config_s.write("0\n")
EM_config_s.write("{0:d} {1:d} {2:d} {3:d} {4:f} {5:f} {6:f} {7:f} 0 0 0 0\n".format(GL_b_rock,GL_s_rock,GL_b_ice,GL_s_ice,ratio_b_rock,ratio_s_rock,ratio_b_ice,ratio_s_ice))
EM_config_s.write("{0:f} {1:f} {2:f}\n".format(cutoff,wexp,noval))
EM_config_s.write("0\n")
EM_config_s.close()
os.system(mesh_extruder+" "+output_mesh+" "+mesh_extruded+" "+extrude_config_s+" {0}/EXT_total".format(data_dir))
os.system("ElmerGrid 2 5 "+mesh_extruded+" -autoclean")	
os.system("ElmerSolver visu_rockice.sif")

if nb_proc>1:
	os.system("ElmerGrid 2 2 {0} -autoclean -metis {1:d}".format(output_mesh,nb_proc))
	EM_config_p=open(extrude_config_p,'w')
	EM_config_p.write("{0:d} {1:d} {2:f} {3:f} {4:d}\n".format(nb_levels_rock,nb_levels_ice,min_depth_rock,min_depth_ice,nb_proc))
	EM_config_p.write("0\n")
	EM_config_p.write("{0:d} {1:d} {2:d} {3:d} {4:f} {5:f} {6:f} {7:f} 0 0 0 0\n".format(GL_b_rock,GL_s_rock,GL_b_ice,GL_s_ice,ratio_b_rock,ratio_s_rock,ratio_b_ice,ratio_s_ice))
	EM_config_p.write("{0:f} {1:f} {2:f}\n".format(cutoff,wexp,noval))
	EM_config_p.write("0\n")
	EM_config_p.close()
	os.system(mesh_extruder+" "+output_mesh+" "+mesh_extruded+" "+extrude_config_p+" {0}/EXT_total".format(data_dir))
	os.system("mpirun -np {0:d} --bind-to hwthread ElmerSolver visu_rockice.sif".format(nb_proc))

