import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from mpl_toolkits import mplot3d
import math
import sys
import os
########################################################
def bed(x,y,abx, DD, rr, ar, lr, Ly, q1, q2, am, lm, Lx , Zmax): 
    y = (0.5*Ly+q1*x*np.exp(-q2*x))*2*y/Ly + am*np.sin(2*np.pi*x/lm)
    rr2=rr+(3.0-rr)*(x/Lx)**0.4
    bed = Zmax - abx*x-DD*(1-(y/(DD*rr2))**2) - ar*np.sin(2*np.pi*x/lr)
    return bed
#########
def rotate(origin, point, angle):
    #Rotate a point counterclockwise by a given angle around a given origin.
    #The angle should be given in radians.
    ox, oy = origin
    px, py = point
    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy
###########################################################
#Config file reader
for line in open(sys.argv[1],'r'):
	wlist=line.split()
	print(wlist)
	#General Parameters
	if wlist[0]=="target_dir":
		target_dir=wlist[1]          # Name of the bed
	if wlist[0]=="case_name":
		case_name=wlist[1]           # Name of the bed
	if wlist[0]=="minimal_ice_height":
		MinH=float(wlist[1])         # Initial height of ice
	if wlist[0]=="altitude_max":
		Zmax=float(wlist[1])         # Altitude max [m]
	if wlist[0]=="orientation":
		aspect=float(wlist[1])       # Aspect [°] (E=0 S=90 W=180 N=270)
	if wlist[0]=="bed_slope":
		slope=float(wlist[1])        # Mean bedrock slope [°]
	if wlist[0]=="alt_cutter":
		alt_cutter=float(wlist[1])   # Maximal valley depth [m] 
	if wlist[0]=="D_width":
		Ly = float(wlist[1])         # Domain width [m]
	if wlist[0]=="step_x":
		dx = float(wlist[1])         # X resolution [m]
	if wlist[0]=="step_y":
		dy = float(wlist[1])         # Y resolution [m]
	if wlist[0]=="xcoef":
		xcoef= float(wlist[1])       # X distance ratio boundary-contour 
	if wlist[0]=="ycoef":
		ycoef= float(wlist[1])       # Y distance ratio boundary-contour 
	#Bed geometry
	if wlist[0]=="DD":
		DD=float(wlist[1])           # Valley depth [m] - 500.0
	if wlist[0]=="rr":
		rr=float(wlist[1])           # Lateral aspect ratio [-] - 2.0
	if wlist[0]=="ar":
		ar=float(wlist[1])           # Roughness amplitude [m] - 0.0
	if wlist[0]=="lr":
		lr=float(wlist[1])           # Roughness wavelength [m] - infinity
	if wlist[0]=="q1":
		q1=float(wlist[1])           # Width coefficient [-] - 0.0 
	if wlist[0]=="q2":
		q2=float(wlist[1])           # Width exponent [m^-1] - infinity
	if wlist[0]=="am":
		am=float(wlist[1])           # Meander amplitude [m] - 0.0
	if wlist[0]=="lm":
		lm=float(wlist[1])           # Wavelength of glacier curvature [m] - 15000.0
	if wlist[0]=="min_alt_coef":
		min_alt_coef=float(wlist[1]) # ratio between max altitude and bed drop  [-]

#Creates mesh 
abx= np.tan(slope*np.pi/180) 
z0=bed(0, 0, abx=abx, DD=DD, rr=rr, ar=ar, lr=lr, Ly=Ly, q1=q1, q2=q2, am=am, lm=lm, Lx=4000.0,Zmax=Zmax)
Lx = (z0/abx)*min_alt_coef
X0 = 0.0
Y0 = 0-Ly/2
Nx = round(Lx/dx)
Ny = round(Ly/dy)
x = np.linspace(X0,X0+Lx,Nx+1)
y = np.linspace(Y0,Y0+Ly,Ny+1)
X, Y = np.meshgrid(x,y)
#Defines the nan mask
zb=None
zb=bed(X, Y, abx=abx, DD=DD, rr=rr, ar=ar, lr=lr, Ly=Ly, q1=q1, q2=q2, am=am, lm=lm, Lx=Lx,Zmax=Zmax)
zbmax=z0+alt_cutter-X*abx
zbcut=np.copy(zb)
zbcut[zb>zbmax] = np.nan
# Get contour
xc = np.zeros(6)
yc = np.zeros(6)
xc[0]=xcoef*dx
yc[0]=Ly/2-ycoef*dy
xc[1]=np.min(X[np.isnan(zbcut)])-xcoef*dx
yc[1]=Ly/2-ycoef*dy
xc[2]=Lx-xcoef*dx
yc[2]=np.max(Y[~np.isnan(zbcut[:,-1])])-ycoef*dy
xc[3]=Lx-xcoef*dx
yc[3]=np.min(Y[~np.isnan(zbcut[:,-1])])+ycoef*dy
xc[4]=np.min(X[np.isnan(zbcut)])-xcoef*dx
yc[4]=-Ly/2+ycoef*dy
xc[5]=xcoef*dx
yc[5]=-Ly/2+ycoef*dy
# Apply rotation :
(X,Y)=rotate((0, 0), (X,Y), math.radians(aspect))
(xc,yc)=rotate((0, 0), (xc,yc), math.radians(aspect))
# plot this bed 
plt.figure()
plt.contourf(X, Y, zbcut)
plt.plot(xc,yc,c='k')
plt.title('Synthetical bed')
plt.colorbar()
plt.axis('equal')
# Save contour bed and sirf an ascii files 
bed=np.zeros((len(x)*len(y),3))
surf=np.zeros((len(x)*len(y),3))
print(np.size(x),np.size(y),np.size(zb))
k=0
for j in range(len(y)):
	for i in range(len(x)):
		if np.isnan(zbcut[j,i]):
			zbl=-9999
			zsl=-9999
		else:
			zbl=zbcut[j,i]
			zsl=zbcut[j,i]+MinH

		bed[k,0]=X[j,i]
		bed[k,1]=Y[j,i]
		bed[k,2]=zbl
		surf[k,0]=X[j,i]
		surf[k,1]=Y[j,i]
		surf[k,2]=zsl
		k+=1
os.system("mkdir -p {0}".format(target_dir))
np.savetxt(target_dir+'/Contour_'+case_name+'.dat',
np.column_stack((xc,yc)), fmt="%10.2f %10.2f")
np.savetxt(target_dir+'/Bed_'+case_name+'.dat',bed)
np.savetxt(target_dir+'/Surf_'+case_name+'.dat',surf)
plt.show()
