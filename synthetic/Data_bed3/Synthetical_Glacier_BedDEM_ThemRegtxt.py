import numpy as np
import matplotlib.pyplot as plt

from datetime import datetime
import math


def bed(x,y,abx, DD, rr, ar, lr, Ly, q1, q2, am, lm, Lx , Zmax): 
    y = (0.5*Ly+q1*x*np.exp(-q2*x))*2*y/Ly + am*np.sin(2*np.pi*x/lm)
    rr2=rr+(3.0-rr)*(x/Lx)**0.4
    interface = Zmax - abx*x-DD*(1-(y/(DD*rr2))**2) - ar*np.sin(2*np.pi*x/lr)
    return interface

def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy


bed_version = 'bed3'
MinH = 20.0
rockheight=200.0

#Parameters

Zmax=4000.0   # Altitude max
abx= np.tan(45*np.pi/180)    # Mean bedrock slope [-] - 0.15
aspect=180.0         # Aspect in degree (E=0 S=90 W=180 N=270)

#========================================

DD=40.0      # valley depth [m] - 500.0
rr=10.0        # lateral aspect ratio [-] - 2.0
ar=0.0        # Roughness amplitude [m] - 0.0
lr=500.0     # Roughness wavelength [m] - infinity
#Ly=Ly,       # Half domain width [m] - 2000.0
q1=0.0        # Width coefficient [-] - 0.0 
q2=1/500.0   # Width exponent [m^-1] - infinity
am=0.0      # Meander amplitude [m] - 0.0
lm=6000.0    # Wavelength of glacier curvature [m] - 15000.0
Ly = 1000.0   #Domain width
Lx0= 4000.0
# create a grid with to interpolate teh continuous DEM and save it
# dx = dy = 20

z0=bed(0, 0, abx=abx, DD=DD, rr=rr, ar=ar, lr=lr, Ly=Ly, q1=q1, q2=q2, am=am, lm=lm, Lx=Lx0,Zmax=Zmax)
Lx = (z0/abx)*0.7
X0 = 0.0
Y0 = 0-Ly/2
dx = 30.0
dy = 30.0
Nx = round(Lx/dx)
Ny = round(Ly/dy)
x = np.linspace(X0,X0+Lx,Nx+1)
y = np.linspace(Y0,Y0+Ly,Ny+1)
X, Y = np.meshgrid(x,y)

zi=None
zb=None
zi=bed(X, Y, abx=abx, DD=DD, rr=rr, ar=ar, lr=lr, Ly=Ly, q1=q1, q2=q2, am=am, lm=lm, Lx=Lx,Zmax=Zmax)
zb=bed(X, np.zeros(Y.shape), abx=abx, DD=DD, rr=rr, ar=0.0, lr=lr, Ly=Ly, q1=q1, q2=q2, am=0.0, lm=lm, Lx=Lx,Zmax=Zmax)

zimax=z0+400-X*abx
zicut=np.copy(zi)
zicut[zi>zimax] = np.nan

print(zicut[zi>zimax])
# Get contour
xc = np.zeros(6)
yc = np.zeros(6)
xc[0]=2*dx
yc[0]=Ly/2-2*dy
xc[1]=np.min(X[np.isnan(zicut)])-dx
yc[1]=Ly/2-2*dy
xc[2]=Lx-dx
yc[2]=np.max(Y[~np.isnan(zicut[:,-1])])-2*dy
xc[3]=Lx-dx
yc[3]=np.min(Y[~np.isnan(zicut[:,-1])])+2*dy
xc[4]=np.min(X[np.isnan(zicut)])-dx
yc[4]=-Ly/2+2*dy
xc[5]=2*dx
yc[5]=-Ly/2+2*dy


# Apply rotation :

theta = np.radians(30)
c, s = np.cos(theta), np.sin(theta)
R = np.array(((c, -s), (s, c)))

(X,Y)=rotate((0, 0), (X,Y), math.radians(aspect))
(xc,yc)=rotate((0, 0), (xc,yc), math.radians(aspect))


# plot this bed 
plt.figure()
plt.contourf(X, Y, zicut)
plt.plot(xc,yc,c='k')
#plt.contour(x,y,zb, [4400])
plt.title('Synthetical bed')
plt.colorbar()
plt.axis('equal')


today = datetime.today()
# Save the contour in an ascii file 
np.savetxt('SyntBed_Contour_'+bed_version+'.dat',
np.column_stack((xc,yc)), fmt="%10.2f %10.2f")

bed=np.zeros((len(x)*len(y),3))
interface=np.zeros((len(x)*len(y),3))
surf=np.zeros((len(x)*len(y),3))
print(np.size(x),np.size(y),np.size(zi))
k=0
for j in range(len(y)):
	for i in range(len(x)):
		if np.isnan(zicut[j,i]):
			zbl=-9999
			zil=-9999
			zsl=-9999
		else:
			zbl=zb[j,i]-rockheight
			zil=zicut[j,i]
			zsl=zicut[j,i]+MinH
		bed[k,0]=X[j,i]
		bed[k,1]=Y[j,i]
		bed[k,2]=zbl
		interface[k,0]=X[j,i]
		interface[k,1]=Y[j,i]
		interface[k,2]=zil
		surf[k,0]=X[j,i]
		surf[k,1]=Y[j,i]
		surf[k,2]=zsl
		k+=1

np.savetxt('SyntBed_Bed_'+bed_version+'.dat',bed)
np.savetxt('SyntBed_Interface_'+bed_version+'.dat',interface)
np.savetxt('SyntBed_Surf_'+bed_version+'.dat',surf)

plt.show()
